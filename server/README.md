# PATHFINDER-Server Tiling Server

#### Authors:

Lukas Baur | st141998@stud.uni-stuttgart.de

##### Version: 

September 2021



## Installation

Similar to Pathfinder, same packages. Additionally: Install OpenCV. 

#### OpenCV

See instructions here:
https://docs.opencv.org/3.4/d7/d9f/tutorial_linux_install.html

```Bash
cd ~/
git clone https://github.com/opencv/opencv.git
cd ~/opencv
mkdir build
cmake -D CMAKE_BUILD_TYPE=Release -D CMAKE_INSTALL_PREFIX=/usr/local build
cd build
make -j7 # runs 7 jobs in parallel
make install
```

We now have to insert the path `~/opencv` into our `CMakeLists.txt` file (if not done already):
`set(OpenCV_DIR /home/<your name>/opencv/build)`

#### Pistache

Similar to the PathfinderVis application, the lib folder has to have a pistache installation. 

Simply copy the lib folder from there and insert it into the same position respectively to let the execution access the files correctly. 
