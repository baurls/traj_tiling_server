
#include "defs.h"
#include "exporter.h"
#include "flags.h"
#include "graph.h"
#include "naive_ch_pathfinder.h"
#include "naive_pathfinder.h"
#include "options.h"
#include "path_generator.h"
#include "path_parser.h"
#include "pathfinder.h"
#include "pathfinder_ds.h"
#include "random.h"
#include "stxxl_init.h"
#include "time_tracker.h"
#include "heatmap_builder.h"
#include "grid.h"
#include "intersection_checker.h"
#include "tiling/tile_creator.h"
#include "tiling/static_service.h"
#include "tiling/interactive_service.h"
#include "grid_stack.h"
#include "level_extractor.h"
#include "path_exporter.h"
#include "path_importer.h"

#include <fstream>
#include <sstream>
#include <string>

#include <pistache/description.h>
#include <pistache/endpoint.h>
#include <pistache/http.h>
#include <pistache/http_header.h>

#include <pistache/serializer/rapidjson.h>

#include <rapidjson/document.h>
#include <rapidjson/rapidjson.h>
#include <rapidjson/stringbuffer.h>
#include <rapidjson/writer.h>

#include <iostream>
#include <stdio.h>

using namespace std;

pf::Graph graph;
pf::PathsDS paths_ds;
pf::PathsDS raw_paths_ds;
pf::GridStack grid_stack;

double smoothness = 1;
int max_zoom;

std::string static_tile_dir = "../../data/tiles/static";
std::string dynamic_tile_dir = "../../data/tiles/dynamic";

pf::StaticTileService static_tile_service(static_tile_dir);


pf::LevelExtractor level_extractor;

using namespace Pistache;

namespace Generic
{

void handleReady(const Rest::Request&, Http::ResponseWriter response)
{
	response.send(Http::Code::Ok, "1");
}
} // namespace Generic

class TileService
{

public:
	// Initialization of Pathfinder
	
	TimeTracker time_tracker;
	pf::BoundingBox query_box;
	pf::BoundingBox view_box;
	pf::Options query_options;

	
	// Cache variables
	long no_of_found_paths = 0;
	pf::BoundingBox cached_query_box;
	pf::Options cached_query_options;
	pf::PathIDs cached_results;

	// for batched, inorder update
	TileService(Address addr, uint nr_of_pathfinder_threads, uint nr_of_service_threads, pf::Pathfinder& p, const pf::Graph& graph, const pf::PathboxTileCreator& tile_generator)
	    : httpEndpoint(std::make_shared<Http::Endpoint>(addr))
	    , desc("TileService API", "0.1")
		, nr_of_service_threads(nr_of_service_threads)
		, dynamic_tile_service(dynamic_tile_dir, graph, p, 10, tile_generator)
	{
		
	}

	void init()
	{
		auto opts = Http::Endpoint::options().threads(nr_of_service_threads);
		httpEndpoint->init(opts);
		createDescription();
	}

	void start()
	{
		router.initFromDescription(desc);

		Rest::Swagger swagger(desc);
		swagger.uiPath("/doc")
		    .uiDirectory("/home/octal/code/web/swagger-ui-2.1.4/dist")
		    .apiPath("/PathFinder-api.json")
		    .serializer(&Rest::Serializer::rapidJson)
		    .install(router);

		httpEndpoint->setHandler(router.handler());
		httpEndpoint->serve();
	}

	void stop()
	{

		httpEndpoint->setHandler(router.handler());
		httpEndpoint->shutdown();
	}

private:

	std::shared_ptr<Http::Endpoint> httpEndpoint;
	Rest::Description desc;
	Rest::Router router;
	const size_t nr_of_service_threads;
	pf::InteractiveTileService dynamic_tile_service;


	// Creates the server description and maps the paths
	void createDescription()
	{

		desc.info().license(
		    "Apache", "http://www.apache.org/licenses/LICENSE-2.0");

		auto backendErrorResponse =
		    desc.response(Http::Code::Internal_Server_Error,
		        "An error occured with the backend");

		desc.schemes(Rest::Scheme::Http)
		    .basePath("/tile_server")
		    .produces(MIME(Application, Json))
		    .consumes(MIME(Application, Json));

		desc.route(desc.get("/ready"))
		    .bind(&Generic::handleReady)
		    .response(Http::Code::Ok, "Response to the /ready call")
		    .hide();

		auto pathfinderPath = desc.path("/tile_server");

		pathfinderPath.route(desc.options("/"), "Json-Response")
		    .bind(&TileService::optionsRecieved, this);


		// static tiling
		pathfinderPath.route(desc.get("/calculate"), "Json-Response")
		    .bind(&TileService::calculate, this)
		    .produces(MIME(Text, Json));

		// static tiling
		pathfinderPath.route(desc.get("/:z/:x/:y"), "Json-Response")
		    .bind(&TileService::get_tile, this)
		    .produces(MIME(Image, Png));

		// dynamic tiling

		pathfinderPath.route(desc.get("/:session_id/:z/:x/:y"), "Json-Response")
		    .bind(&TileService::get_session_tile, this)
		    .produces(MIME(Image, Png));
	}

	void optionsRecieved(const Rest::Request&, Http::ResponseWriter response)
	{
		// Allow all methods for access control, send the response
		response.headers().add<Http::Header::AccessControlAllowOrigin>("*");
		response.headers().add<Http::Header::AccessControlAllowHeaders>(
		    "Origin, X-Requested-With, Content-Type, Accept, Authorization");
		response.headers().add<Http::Header::AccessControlAllowMethods>(
		    "GET, PUT, POST, DELETE, PATCH, OPTIONS");
		response.send(Http::Code::Ok);
	}


	void add_response_headers(Http::ResponseWriter& response){
			// Allow all methods for access control
		response.headers().add<Http::Header::AccessControlAllowOrigin>("*");
		response.headers().add<Http::Header::AccessControlAllowHeaders>(
		    "Origin, X-Requested-With, Content-Type, Accept, Authorization");
		response.headers().add<Http::Header::AccessControlAllowMethods>(
		    "GET, PUT, POST, DELETE, PATCH, OPTIONS");
	}

	void get_tile(const Rest::Request& request, Http::ResponseWriter response){
		try{
			static_tile_service.handle_tile_request(request, response);
		} catch (const invalid_argument& e){
			response.send(Http::Code::Bad_Request, e.what());
		}
	}

	void get_session_tile(const Rest::Request& request, Http::ResponseWriter response){
		try{
			dynamic_tile_service.handle_tile_request(request, response);
		} catch (const invalid_argument& e){
			response.send(Http::Code::Bad_Request, e.what());
		}
	}

	void calculate(const Rest::Request& request, Http::ResponseWriter response){
		try{
			// simulate new calculation request:
			pf::Query  q(pf::BoundingBox(pf::Coordinate(-100, 1000), pf::Coordinate(-100, 1000)));
			auto session_id = dynamic_tile_service.create_session_from_query(q);
			Print("Session ID (dynamic tiling): " << session_id);
			
			std::string return_message = "{\"session_id\" : " +  std::to_string(session_id)  + "}";
			response.send(Http::Code::Ok, return_message);
		} catch (const invalid_argument& e){
			response.send(Http::Code::Bad_Request, e.what());
		}
	}

};
using namespace pf;



int main()
{

	// Initialization of the Paths
	std::string graph_filename;
	std::string path_filename;
	std::string path_import_dir;
	std::string path_import_filename;
	std::int16_t import_type = -1;
	std::int16_t max_path_length = -1;
	std::int16_t nr_of_pathfinder_threads = 4;
	std::int16_t nr_of_service_threads = 4;
	std::int16_t tiling_level_start = 1;
	std::int16_t tiling_level_stop = 18;
	std::int16_t generate_tiling_no = -1;
	std::int16_t log_tiling_no = -1;
	bool generate_tiling = false;
	bool log_tiling = false;

	// config :
	std::string stng;
	std::string configPath = "../settings.txt";

	std::ifstream fin(configPath);
	std::string line;
	std::istringstream sin;
	ifstream configFile(configPath);



	// Keep track how much time was needed for initializing the datastructures
	auto t = std::time(nullptr);
    auto tm = *std::localtime(&t);
	std::cout << "[" << std::put_time(&tm, "%d-%m-%Y %H-%M-%S") << "]: Program start" << std::endl;
	TimeTracker startup_time_tracker;
	startup_time_tracker.start();

	
	cout << "------------Tile-Server------------" << endl;
	if (configFile.good()) {
		cout << "Config file with the name \"settings.txt\" found" << endl;
		while (std::getline(fin, line)) {
			sin.str(line.substr(line.find("=") + 1));
			if (line.rfind("graph_filename", 0) == 0) {
				sin >> graph_filename;
				cout << "| graph_filename:                 " << graph_filename<< endl;
			} else if (line.rfind("real_paths_filename", 0) == 0) {
				sin >> path_filename;
				cout << "| real_paths_filename:            " << path_filename<< endl;
			} else if (line.rfind("import_type", 0) == 0) {
				sin >> import_type;
				cout << "| import_type:                   " << import_type<< endl;
			} else if (line.rfind("generated_paths_import_name", 0) == 0) {
				sin >> path_import_dir;
				cout << "| generated_paths_import_name:   " << path_import_dir<< endl;
			} else if (line.rfind("max_path_length", 0) == 0) {
				sin >> max_path_length;
				cout << "| max_path_length:               " << max_path_length<< endl;
			} else if  (line.rfind("gen_paths_filename", 0) == 0) {
				sin >> path_import_filename;
				cout << "| gen_paths_filename:            " << path_import_filename<< endl;
			} else if  (line.rfind("smoothness", 0) == 0) {
				sin >> smoothness;
				cout << "| smoothness:                    " << smoothness<< endl;
			} else if (line.rfind("max_zoom", 0) == 0) {
				sin >> max_zoom;
				cout << "| max_zoom:                      " << max_zoom<< endl;
			} else if (line.rfind("nr_of_pathfinder_threads", 0) == 0) {
				sin >> nr_of_pathfinder_threads;
				cout << "| nr_of_pathfinder_threads:      " << nr_of_pathfinder_threads<< endl;
			} else if (line.rfind("nr_of_service_threads", 0) == 0) {
				sin >> nr_of_service_threads;
				cout << "| nr_of_service_threads:         " << nr_of_service_threads<< endl;
			} else if (line.rfind("generate_tiling", 0) == 0) {
				sin >> generate_tiling_no;
				generate_tiling = (generate_tiling_no == 1);
				cout << "| Generate tiling:               " << generate_tiling << endl;
			}else if (line.rfind("tiling_level_start", 0) == 0) {
				sin >> tiling_level_start;
				cout << "| Create tiling start            " << tiling_level_start << endl;
			}else if (line.rfind("tiling_level_stop", 0) == 0) {
				sin >> tiling_level_stop;
				cout << "| Create tiling stop             " << tiling_level_stop << endl;
			}else if (line.rfind("log_tiling", 0) == 0) {
				sin >> log_tiling_no;
				log_tiling = (log_tiling_no == 1);
				cout << "| Log tiling:                    " << log_tiling_no << endl;
			}
			sin.clear();
		}
	} else {
		cout << "No config file with the name \"settings.txt\" found." << endl;
		cout << "Please enter in the \"./pathfinder_server/code\" directory "
		        "the \"settings.txt\"-file!"
		     << endl;
		cout << "Server will interrupt, please retry!" << endl;
		return EXIT_SUCCESS;
	}

	cout << endl;

	cout << "------------Calculation will start------------" << endl;

	level_extractor = LevelExtractor(smoothness, max_zoom);
	TimeTracker time_tracker;
	Random random;


	Print("Loading graph from file..");
	time_tracker.start();
	graph.init(graph_filename);
	time_tracker.stop();
	Print("↳ Took " << time_tracker.getLastMeasurment() << " seconds");
	
	
	Print("");
	time_tracker.start();
	if (import_type == 0 or import_type == 1){
		// Parsing .json path files
		Print("Parsing paths...");
		PathParser path_parser(graph, raw_paths_ds);
		bool success = path_parser.parse(path_filename);
		if (!success) {
			throw Exception(
			    "Parsing of the following path file failed:\n" + path_filename);
		}
		paths_ds = partitioner_util::partitionPaths(graph, raw_paths_ds);
		
	}
	else if(import_type == 5){
		// importing generated paths
		Print("Importing generated paths [plain]:" + path_import_dir);
		bool success = PathImporter(graph, paths_ds).importPaths(path_import_dir);
		assert(success);
	}
	else if(import_type == 6){
		// importing generated paths
		Print("Importing generated paths [binary]:" + path_import_dir);
		bool success = PathImporter(graph, paths_ds).importPathsBin(path_import_dir);
		assert(success);
	}
	else if(import_type == -1){
		throw Exception("Import Type is not decalred in settings file. Choose from 5,6");
	}
	else {
		throw Exception("Invalid Import Type! Choose from 5,6");
	}
	time_tracker.stop();
	Print("| Paths read successfully.");
	Print("↳ Took " << time_tracker.getLastMeasurment() << " seconds\n");
	
	// --- start pathfinder instance -------------------------
	Print("Execute pathfinder algorithm...");
	// pathfinder code
	pf::PathfinderDS p_ds = PathfinderDS(graph, nr_of_pathfinder_threads, std::move(paths_ds));
	pf::Pathfinder p = pf::Pathfinder(p_ds, nr_of_pathfinder_threads);

	// --- tile generation -------------------------
	const LevelExtractor level_extractor(smoothness, max_zoom);
	const BoundingBoxes& path_boxes = p_ds.getPathBoxes(); 

	// offline tiling creation
	if(generate_tiling){
		PathfinderTileCreator tile_generator = PathfinderTileCreator(graph, paths_ds, p, static_tile_dir, level_extractor, log_tiling);
		if (tiling_level_start < 1 || tiling_level_start > 18){
			throw std::invalid_argument( "Tiling start must be in [1, 18]" );
		}
		else if (tiling_level_stop < 1 || tiling_level_stop > 18){
			throw std::invalid_argument( "Tiling end must be in [1, 18]" );
		}
		tile_generator.generate_tiles_for_level_range(tiling_level_start, tiling_level_stop);
	}
	
	// online tiling creation
	PathboxTileCreator dynamic_tile_generator = PathboxTileCreator(graph, paths_ds, static_tile_dir, path_boxes, level_extractor, nr_of_pathfinder_threads);


	// --- Start the server ----------------------------- 
	std::cout << "------------Tile-Server starts------------" << std::endl;


	// Server data
	const int port_nbr = 9081;
	Pistache::Port port(port_nbr);
	Pistache::Address addr(Pistache::Ipv4::any(), port);

	TileService service(addr, nr_of_pathfinder_threads, nr_of_service_threads, p, graph, dynamic_tile_generator);

	std::cout << "------------ Tile-Server hardware ------------" << std::endl;
	std::cout << "Cores = " << hardware_concurrency() << std::endl;
	std::cout << "Service:    Using " << nr_of_service_threads << " threads" << std::endl;
	std::cout << "PATHFINDER: Using " << nr_of_pathfinder_threads << " threads" << std::endl;
	std::cout << "Port:             " << port_nbr  << std::endl;

	// show server ready message
	t = std::time(nullptr);
    tm = *std::localtime(&t);
	startup_time_tracker.stop();
	std::cout << "[" << std::put_time(&tm, "%d-%m-%Y %H-%M-%S") << "]: The server is now ready to serve." << std::endl;
	int time_needed = (int) startup_time_tracker.getLastMeasurment();
	auto t_sec = time_needed % 60;
	auto t_min = ((time_needed - t_sec) % 3600) / 60;
	auto t_h = (time_needed - t_sec - 60*t_min) / 3600;

	Print("[Total startup time:   " << t_h << "h " << t_min << "m " << t_sec << "s]");
	// start server request handler
	service.init();
	service.start();

	return EXIT_SUCCESS;
}


