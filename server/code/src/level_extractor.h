#pragma once

#include "basic_types.h"
#include "defs.h"
#include "paths.h"
#include "query.h"
#include "time_tracker.h"
#include "options.h"

#include <map>

/*
	# # # # # # # # # # # # # # # # # # # # # # # # # #
	#                                                 #
	#   Created by Lukas Baur, Master Thesis 2021.    #
	#                                                 #
	#   Supervisor: T. Rupp, Prof. S. Funke           #
	#   Institute:  FMI (University of Stuttgart)     #
	#                                                 #
	# # # # # # # # # # # # # # # # # # # # # # # # # #
*/


namespace pf
{

/*
 * Util function to get the dynamic level based on the zoom.
 */
class LevelExtractor
	{
	public:
		/*
		* Retrieves all path ids for the given query
		*/
		LevelExtractor();
		LevelExtractor(const double smoothness, const double max_zoom);
		pf::Level extract_level(const int& zoom, const pf::Graph& graph) const;
		pf::Level extract_level(const pf::Options& query_options, const pf::Graph& graph) const;

		double smoothness;
		double max_zoom;
	};
} // namespace pf
