#include "grid.h"
#include "defs.h"

namespace pf
{

//
// Member functions of CountGrid and Grid
//
//            j
//		_____________n
//     |              |
//     |              |
//   i |              |
//     |              |
//    m|______________|
//		

	Grid::Grid(const GridIndex m, const GridIndex n, const BoundingBox& point_bb)
		: Grid(m, n, point_bb.min_coordinate, point_bb.max_coordinate)
	{
	}


	Grid::Grid(const GridIndex m, const GridIndex n, const Coordinate& min_coordinate, const Coordinate& max_coordinate)
		: m(m)
		, n(n)
		, no_of_cells(m*n)
		, p0(min_coordinate)
		, p1(max_coordinate)
		, span_y(p1.lat - p0.lat)
		, span_x(p1.lon - p0.lon)
		, h_y(span_y / m)
		, h_x(span_x / n)
		, center_offset(Coordinate(0.5*h_y, 0.5*h_x))
	{
	}


	Grid Grid::clone() const{
		return Grid(m, n, p0, p1);
	}

	Grid Grid::get_increased_level_copy() const{
		return Grid(m / 2, n / 2, p0, p1);
	}
	
	GlobalGridIndex Grid::index_for(const GridIndex i, const GridIndex j) const{
		assert(0 <= i);
		assert(0 <= j);
		assert(i < m);
		assert(j < n);
		return i*n + j;
	}
	
	void Grid::index_for(const GlobalGridIndex index, GridIndex& i, GridIndex& j) const{
		j = index % n;
		i = (index - j) / n; 
	}

	GlobalGridIndex Grid::get_cell(const Coordinate point) const{
		GridIndex i_out;
		GridIndex j_out;
		get_cell(point.lon, point.lat, i_out, j_out);
		return index_for(i_out, j_out);
	}

	void Grid::get_cell(const Coordinate point, GridIndex& i_out, GridIndex& j_out) const{
		get_cell(point.lon, point.lat, i_out, j_out);
	}

	void Grid::get_cell(const double x, const double y, GridIndex& i_out, GridIndex& j_out) const{
		auto x_diff = x - p0.lon;
		auto y_diff = y - p0.lat;

		// check points are within the bounnding box
		assert(x_diff >= 0);
		assert(x_diff <= span_x);
		assert(y_diff <= span_y);

		// calculate indices and write back
		j_out =  static_cast<GridIndex>(x_diff / h_x);
		i_out =  static_cast<GridIndex>(y_diff / h_y);
		// special cases where the point lies exacly on the left (or lower) boundary. 
		if(i_out == m){
			i_out--;
		}
		if(j_out == n){
			j_out--;
		}
	}

	Coordinate Grid::get_cell_center(const GridIndex& i, const GridIndex& j) const{
			auto center = Coordinate(i*h_y, j*h_x);
			center.shift_by(center_offset);
			center.shift_by(p0);
			return center;
		}

	Coordinate Grid::get_cell_center(const GlobalGridIndex& k) const{
		GridIndex i=0;
		GridIndex j=0;
		index_for(k, i, j);
		return get_cell_center(i, j);
	}
	
	
	
	void CountGrid::add_point(const Coordinate& point){
		add_point(point.lon, point.lat);
	}


	void CountGrid::add_point(const double x, const double y){
		// get i and j indey for point
		GridIndex i;
		GridIndex j;
		get_cell(x, y, i, j);
		// increment the count
		increment_count(i, j);
	}
	
	void CountGrid::process_covering(const CellCovering& covering){
		for (auto marker : covering){
			count_vector[marker]++;
		}
	}


	inline Count CountGrid::count_at(const GridIndex i, const GridIndex j) const{
		return count_vector[index_for(i, j)];
	}


} // namespace pf
