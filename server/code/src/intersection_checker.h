#pragma once

#include "basic_types.h"
#include "defs.h"
#include "graph.h"
#include "paths.h"
#include "bounding_box.h"


/*
	# # # # # # # # # # # # # # # # # # # # # # # # # #
	#                                                 #
	#   Created by Lukas Baur, Master Thesis 2021.    #
	#                                                 #
	#   Supervisor: T. Rupp, Prof. S. Funke           #
	#   Institute:  FMI (University of Stuttgart)     #
	#                                                 #
	# # # # # # # # # # # # # # # # # # # # # # # # # #
*/



namespace pf
{

// types
using Count = unsigned long int;


namespace unit_tests
{
	void testCountGrid();
} // namespace unit_tests


class IntersectionChecker
{
	public:
		// methods
		CellCovering return_intersected_cells(const Edge& edge, const Grid& grid);
		
	private:


};

class NaiveIntersectionChecker : IntersectionChecker{

	public:
		// methods
		static CellCovering return_intersected_cells(const Graph& graph, const Edge& edge, const Grid& grid){
			// load start cell
			auto u = graph.getNode(edge.source);
			GridIndex u_i;
			GridIndex u_j;
			grid.get_cell(u.coordinate, u_i, u_j);

			// load end cell
			auto v = graph.getNode(edge.target);
			GridIndex v_i;
			GridIndex v_j;
			grid.get_cell(v.coordinate, v_i, v_j);

			return calc_intersected_cells(u_i, u_j, u.coordinate, v_i, v_j, v.coordinate, grid);
		};

	private:

		//
		//   ______________ v
		//  |__|__|__|__|x_|
		//  |__|__| x|x_|__|
		//  |__|_x|x_|__|__|
		//  |_x|__|__|__|__|
		//	u
		static CellCovering calc_intersected_cells(const GridIndex& u_i, const GridIndex& u_j, const Coordinate& u, const GridIndex& v_i, const GridIndex& v_j, const Coordinate& v, const Grid& grid){
			CellCovering found_indices = CellCovering();
			auto max_i = std::max(u_i, v_i);
			auto max_j = std::max(u_j, v_j);
			for(auto i = std::min(u_i, v_i); i <= max_i; i++){
				for(auto j = std::min(u_j, v_j); j <= max_j; j++){
					if(box_intersects_line(i, j, u, v, grid)){
						found_indices.push_back(grid.index_for(i, j));
					}
				}	
			}
			return found_indices;
		};
		static bool box_intersects_line(const GridIndex& i, const GridIndex& j, const Coordinate& u, const Coordinate& v, const Grid& grid){
			Coordinate top_left = grid.get_upper_left_corner(i, j);
			Coordinate top_right = grid.get_upper_right_corner(i, j);
			Coordinate bottom_left = grid.get_lower_left_corner(i, j);
			Coordinate bottom_right = grid.get_lower_right_corner(i, j);
			int orientation_sum = 0;
			orientation_sum += orientation_check(top_left, u, v);
			orientation_sum += orientation_check(top_right, u, v);
			orientation_sum += orientation_check(bottom_left, u, v);
			orientation_sum += orientation_check(bottom_right, u, v);
			return std::abs(orientation_sum) < 4;
		};

		// Checks the orientation of a point to a line (defined by a and b).
		static int orientation_check(const Coordinate& point, const Coordinate& a, const Coordinate& b){
			// see stack overflow implementation of orientation test:
			return sign(((b.lon - a.lon) * (point.lat - a.lat) - (b.lat - a.lat) * (point.lon - a.lon)));
		}

		// Returns 0 if value is 0, else the sign as +/- 1
		static inline int sign(const float orientation_val){
			if(orientation_val == 0.0){
				return 0;
			}
			return orientation_val > 0? 1: -1;
		}
};


} // namespace pf
