#pragma once

#include <algorithm>
#include <cmath>
#include <vector>

namespace pf
{

namespace statistics_util
{
using std::make_pair;
using std::pair;
using std::vector;

template <typename T>
T median(std::vector<T>& v)
{
	T n = v.size() / 2;
	std::nth_element(v.begin(), v.begin() + n, v.end());
	return v[n];
}

template <typename T>
T calcMin(std::vector<T> const& v)
{
	T min = std::numeric_limits<T>::max();
	for (T value : v) {
		min = std::min(value, min);
	}
	return min;
}
template <typename T>
T calcMax(std::vector<T> const& v)
{
	T max = std::numeric_limits<T>::lowest();
	for (T value : v) {
		max = std::max(value, max);
	}
	return max;
}

template <typename T>
T calcSum(std::vector<T> const& v)
{
	T sum = 0;
	for (T d : v) {
		sum += d;
	}
	return sum;
}

template <typename T>
T calcAverage(std::vector<T> const& v)
{
	return calcSum(v) / v.size();
}

template <typename T>
double calcVariance(std::vector<T> const& v)
{
	double average = calcAverage(v);

	double squared_diff_sum = 0;
	for (double d : v) {
		double diff = average - d;
		double squared_diff = diff * diff;
		squared_diff_sum += squared_diff;
	}

	return squared_diff_sum / v.size();
}

template <typename T>
double calcStandardDeviaton(std::vector<T> const& v)
{
	return sqrt(calcVariance(v));
}

} // namespace statistics_util

} // namespace pf