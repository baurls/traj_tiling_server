#include "pathfinder_ds.h"

#include "defs.h"
#include "edge_id_range.h"
#include "merge_sorted_unique.h"
#include "pathfinder.h"
#include "query.h"
#include "time_tracker.h"

#include <algorithm>
#include <set>
#include <utility>

namespace pf
{

PathfinderDS::PathfinderDS(Graph& graph, uint nr_of_threads, PathsDS&& paths_ds,
    InvertedIndicesVariant inverted_indices_variant, bool sortWRTEdgeStatus)
    : graph(graph)
    , paths_ds(paths_ds)
    , ch_traversal(graph)
    , index(createIndex(inverted_indices_variant))
    , path_ids_lists(nr_of_threads)
    , search_stacks(nr_of_threads)
{
	TimeTracker time_tracker;

	collected.assign(this->paths_ds.getNrOfPaths(), false);

	Print("Average number of CH-edges for a path:");
	Print(calculateAverageNofEdgesInPath(this->paths_ds));

	Print("InitEdgeToPathsInvertedIndices ...");
	time_tracker.start();
	initEdgeToPathsIndex();
	time_tracker.stop();
	Print(" ↳ Took " << time_tracker.getLastMeasurment() << " seconds");

	Print("Mark obsolete edges ...");
	time_tracker.start();
	markObsoleteEdges();
	time_tracker.stop();
	Print(" ↳ Took " << time_tracker.getLastMeasurment() << " seconds");

	if (sortWRTEdgeStatus) {
		Print("Mark tree edges ...");
		time_tracker.start();
		markTreeEdges();
		time_tracker.stop();
		Print(" ↳ Took " << time_tracker.getLastMeasurment() << " seconds");

		Print("Resort edges with respect to edge status ...");
		time_tracker.start();
		graph.sortWRTEdgeStatus(is_obsolete, is_tree_edge);
		time_tracker.stop();
		Print(" ↳ Took " << time_tracker.getLastMeasurment() << " seconds");
	}

	Print("Init BoundingBoxVectors...");
	time_tracker.start();
	initBoundingBoxVectors();
	time_tracker.stop();
	Print(" ↳ Took " << time_tracker.getLastMeasurment() << " seconds");

	// Time measurements are made inside initTopNodes
	Print("Init top nodes...");
	initTopNodes();

	//	Print("CountMaxIntersectionOfEdgesForPath ...");
	//	time_tracker.start();
	//	countMaxIntersectionOfEdgesForPath();
	//	time_tracker.stop();
	//	Print(" ↳ Took " << time_tracker.getLastMeasurment() << " seconds");

	Print("InitTimeIntervallVectors ...");
	time_tracker.start();
	initTimeIntervallVectors();
	time_tracker.stop();
	Print(" ↳ Took " << time_tracker.getLastMeasurment() << " seconds");
}

Graph const& PathfinderDS::getGraph() const
{
	return graph;
}

PathsDS const& PathfinderDS::getPathsDS() const
{
	return paths_ds;
}

Path const PathfinderDS::getPath(PathID path_id) const
{
	return paths_ds.getPath(path_id);
}

BoundingBox const& PathfinderDS::getEdgeBox(EdgeID edge_id) const
{
	return edge_boxes[edge_id];
}

BoundingBox const& PathfinderDS::getPathBox(PathID path_id) const
{
	return path_boxes[path_id];
}

BoundingBoxes const& PathfinderDS::getEdgeBoxes() const
{
	return edge_boxes;
}

BoundingBoxes const& PathfinderDS::getPathBoxes() const
{
	return path_boxes;
}

TimeIntervall const& PathfinderDS::getEdgeIntervall(EdgeID edge_id) const
{
	return edge_intervalls[edge_id];
}

TimeIntervall const& PathfinderDS::getREdgeIntervall(NodeID node_id) const
{
	return r_node_intervalls[node_id];
}

TimeSlots const& PathfinderDS::getEdgeSlots(EdgeID edge_id) const
{
	return edge_slots[edge_id];
}

TimeSlots const& PathfinderDS::getREdgeSlots(NodeID node_id) const
{
	return r_node_slots[node_id];
}

BoundingBox const& PathfinderDS::getRNodeBox(NodeID node_id) const
{
	return r_node_boxes[node_id];
}

BoundingBox PathfinderDS::getBoxOfAdjacentEdges(NodeID node_id) const
{
	BoundingBox box;
	for (auto direction : {Direction::Forward, Direction::Backward}) {
		for (auto const& edge : graph.getEdgesOf(node_id, direction)) {
			box.expand(edge_boxes[edge.id]);
		}
	}

	return box;
}

TimeIntervall PathfinderDS::getIntervallOfAdjacentEdges(NodeID node_id) const
{
	TimeIntervall intervall;
	for (auto direction : {Direction::Forward, Direction::Backward}) {
		for (auto const& edge : graph.getEdgesOf(node_id, direction)) {
			intervall.expand(edge_intervalls[edge.id]);
		}
	}

	return intervall;
}

TimeSlots PathfinderDS::getSlotsOfAdjacentEdges(NodeID node_id) const
{
	TimeSlots slots(false);
	for (auto direction : {Direction::Forward, Direction::Backward}) {
		for (auto const& edge : graph.getEdgesOf(node_id, direction)) {
			slots.expand(edge_slots[edge.id]);
		}
	}

	return slots;
}

NodeIDs PathfinderDS::getTopNodes(BoundingBox const& box) const
{
	return top_nodes_tree.search(box);
}

bool PathfinderDS::isContainedInPath(EdgeID edge_id)
{
	return index->isContainedInPath(edge_id);
}

TimeIntervall PathfinderDS::getTimeIntervall(EdgeID edge_id)
{
	return index->getTimeIntervall(edge_id);
}

auto PathfinderDS::getPathIntervalls(EdgeID edge_id) -> PathIntervalls
{
	return index->getPathIntervalls(edge_id);
}

void PathfinderDS::getPathIDs(EdgeIDs const& edge_ids, Query const& query)
{
#if defined(PATHS_ON_DISK_STATS)
	// to measure more cleanly
	index->clearCaches();
#endif

	if (getNrOfThreads() == 1) {
		getPathIDsSequential(edge_ids, query);
	} else {
		getPathIDsParallel(edge_ids, query);
	}
}

void PathfinderDS::getPathIDsSequential(
    EdgeIDs const& edge_ids, Query const& query)
{
	path_ids_lists.front().clear();
	if (query.isSpatialOnly()) {
		getPathIDsSequentialOnlySpatial(edge_ids);
	} else {
		getPathIDsSequentialWithTime(edge_ids, query);
	}
}

void PathfinderDS::getPathIDsSequentialOnlySpatial(EdgeIDs const& edge_ids)
{
	for (auto edge_id : edge_ids) {
		index->getPathIDsOnlySpatial(
		    edge_id, path_ids_lists.front(), search_stacks.front(), collected);
	}
}

void PathfinderDS::getPathIDsSequentialWithTime(
    EdgeIDs const& edge_ids, Query const& query)
{
	for (auto edge_id : edge_ids) {
		index->getPathIDs(edge_id, query, path_ids_lists.front(),
		    search_stacks.front(), collected);
	}
}

void PathfinderDS::getPathIDsParallel(
    EdgeIDs const& edge_ids, Query const& query)
{
#pragma omp parallel num_threads(getNrOfThreads())
	{
		path_ids_lists[omp_get_thread_num()].clear();
		if (query.isSpatialOnly()) {
			getPathIDsParallelOnlySpatial(edge_ids);
		} else {
			getPathIDsParallelWithTime(edge_ids, query);
		}

		PathIDs& path_ids = path_ids_lists[omp_get_thread_num()];

		// remove duplicates threadwise
		std::sort(path_ids.begin(), path_ids.end());
		auto new_end = std::unique(path_ids.begin(), path_ids.end());
		path_ids.erase(new_end, path_ids.end());
	}
}

void PathfinderDS::getPathIDsParallelOnlySpatial(EdgeIDs const& edge_ids)
{
#pragma omp for schedule(guided)
	for (uint i = 0; i < edge_ids.size(); i++) {
		EdgeID edge_id = edge_ids[i];
		auto& path_ids = path_ids_lists[omp_get_thread_num()];
		auto& search_stack = search_stacks[omp_get_thread_num()];

		index->getPathIDsOnlySpatial(
		    edge_id, path_ids, search_stack, collected);
	}
}

void PathfinderDS::getPathIDsParallelWithTime(
    EdgeIDs const& edge_ids, Query const& query)
{
#pragma omp for schedule(guided)
	for (uint i = 0; i < edge_ids.size(); i++) {
		EdgeID edge_id = edge_ids[i];
		auto& path_ids = path_ids_lists[omp_get_thread_num()];
		auto& search_stack = search_stacks[omp_get_thread_num()];

		index->getPathIDs(edge_id, query, path_ids, search_stack, collected);
	}
}

PathIDs PathfinderDS::removeDuplicatePathIDs() const
{
	if (getNrOfThreads() == 1) {
		PathIDs path_ids = path_ids_lists.front();

		// remove duplicates
		std::sort(path_ids.begin(), path_ids.end());
		auto new_end = std::unique(path_ids.begin(), path_ids.end());
		path_ids.erase(new_end, path_ids.end());

		return path_ids;
	} else {
		return merge_sorted_unique::inplace(path_ids_lists);
	}
}

bool PathfinderDS::isObsolete(EdgeID edge_id) const
{
	return is_obsolete[edge_id];
}

bool PathfinderDS::isTreeEdge(EdgeID edge_id) const
{
	return is_tree_edge[edge_id];
}

CHTraversal& PathfinderDS::traverse()
{
	return ch_traversal;
}

//
// private member functions
//

void PathfinderDS::initBoundingBoxVectors()
{
	initEdgeBoxes();
	initRNodeBoxes();
	initPathBoxes();
}

void PathfinderDS::initTimeIntervallVectors()
{
	initEdgeIntervalls();
	initRNodeIntervalls();
	initEdgeSlots();
	initRNodeSlots();
}

void PathfinderDS::initEdgeBoxes()
{
	// Set bounding boxes for all edges. Note though that for shortcuts the
	// bounding box might actually be larger. This is taken care of in a second
	// loop.
	edge_boxes.reserve(graph.getNumberOfEdges());
	for (EdgeID i = 0; i < (EdgeID)graph.getNumberOfEdges(); ++i) {
		auto const& edge = graph.getEdge(i);
		auto const& source = graph.getNode(edge.source);
		auto const& target = graph.getNode(edge.target);

		edge_boxes.emplace_back(source, target);
	}

	// push the bounding boxes up in the CH to fix the bounding boxes of
	// shortcuts
	auto push_to_parents_function = [&](EdgeID parent_id, EdgeID edge_id) {
		edge_boxes[parent_id].expand(edge_boxes[edge_id]);
	};
	ch_traversal.toParents(std::move(push_to_parents_function));
}

void PathfinderDS::initPathBoxes()
{
	// Set bounding boxes for all paths

	path_boxes.reserve(paths_ds.getNrOfPaths());
	for (PathID path_id = 0; path_id < (PathID)paths_ds.getNrOfPaths(); ++path_id) {
		auto const& path = paths_ds.getPath(path_id);
		
		// skip paths with no edge (since there's no bounding box) 
		if (path.getNoOfRootEdges() == 0){
			path_boxes.emplace_back(BoundingBox());
			continue;
		}
			
		// deal with first root edge
		auto edge_bb = edge_boxes[path.getEdge(0)];
		path_boxes.emplace_back(BoundingBox(edge_bb.min_coordinate, edge_bb.max_coordinate));

		// deal with all other root edges
		for(EdgeID root_edge_index = 1; root_edge_index < (EdgeID)path.getNoOfRootEdges(); ++root_edge_index){
			path_boxes[path_id].expand(edge_boxes[path.getEdge(root_edge_index)]);
		}
	}
}

void PathfinderDS::initEdgeIntervalls()
{
	// Set time intervalls for all edges.
	edge_intervalls.reserve(graph.getNumberOfEdges());
	for (EdgeID edge_id = 0; edge_id < (EdgeID)graph.getNumberOfEdges();
	     ++edge_id) {
		edge_intervalls.emplace_back(getTimeIntervall(edge_id));
	}

	// no push up to the shortcuts necessary
}

void PathfinderDS::initRNodeBoxes()
{
	// set initial boxes to just the own node coordinates
	r_node_boxes.reserve(graph.getNumberOfNodes());
	for (auto const& node : graph.getAllNodes()) {
		r_node_boxes.emplace_back(node.coordinate);
	}

	// push the values up the CH
	auto push_up_function = [&](Edge const& edge, Direction direction) {
		if (is_obsolete[edge.id]) {
			return;
		}

		auto tail_id = edge.getTail(direction);
		auto head_id = edge.getHead(direction);
		r_node_boxes[head_id].expand(r_node_boxes[tail_id]);
		r_node_boxes[head_id].expand(edge_boxes[edge.id]);
	};
	ch_traversal.up(std::move(push_up_function));
}

void PathfinderDS::initRNodeIntervalls()
{
	// set initial intervalls to invalid
	r_node_intervalls.assign(graph.getNumberOfNodes(), TimeIntervall());

	// push the time intervalls up the CH
	auto push_up_function = [&](Edge const& edge, Direction direction) {
		if (is_obsolete[edge.id]) {
			return;
		}

		auto tail_id = edge.getTail(direction);
		auto head_id = edge.getHead(direction);
		r_node_intervalls[head_id].expand(r_node_intervalls[tail_id]);
		r_node_intervalls[head_id].expand(edge_intervalls[edge.id]);
	};
	ch_traversal.up(std::move(push_up_function));
}

void PathfinderDS::initEdgeSlots()
{
	// see initEdgeIntervalls
	edge_slots.reserve(graph.getNumberOfEdges());
	for (EdgeID edge_id = 0; edge_id < (EdgeID)graph.getNumberOfEdges();
	     ++edge_id) {
		TimeSlots timeSlots = TimeSlots(false);
		timeSlots.expand(getTimeIntervall(edge_id));
		edge_slots.emplace_back(timeSlots);
	}

	// no push up to the shortcuts necessary
}

void PathfinderDS::initRNodeSlots()
{
	// see initRNodeIntervalls
	r_node_slots.assign(graph.getNumberOfNodes(), TimeSlots());

	auto push_up_function = [&](Edge const& edge, Direction direction) {
		if (is_obsolete[edge.id]) {
			return;
		}

		auto tail_id = edge.getTail(direction);
		auto head_id = edge.getHead(direction);
		r_node_slots[head_id].expand(r_node_slots[tail_id]);
		r_node_slots[head_id].expand(edge_slots[edge.id]);
	};
	ch_traversal.up(std::move(push_up_function));
}

void PathfinderDS::initTopNodes()
{
	TimeTracker time_tracker;

	Print("Collect top nodes...");
	time_tracker.start();

	auto remaining_nodes = graph.getContractionOrder();

	ch_traversal
	    .resetSeen(); // ... as we set the reset_seen flag to false in the loop
	while (!remaining_nodes.empty()) {
		// Get node with highest level
		auto max_node_id = remaining_nodes.back();
		top_nodes_tree.emplace(r_node_boxes[max_node_id], max_node_id);

		auto down_function = [&](Edge const& edge, Direction direction) {
			Unused(edge);
			Unused(direction);
			// We stop anyway if we saw the head node already. Thus, always
			// try to expand.
			return Result::Expand;
		};
		ch_traversal.down(std::move(down_function), max_node_id, false);

		// pop from remaining_nodes until we find an unseen node
		while (!remaining_nodes.empty() &&
		       ch_traversal.wasSeen(remaining_nodes.back())) {
			remaining_nodes.pop_back();
		}
	}

	time_tracker.stop();
	Print(" ↳ Took " << time_tracker.getLastMeasurment() << " seconds");

	Print("Build top nodes tree...");
	time_tracker.start();
	top_nodes_tree.build();
	time_tracker.stop();
	Print(" ↳ Took " << time_tracker.getLastMeasurment() << " seconds");
}

void PathfinderDS::initEdgeToPathsIndex()
{
	using PathEdgePair = std::pair<EdgeID, PathIntervall>;

#if defined(PATHS_ON_DISK)
	using PathEdgePairs = stxxl::VECTOR_GENERATOR<PathEdgePair>::result;
#else
	using PathEdgePairs = std::vector<PathEdgePair>;
#endif

	// compute pairs array size
	size_t pairs_size = 0;
	for (PathID path_id = 0; path_id < paths_ds.getNrOfPaths(); ++path_id) {
		pairs_size += paths_ds.getPathLength(path_id);
	}

	PathEdgePairs pairs;
	pairs.reserve(pairs_size);

	// fill pairs array
	size_t j = 0;
	for (PathID path_id = 0; path_id < paths_ds.getNrOfPaths(); ++path_id) {
		Path path = paths_ds.getPath(path_id);
		for (EdgeIndex edge_index = 0; edge_index < path.getEdges().size();
		     edge_index++) {
			PathIntervall path_intervall(
			    path_id, path.getTimeIntervallForEdge(edge_index));
			pairs.push_back(
			    PathEdgePair(path.getEdge(edge_index), path_intervall));
			j++;
		}
	}
	assert(pairs.size() == pairs_size);

	// sort descending wrt EdgeID
	std::sort(pairs.begin(), pairs.end(), std::greater<PathEdgePair>());

	index->init(graph.getNumberOfEdges(), pairs_size);

	size_t originalPairsSize = pairs_size;
	long i = pairs_size - 1; // point to last element
	for (EdgeID edge_id = 0; edge_id < (EdgeID)graph.getNumberOfEdges();
	     ++edge_id) {

		index->preAddEdge();

		while (i >= 0 && pairs[i].first == edge_id) {
			PathIntervall path_intervall = pairs[i].second;
			index->addPathIntervall(path_intervall);
			i--;
		}

		uint offset = originalPairsSize - i - 1;
		index->postAddEdge(offset);
	}

	assert(i == -1);
	index->finish();
	pairs.clear();
}

void PathfinderDS::markObsoleteEdges()
{
	is_obsolete.assign(graph.getNumberOfEdges(), false);

	std::size_t obsolete_count = 0;
	for (auto& edge : graph.getAllEdges()) {
		if (isContainedInPath(edge.id)) {
			continue;
		}

		auto lower_node = graph.getLowerNode(edge.id);

		std::size_t non_obsolete_count = 0;
		for (auto direction : {Direction::Forward, Direction::Backward}) {
			for (auto const& edge : graph.getUpEdgesOf(lower_node, direction)) {
				if (!is_obsolete[edge.id]) {
					++non_obsolete_count;
				}
			}
		}
		if (non_obsolete_count > 1) {
			++obsolete_count;
			is_obsolete[edge.id] = true;
		}
	}

	Print("We marked " << obsolete_count << " out of "
	                   << graph.getNumberOfEdges() << " edges as obsolete.");
}

void PathfinderDS::markTreeEdges()
{
	is_tree_edge.assign(graph.getNumberOfEdges(), true);

	std::size_t non_obsolete_count = 0;
	std::size_t non_tree_edges_count = 0;

	for (auto& edge : graph.getAllEdges()) {
		// tree edges shall be a subset of non-obsolete edges
		if (is_obsolete[edge.id]) {
			++non_tree_edges_count;
			is_tree_edge[edge.id] = false;
		}
	}

	for (auto& edge : graph.getAllEdges()) {

		// already handled
		if (is_obsolete[edge.id]) {
			continue;
		}
		non_obsolete_count++;

		auto lower_node = graph.getLowerNode(edge.id);

		std::size_t tree_edge_count_for_lower_node = 0;
		for (auto direction : {Direction::Forward, Direction::Backward}) {
			for (auto const& edge : graph.getUpEdgesOf(lower_node, direction)) {
				if (is_tree_edge[edge.id]) {
					++tree_edge_count_for_lower_node;
				}
			}
		}
		if (tree_edge_count_for_lower_node > 1) {
			++non_tree_edges_count;
			is_tree_edge[edge.id] = false;
		}
	}

	std::size_t tree_edge_count =
	    graph.getNumberOfEdges() - non_tree_edges_count;

	Print("We marked " << tree_edge_count << " out of " << non_obsolete_count
	                   << " non obsolete edges as tree edges.");
}

double PathfinderDS::calculateAverageNofEdgesInPath(
    PathsDS const& paths_ds) const
{
	long nof_edges_in_paths = 0;
	for (PathID path_id = 0; path_id < paths_ds.getNrOfPaths(); path_id++) {
		nof_edges_in_paths += paths_ds.getPathLength(path_id);
	}
	double avg_edges_in_path =
	    (double)nof_edges_in_paths / paths_ds.getNrOfPaths();
	return avg_edges_in_path;
}

void PathfinderDS::resetCollected()
{
	// possible optimization: iterate over the queried path_ids if its size is
	// small to reset faster
	std::fill(collected.begin(), collected.end(), 0);
}

uint PathfinderDS::getNrOfThreads() const
{
	// see constructor
	assert(path_ids_lists.size() == search_stacks.size());
	return path_ids_lists.size();
}

} // namespace pf
