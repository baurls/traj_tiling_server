#pragma once

#include "course.h"
#include "dijkstra.h"
#include "geometry.h"
#include "graph.h"
#include "partitioner_util.h"
#include "paths.h"
#include "paths_ds.h"
#include "random.h"
#include "time_tracker.h"

#include <vector>

namespace pf
{

namespace unit_tests
{
void testPathGenerator();
} // namespace unit_tests

class PathGenerator
{
public:
	PathGenerator(Graph const& graph, Random& random, uint nr_of_threads);

	// NOTE: If you add a new PathType you also have to manually increase the
	// number_of_path_types member variable.
	enum class PathType {
		// The following path creation type conducts a random walk where we
		// don't take an edge if we have already visited the target. The
		// arguments p_stop is the probability to stop doing a random walk in
		// each step.
		RandomWalk,
		// Calculates a shortest path from the provided source to a random
		// target.
		RandomShortest,
		// Chooses a random course and then always takes the edge which goes
		// the most in direction of this course.
		RandomCourse,
	};
	using PathTypes = std::vector<PathType>;

	// The following are functions to create paths
	Path getPath(PathType path_type, NodeID source = c::NO_NID);
	PathsDS getPaths(PathType path_type, std::size_t number);

	Path getStitchedPath(PathTypes const& path_types);
	PathsDS getStitchedPaths(PathTypes const& path_types, std::size_t number);

	Path getRandomPath();
	PathsDS getRandomPaths(std::size_t number);
	Path getRandomShortestPath(NodeID node_id, double maxDistance);
	PathsDS getRandomShortestPaths(std::size_t number, double max_distance);
	PathsDS getRandomShortestArtificiallyStitchedPaths(
	    std::size_t number, double max_distance);
	PathsDS getDistanceBasedStitchedPaths(
	    std::size_t number, double max_distance);

	PathType getRandomPathType();
	PathTypes getRandomPathTypes(std::size_t number);

	// With the following functions you can change the parameters
	void setPStop(float p_stop);

private:
	Graph const& graph;
	Random& random;
	Geometry geometry;

	const uint nr_of_threads;

	struct ThreadData {
		CHDijkstra ch_dijkstra;
		PathsDS paths_ds;
		std::unique_ptr<ShortestPathPartitioner> partitioner;
		Random random;

		ThreadData(Graph const& graph, Random& random)
		    : ch_dijkstra(graph), random(random.getSeed())
		{
			partitioner = partitioner_util::createPartitioner(
			    graph, PartitionerType::INCREMENTAL);
		};
	};
	std::vector<ThreadData> _thread_data;

	static std::size_t constexpr number_of_path_types = 3;

	// Parameters for the path creation functions
	float p_stop = 0.01;

	// Path creation functions for the different types
	Path getRandomWalkPath(NodeID source = c::NO_NID);
	Path getRandomShortestPath(NodeID source = c::NO_NID);
	Path getRandomShortestPath(double maxDistance);
	Path getRandomShortestPathEfficiently(double maxDistance);
	Path getRandomShortestCoursedPath(
	    NodeID source, double distance, int nofAttempts);
	Path getRandomShortestCoursedPath(NodeID source, double distance);
	Path getRandomCoursePath(
	    NodeID source = c::NO_NID, double max_distance = 25);

	Path getArtificiallyStitchedPath(Path const& ch_rep_shopa);
	Path getDistanceBasedStitchedPath(uint nof_subpaths, double distance);
	std::vector<Path> getDistanceBasedStitchedSubPaths(
	    uint nof_subpaths, double distance);

	void setRandomTimeIntervalls(Path& path, Random& random);
	void setRandomTimeIntervalls(Path& path, TimeType start, Random& random);

	auto _myThreadData() -> ThreadData&;

	friend void unit_tests::testPathGenerator();
};

} // namespace pf
