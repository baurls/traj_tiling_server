#pragma once

#include "IPathfinder.h"
#include "algorithm/edgeCandidateFinder.h"
#include "algorithm/refiner.h"
#include "basic_types.h"
#include "flags.h"
#include "pathfinder_ds.h"
#include "paths.h"
#include "query.h"
#include "stxxl_tracker.h"
#include "time_tracker.h"

#include "map"

namespace pf
{

namespace unit_tests
{
void testPathfinder();
void testConservativeFilter();
void testConservativeFilterGenerated();
void testConservativeFilterReal();
void testConservativeFilter(Graph& graph, PathsDS&& paths_ds);
} // namespace unit_tests

class Pathfinder : public IPathfinder
{
public:
	Pathfinder(PathfinderDS& ds, int nr_of_threads);
	PathIDs run(BoundingBox const& query_box);
	PathIDs run(Query const& query);

	void printTimes();

	std::map<Substep, TimeTracker> getSubstepTimeTrackers();
	StxxlTracker* getStxxlTracker();

private:
	PathfinderDS& ds;
	EdgeCandidateFinder edge_candidate_finder;
	Refiner refiner;

	// Reusable list
	EdgeIDs edge_candidates_total;

	std::map<Substep, TimeTracker> substepTimeTrackers;
	StxxlTracker stxxl_tracker;

	void initTimeTrackers();

	friend void unit_tests::testPathfinder();
};

namespace unit_tests
{
void checkTimeIntervallDifference(Pathfinder& pathfinder, Graph const& graph);
void checkTimeSlotDifference(Pathfinder& pathfinder, Graph const& graph);
void checkConservativeFilterDifference(Pathfinder& uncompressed_pathfinder,
    Pathfinder& compressed_pathfinder, Graph const& graph);
} // namespace unit_tests

} // namespace pf
