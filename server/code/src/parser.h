#pragma once

#include "basic_types.h"

#include <string>

namespace pf
{

namespace unit_tests
{
void testParser();
} // namespace unit_tests

class Parser
{
public:
	// The data will be filled into nodes and edges on calling the parse member
	// function.
	Parser(Nodes& nodes, Edges& edges);

	// Returns true iff parsing was successful.
	bool parse(const std::string& filename);

private:
	bool already_parsed = false;

	NodeID number_of_nodes;
	EdgeID number_of_edges;

	Nodes& nodes;
	Edges& edges;

	// Helper functions for parsing the graph file.
	void readHeader(std::ifstream& f);
	void readNumberOfNodesAndEdges(std::ifstream& f);
	void readNumberOfNodesAndEdgesBin(std::ifstream& f);
	template <typename T>
	T readBinary(std::ifstream& f);
	template <typename T>
	void SwapEnd(T& var);
	void printInt(char* charArray);
	void readNodes(std::ifstream& f);
	void readNodesBin(std::ifstream& f);
	void skipRead(std::ifstream& f);
	void readEdges(std::ifstream& f);
	void readEdgesBin(std::ifstream& f);
	bool parseNormal(const std::string& filename);
	bool parseBin(const std::string& filename);

	friend void unit_tests::testParser();
};

} // namespace pf
