#include "path_parser.h"

#include "defs.h"
#include "graph.h"

#include <cassert>
#include <fstream>
#include <iostream>
#include <iterator>
#include <math.h>
#include <sstream>
#include <string>
#include <vector>

namespace pf
{

PathParser::PathParser(Graph const& graph, PathsDS& paths_ds)
    : graph(graph), paths_ds(paths_ds), path_analyzer(graph)
{
	initOsmIdsToGraphIds();
}

void PathParser::initOsmIdsToGraphIds()
{
	OsmNodeId maxOsmId = 0;
	for (Node node : graph.getAllNodes()) {
		if (node.osm_id > maxOsmId) {
			maxOsmId = node.osm_id;
		}
	}

	for (uint i = 0; i < graph.getAllNodes().size(); i++) {
		Node node = graph.getAllNodes()[i];
		osmIdsToGraphIds[node.osm_id] = i;
	}
}

void PathParser::readOpenBracket(std::ifstream& f)
{
	std::string line;
	std::getline(f, line);

	if (line[0] != '{') {
		throw Exception("Expected \"{\"" + line);
	}
}

void PathParser::readTraces(std::ifstream& f)
{
	std::string line;
	std::getline(f, line);

	while (line[0] != '}') {
		readTrace(f);
		std::getline(f, line);
	}

	Print("Paths omitted:");
	Print(nof_paths_omitted);
}

void PathParser::readTrace(std::ifstream& f)
{
	std::string line;

	std::getline(f, line); // geoMeasurements
	std::vector<GeoMeasurement> geomeasurements = readGeoMeasurements(line);

	std::getline(f, line); // travNodesOsmIdsLine

	std::getline(f, line); // travNodesCHLvls

	std::vector<Factor> factors;

	while (!checkForTraceEnd(line)) {
		std::getline(f, line);

		Factor factor = readFactorHeader(line);
		std::getline(f, line);
		while (!checkForResPathEnd(line)) {
			readOsmEdgeIfPossible(line, factor.resPath);
			std::getline(f, line);
		}
		if (!checkForNoLastOsmEdge(line)) {
			readOsmEdgeIfPossible(line, factor.resPath);
		}

		factors.push_back(factor);
	}

	Path path(graph, geomeasurements, factors, osmIdsToGraphIds);

	if (path_analyzer.checkPath(path)) {
		paths_ds.addPath(path);
	} else {
		nof_paths_omitted++;
	}
}

Factor PathParser::readFactorHeader(const std::string line)
{

	std::vector<std::string> data = split(line, ',');
	assert(data.size() == 5);
	Factor factor;
	factor.neStartPos = std::stod(data[0]);
	factor.neStopPos = std::stod(data[1]);
	factor.smplStart = std::stod(data[2]);
	factor.smplStop = std::stod(data[3]);
	return factor;
}

void PathParser::readOsmEdgeIfPossible(
    const std::string line, std::vector<OsmEdge>& path)
{
	std::vector<std::string> data = split(line, ',');
	// rare special case where the factor is a path of length 0,
	// then the single node is written
	if (data.size() != 1) {
		OsmEdge osmEdge = readOsmEdge(line);
		path.push_back(osmEdge);
	}
}

OsmEdge PathParser::readOsmEdge(const std::string line)
{
	std::vector<std::string> data = split(line, ',');

	assert(data.size() == 2);
	OsmEdge osmEdge;
	osmEdge.src = std::stol(data[0]);
	osmEdge.tgt = std::stol(data[1]); // a string "xxx] is converted to xxx
	return osmEdge;
}
std::vector<GeoMeasurement> PathParser::readGeoMeasurements(
    const std::string line)
{
	std::string dataPart = line.substr(19, line.size() - 2 - 19);
	std::vector<std::string> data = split(dataPart, ',');
	assert(data.size() % 3 == 0);
	int nofMeasurements = data.size() / 3;
	std::vector<GeoMeasurement> geoMeasurements;
	for (int i = 0; i < nofMeasurements; i++) {

		GeoMeasurement geoMeasurement;
		Coordinate coordinate;
		geoMeasurement.coordinate = coordinate;

		int index = 3 * i;
		coordinate.lat = std::stod(data[index + 0]);
		coordinate.lon = std::stod(data[index + 1]);

		TimeType timestamp_or_number = std::stoi(data[index + 2]);
		// if there are no timestamps given, Martin's tool is writing an
		// increasing number instead
		if (timestamp_or_number == i) {
			geoMeasurement.timestamp = c::INVALID_TIME;
		} else {
			geoMeasurement.timestamp = timestamp_or_number;
		}

		geoMeasurements.push_back(geoMeasurement);
	}

	return geoMeasurements;
}

bool PathParser::checkForNoLastOsmEdge(const std::string line)
{
	if (line.size() < 2) {
		return false;
	} else {
		if (line.compare("],") == 0) {
			return true;
		}
		if (line.compare("]]") == 0) {
			return true;
		}
		if (line.size() < 3) {
			return false;
		} else {
			return line.compare("]],") == 0;
		}
	}
}

bool PathParser::checkForResPathEnd(const std::string line)
{
	if (line.size() < 2) {
		return false;
	} else {
		if (line.compare(line.size() - 2, 2, "],") == 0) {
			return true;
		}
		if (line.compare(line.size() - 2, 2, "]]") == 0) {
			return true;
		}
		if (line.size() < 3) {
			return false;
		} else {
			return line.compare(line.size() - 3, 3, "]],") == 0;
		}
	}
}

bool PathParser::checkForTraceEnd(const std::string line)
{
	if (line.size() < 2) {
		return false;
	} else {
		if (line.compare(line.size() - 2, 2, "]]") == 0) {
			return true;
		}
		if (line.size() < 3) {
			return false;
		} else {
			return line.compare(line.size() - 3, 3, "]],") == 0;
		}
	}
}

template <typename Out>
void PathParser::split(const std::string& s, char delim, Out result)
{
	std::stringstream ss(s);
	std::string item;
	while (std::getline(ss, item, delim)) {
		*(result++) = item;
	}
}

std::vector<std::string> PathParser::split(const std::string& s, char delim)
{
	std::vector<std::string> elems;
	split(s, delim, std::back_inserter(elems));
	return elems;
}

bool PathParser::parse(const std::string& filename)
{
	assert(!already_parsed);
	already_parsed = true;

	bool success = true;
	std::ifstream f;
	f.open(filename.c_str());

	if (f.is_open()) {
		try {
			readOpenBracket(f);
			readTraces(f);
			Print("Number of paths:");
			Print(paths_ds.getNrOfPaths());
		} catch (Exception e) {
			throw e;
			success = false;
		}

		f.close();
	} else {
		success = false;
	}

	return success;
}

} // namespace pf
