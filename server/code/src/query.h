#pragma once

#include "bounding_box.h"
#include "timeIntervall.h"
#include "timeSlots.h"

#include <vector>

namespace pf
{

struct Query {

	BoundingBox bounding_box;
	TimeIntervall time_intervall;
	TimeSlots time_slots;

	Query();
	Query(BoundingBox bounding_box);
	Query(BoundingBox bounding_box, TimeIntervall time_intervall,
	    TimeSlots time_slots);

	bool isSpatialOnly() const;
};

using Queries = std::vector<Query>;

} // namespace pf
