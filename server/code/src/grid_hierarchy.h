#pragma once

#include "basic_types.h"
#include "defs.h"
#include "graph.h"
#include "paths.h"
#include "paths_ds.h"
#include "bounding_box.h"
#include "grid.h"

/*
	# # # # # # # # # # # # # # # # # # # # # # # # # #
	#                                                 #
	#   Created by Lukas Baur, Master Thesis 2021.    #
	#                                                 #
	#   Supervisor: T. Rupp, Prof. S. Funke           #
	#   Institute:  FMI (University of Stuttgart)     #
	#                                                 #
	# # # # # # # # # # # # # # # # # # # # # # # # # #
*/



namespace pf
{

//
// Member functions of GridHierarchy
//
// level 0:
//              j
//		   ______________n = 2^q
//        |              |
//        |              |
//    i   |              |
//        |              |
//  2^p=m |______________|
// 
//				


class GridHierarchyBuilder{
	public:
		// constructor
		GridHierarchyBuilder(const GridIndex p, const GridIndex q, const GridIndex level, const BoundingBox& point_bb);
		
		// methods
		void calculate_cell_covering_pyramide_recursively(Edge const& edge, Graph& graph) const;
		CellCovering calculate_next_level_indices(const CellCovering& old_indices, const GridIndex n) const;

		Grid get_level0_grid() const{
			return start_grid;
		}

	private:
		GridHierarchyBuilder();

		// attributes
		const GridIndex p;
		const GridIndex q;
		const GridIndex level;
		Grid start_grid;

		// util-methods
		CellCoverings calculate_for_non_shortcut_edge(const Edge& edge, const Graph& graph) const;
		CellCoverings calculate_merged_pyramide(const Edge& edge1, const Edge& edge2) const;
		CellCovering merge_cell_covering(const CellCovering& cover1, const CellCovering& cover2) const;
};

class GridHierarchyAssigner{
	public:
		// constructor
		GridHierarchyAssigner(const GridIndex p, const GridIndex q, const GridIndex level, const PathsDS& paths_ds, Graph& graph);
		void create_hierarchy();
		Grid get_level0_grid() const{
			return grid_hierarchy_builder.get_level0_grid();
		}
		
	private:
		GridHierarchyAssigner();

		// attributes
		const GridHierarchyBuilder grid_hierarchy_builder;
		const PathsDS& paths_ds;
		Graph& graph;
		
};

} // namespace pf
