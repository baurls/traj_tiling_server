#include "grid_stack.h"
#include "grid.h"
#include "defs.h"
#include <math.h>
#include "basic_types.h"

/*
	# # # # # # # # # # # # # # # # # # # # # # # # # #
	#                                                 #
	#   Created by Lukas Baur, Master Thesis 2021.    #
	#                                                 #
	#   Supervisor: T. Rupp, Prof. S. Funke           #
	#   Institute:  FMI (University of Stuttgart)     #
	#                                                 #
	# # # # # # # # # # # # # # # # # # # # # # # # # #
*/


namespace pf
{





//
// Member functions of GridStack
//
// level 0:
//                   j
//    		   ______________n = 2^q
//            |              |
//            |              |
//        i   |              |
//            |              |
//      2^p=m |______________|
//
//
// level 1:
//                   j
//	     	   _________n = 2^(q-1)
//             |        |
//         i   |        |
//   2^(p-1)=m |________|
// 
//				

	Grids calculate_grid_stack(const Grid& initial_grid, const GridIndex level){
		// create datastructure
		Grid inital_grid_copy = initial_grid.clone();
		Grids grid_stack;

		// copy level 0 grid
		assert(level > 0);
		grid_stack.push_back(inital_grid_copy);

		// deal with l-1 remaining grids
		for(GridIndex i=1; i < level; i++){ 
			Grid last_grid = grid_stack[i-1];
			// get copy with decreased level and size
			grid_stack.push_back(last_grid.get_increased_level_copy());
		}
		return grid_stack;
	}

	// --- GridStack ----------

	GridStack::GridStack(const Grid& g, const GridIndex level)
		: grids(calculate_grid_stack(g, level))
		, level(level)
	{
	}

	GridStack::GridStack(){}

	Grid GridStack::get_grid(GridIndex request_level) const{
		assert(request_level < level);
		return grids[request_level];
	}
	

} // namespace pf
