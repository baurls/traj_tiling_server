#pragma once

#include "../basic_types.h"
#include "../bounding_box.h"
#include "../flags.h"
#include "../geometry.h"
#include "../pathfinder_ds.h"

#include <algorithm>

namespace pf
{

class Refiner
{

public:
	Refiner(PathfinderDS& ds, int nr_of_threads)
	    : ds(ds)
	    , graph(ds.getGraph())
	    , geometry(graph)
	    , _thread_data(nr_of_threads)
	    , cut_flags(graph.getNumberOfEdges(), CutFlag::Unknown)
	    , seen_flags(graph.getNumberOfEdges(), SeenFlag::Unseen)
	{
	}

	void refineCandidates(
	    EdgeIDs& edge_candidates_total, BoundingBox const& query_box)
	{
#pragma omp parallel num_threads(_thread_data.size())
		{
			seen_flags.reset(_myThreadData().seen_changed_flags);
			cut_flags.reset(_myThreadData().cut_changed_flags);
		}

#pragma omp parallel for num_threads(_thread_data.size()) schedule(guided)
		for (uint i = 0; i < edge_candidates_total.size(); i++) {
			checkForIntersectionDFS(query_box, edge_candidates_total[i]);
		}

		auto no_cut = [&](EdgeID edge_id) {
			if (cut_flags[edge_id] == CutFlag::Intersection) {
				return false;
			} else if (cut_flags[edge_id] == CutFlag::NoIntersection) {
				return true;
			} else {
				throw Exception(
				    "After checkForIntersectionDFS the CutFlag should be set.");
			}
		};

		auto new_end = std::remove_if(
		    edge_candidates_total.begin(), edge_candidates_total.end(), no_cut);
		edge_candidates_total.erase(new_end, edge_candidates_total.end());
	}

private:
	PathfinderDS& ds;
	Graph const& graph;
	Geometry geometry;

	// Purpose of cut_flags:
	// * In refineEdgeCandidates we use them to keep the search state between
	// the DFSs of the single candidate edges.

	struct ThreadData {
		// Reusable stack for checkForIntersectionDFS
		EdgeIDs dfs_stack;

		std::vector<std::size_t> seen_changed_flags;
		std::vector<std::size_t> cut_changed_flags;
	};
	std::vector<ThreadData> _thread_data;

	enum class CutFlag { Unknown, Intersection, NoIntersection };
	SimpleFlags<CutFlag> cut_flags;

	enum class SeenFlag { Unseen, Seen };
	SimpleFlags<SeenFlag> seen_flags;

	void checkForIntersectionDFS(
	    BoundingBox const& query_box, EdgeID edge_candidate)
	{
		EdgeIDs& dfs_stack = _myThreadData().dfs_stack;
		dfs_stack.clear();
		dfs_stack.push_back(edge_candidate);

		while (!dfs_stack.empty()) {
			auto edge_id = dfs_stack.back();

			if (seen_flags[edge_id] == SeenFlag::Unseen) {
				intersect(edge_id, query_box);
				seen_flags.set(edge_id, SeenFlag::Seen,
				    _myThreadData().seen_changed_flags);
			}

			switch (cut_flags[edge_id]) {
			case CutFlag::Intersection: {
				for (auto stack_edge_id : dfs_stack) {
					cut_flags.set(stack_edge_id, CutFlag::Intersection,
					    _myThreadData().cut_changed_flags);
				}
				return;
			}
			case CutFlag::NoIntersection: {
				if (dfs_stack.size() == 1) {
					return;
				}

				auto parent_id =
				    dfs_stack.rbegin()[1]; // second to last element
				auto child_id1 = graph.getEdge(parent_id).child_edge1;
				auto child_id2 = graph.getEdge(parent_id).child_edge2;

				if (edge_id == child_id1) {
					dfs_stack.back() = child_id2;
				} else if (edge_id == child_id2) {
					cut_flags.set(parent_id, CutFlag::NoIntersection,
					    _myThreadData().cut_changed_flags);
					dfs_stack.pop_back();
				} else {
					throw Exception(
					    "Element on stack should be one of the children.");
				}
				break;
			}
			case CutFlag::Unknown:
				dfs_stack.push_back(graph.getEdge(edge_id).child_edge1);
				break;
			default:
				throw Exception("CutFlag is None where it shouldn't be.");
			}
		}
	}
	void intersect(EdgeID edge_id, BoundingBox const& query_box)
	{
		auto const& edge = graph.getEdge(edge_id);
		auto source_id = graph.getEdge(edge_id).source;
		auto target_id = graph.getEdge(edge_id).target;
		auto source_vertex = geometry.getVertex(source_id);
		auto target_vertex = geometry.getVertex(target_id);

		if (!edge.isShortcut()) {
			if (geometry.intersect(query_box, edge_id)) {
				cut_flags.set(edge_id, CutFlag::Intersection,
				    _myThreadData().cut_changed_flags);
			} else {
				cut_flags.set(edge_id, CutFlag::NoIntersection,
				    _myThreadData().cut_changed_flags);
			}
		}

		if (geometry.nodeInBox(source_vertex, query_box) ||
		    geometry.nodeInBox(target_vertex, query_box)) {

			cut_flags.set(edge_id, CutFlag::Intersection,
			    _myThreadData().cut_changed_flags);
		} else {
			auto edge_box = ds.getEdgeBox(edge_id);
			edge_box.intersect(query_box);
			if (edge_box.isValid()) {
				// Do nothing, the default is already CutFlag::Unknown
				// and we may not overwrite the value because it could
				// have been already set by another thread which analyzed
				// its child edges
			} else {
				cut_flags.set(edge_id, CutFlag::NoIntersection,
				    _myThreadData().cut_changed_flags);
			}
		}
	}

	auto _myThreadData() -> ThreadData&
	{
		return _thread_data[omp_get_thread_num()];
	}
};

} // namespace pf