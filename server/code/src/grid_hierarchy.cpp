#include "grid.h"
#include "defs.h"
#include <math.h>
#include "basic_types.h"
#include "grid_hierarchy.h"
#include "intersection_checker.h"

/*
	# # # # # # # # # # # # # # # # # # # # # # # # # #
	#                                                 #
	#   Created by Lukas Baur, Master Thesis 2021.    #
	#                                                 #
	#   Supervisor: T. Rupp, Prof. S. Funke           #
	#   Institute:  FMI (University of Stuttgart)     #
	#                                                 #
	# # # # # # # # # # # # # # # # # # # # # # # # # #
*/


namespace pf
{

//
// Member functions of GridHierarchy
//
// level 0:
//              j
//		   ______________n = 2^q
//        |              |
//        |              |
//    i   |              |
//        |              |
//  2^p=m |______________|
// 
//			

	// --- Util functions ----------

	void remove_duplicates_and_sort(CellCovering& vec){
		std::sort( vec.begin(), vec.end() );
		vec.erase( std::unique( vec.begin(), vec.end() ), vec.end() );
	}

	inline void append_if_different(GlobalGridIndex& last, CellCovering& vec, const GlobalGridIndex& current){
		if(last != current){
			vec.push_back(current);
			last = current;
		}
	}

	// --- GridHierarchyBuilder ----------

	GridHierarchyBuilder::GridHierarchyBuilder(const GridIndex p, const GridIndex q, const GridIndex level, const BoundingBox& point_bb)
		: p(p)
		, q(q)
		, level(level)
		, start_grid(Grid(pow(2, p), pow(2, q), point_bb))
	{
		// check level depth
		assert(level < p);
		assert(level < q);
		// max-size is 2^15 (which is already quite huge =  32768 ^ 2 = 1073741824 cells) 
		assert(p < 15);
		assert(q < 15);
	}
	
	/*
		Given an edge, we calculate and its CellCovering for all hierachy levels. 
		Calculation is skipped, if there is a value set already.
		In case of a sortcut edge, the values are calculated based on the 
	*/
	void GridHierarchyBuilder::calculate_cell_covering_pyramide_recursively(Edge const& edge, Graph& graph) const{
		// skip, if value already calculated.
		if(edge.has_cell_covering()){
			return;
		}
		if(edge.isShortcut()){
			// calculate for both children and then merge cell-overlay
			Edge const& child1 = graph.getEdge(edge.child_edge1);
			Edge const& child2 = graph.getEdge(edge.child_edge2);
			calculate_cell_covering_pyramide_recursively(child1, graph);
			calculate_cell_covering_pyramide_recursively(child2, graph);
			auto merged_coverings = calculate_merged_pyramide(child1, child2);
			graph.set_edge_cell_covering(edge.id, merged_coverings);
		}
		else{
			// is a level-0 edge, create hierarchy from scratch
			auto cell_coverings  = calculate_for_non_shortcut_edge(edge, graph);
			graph.set_edge_cell_covering(edge.id, cell_coverings);
		}
	}

	/*
		Given the two edges of a shortcut edge, having both the CellCoverings
		calculated, we constuct the shortcut edge covering by merging the two
		pyramide-results.
	*/
	CellCoverings GridHierarchyBuilder::calculate_merged_pyramide(const Edge& edge1, const Edge& edge2) const{
		CellCoverings hierarchy = CellCoverings();
		for(long unsigned int i =0; i < edge1.cell_coverings.size(); i++){
			hierarchy.push_back(merge_cell_covering(edge1.cell_coverings[i], edge2.cell_coverings[i]));
		}
		return hierarchy;
	}


	CellCovering GridHierarchyBuilder::merge_cell_covering(const CellCovering& cover1, const CellCovering& cover2) const{
		CellCovering merge = CellCovering();
		long unsigned int j = 0;
		GlobalGridIndex last = -1;
		for(long unsigned int i = 0; i < cover1.size(); i++){
			while (cover2[j] < cover1[i] and j < cover2.size()){
				append_if_different(last, merge, cover2[j]);
				j++;
			}
			append_if_different(last, merge, cover1[i]);
		}

		// append rest of second list
		while (j < cover2.size()){
			append_if_different(last, merge, cover2[j]);
			j++;
		}
		return merge;
	}

	
	CellCoverings GridHierarchyBuilder::calculate_for_non_shortcut_edge(const Edge& edge, const Graph& graph) const{
		CellCoverings hierarchy = CellCoverings();
		
		// calucluate for lowest grid level
		auto level0_covering = NaiveIntersectionChecker::return_intersected_cells(graph, edge, start_grid);
		hierarchy.push_back(level0_covering);
		
		// calculate for higher grid level
		auto last_level_covering = level0_covering;
		auto m = start_grid.m;
		auto n = start_grid.n;
		for(long unsigned int i=1; i < level; i++){
			auto level_i_covering = calculate_next_level_indices(last_level_covering, n);
			hierarchy.push_back(level_i_covering);

			// prepare for next round
			last_level_covering = level_i_covering;
			m /= 2;
			n /= 2;
		}

		return hierarchy;
	}

	/*
		Transfers the indices of level i to the next grid of level i+1:
		
		      level i:
	                     j
		      ________________________n
	          |0 |1 |2 |3 |4 |5 |6 |7 |
	          |__|__|__|__|__|__|__|__|
	          |8 |9 |10|11|12|13|14|15|
	   i      |__|__|__|__|__|__|__|__|
	          |16|17|18|19|20|21|22|23|
	          |__|__|__|__|__|__|__|__|
	          |24|25|26|27|28|29|30|31|
	       m  |__|__|__|__|__|__|__|__|


		      level i+1:

	                      j
		      _____________________0.5n
	          |     |     |     |     |
	          | 0   | 1   | 2   | 3   |
	          |     |     |     |     |
	    i     |_____|_____|_____|_____|
	          |     |     |     |     |
	          | 4   | 5   | 6   | 7   |
	          |     |     |     |     |
	    0.5m  |_____|_____|_____|_____|

	*/
	CellCovering GridHierarchyBuilder::calculate_next_level_indices(const CellCovering& old_indices, const GridIndex n) const{
		CellCovering new_indices  = CellCovering();
		for(auto old_index: old_indices){
			GridIndex old_j = old_index % n;
			GridIndex old_i = (old_index - old_j) / n; 
			
			GridIndex new_i = old_i / 2;
			GridIndex new_j = old_j / 2;

			GlobalGridIndex new_index = new_i * (n/2) + new_j;
			new_indices.push_back(new_index);
		}
		remove_duplicates_and_sort(new_indices);
		return new_indices;

	}

	

	// --- GridHierarchyAssigner ----------
	GridHierarchyAssigner::GridHierarchyAssigner(const GridIndex p, const GridIndex q, const GridIndex level, const PathsDS& paths_ds, Graph& graph):
	paths_ds(paths_ds),
	graph(graph),
	grid_hierarchy_builder(p, q, level, BoundingBox(graph)){
	}

	void GridHierarchyAssigner::create_hierarchy(){
		for(long int i=0; i < paths_ds.getNrOfPaths(); i++){
		auto paths = paths_ds.getPath(i).getEdges();
			for(EdgeID edge_id : paths){
				const Edge& edge = graph.getEdge(edge_id);
				grid_hierarchy_builder.calculate_cell_covering_pyramide_recursively(edge, graph);
			}
		}
	}
	

} // namespace pf
