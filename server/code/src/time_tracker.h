#pragma once

#include "defs.h"
#include "statistics_util.h"

#include <algorithm>
#include <chrono>
#include <cmath>
#include <iomanip>
#include <vector>

class TimeTracker
{
private:
	using Measurement = double;
	using TimePoint = std::chrono::steady_clock::time_point;

	// XXX: Ugly non-C++17 fix for inline static constexpr variable.
	TimePoint invalidTime()
	{
		return TimePoint::min();
	}

	std::vector<Measurement> measurements;

	TimePoint start_time;

public:
	TimeTracker()
	{
		start_time = invalidTime();
	}

	void start()
	{
		assert(start_time == invalidTime());

		start_time = std::chrono::steady_clock::now();
	}

	void stop()
	{
		using namespace std::chrono;

		assert(start_time != invalidTime());

		auto now = steady_clock::now();
		auto measurement = duration_cast<microseconds>(now - start_time);
		measurements.emplace_back((double)measurement.count() / (1000 * 1000));

		start_time = invalidTime();
	}

	double getLastMeasurment()
	{
		assert(!measurements.empty());
		return measurements.back();
	}

	void clear()
	{
		measurements.clear();
		start_time = invalidTime();
	}

	double calculateMean()
	{
		return pf::statistics_util::calcAverage(measurements);
	}

	void printSummary()
	{

		Measurement mean;
		Measurement median;
		Measurement stddev;

		if (measurements.empty()) {
			Print("No measurements taken, and therefore no summary necessary.");
			return;
		}

		if (measurements.size() == 1) {
			mean = measurements.front();
			Print("Mean duration of experiment: " << mean << " s");
			return;
		}

		mean = calculateMean();
		stddev = pf::statistics_util::calcStandardDeviaton(measurements);
		median = pf::statistics_util::median(measurements);

		Print("Mean duration of experiment: " << mean << " s");
		//		Print("Standard deviation of experiment duration: " << stddev <<
		//"
		// s");
		//		Print("Median duration of experiment: " << median << " s");

		// Avoid unused warnings for non-verbose compilation.
		Unused(mean);
		Unused(median);
		Unused(stddev);
	}
};
