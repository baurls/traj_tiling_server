#pragma once

#include "geometry.h"
#include "graph.h"
#include "io/export_types.h"

#include <cstdint>
#include <string>
#include <unordered_map>
#include <vector>

/*
	# # # # # # # # # # # # # # # # # # # # # # # # # #
	#                                                 #
	#   Created by Lukas Baur, Master Thesis 2021.    #
	#                                                 #
	#   Supervisor: T. Rupp, Prof. S. Funke           #
	#   Institute:  FMI (University of Stuttgart)     #
	#                                                 #
	# # # # # # # # # # # # # # # # # # # # # # # # # #
*/


namespace pf
{
	using CountRange = unsigned short int;
	using CounterVector = std::vector<CountRange>;
	using RelCounterVector = std::vector<float>;
	
	class HeatmapBuilder
	{
	public:
		HeatmapBuilder(Graph const &graph, PathsDS const &paths_ds);
		void print() const;
		CounterVector get_absolute_nodes_counter() const;
		RelCounterVector get_truncated_rel_nodes_counter(size_t) const;
		

	private:
		Graph const &graph;
		PathsDS const &paths_ds;
		};

} // namespace pf
