#pragma once

#include "defs.h"
#include "timeSlots.h"

namespace pf
{

enum class ExportType{TRAJECORIES, SEGMENT_GRAPH, HEATMAP, BATCHED_PLAIN_INORDER,BATCHED_PLAIN_LEVELORDER, BATCHED_EDGEWISE, PRUNNING, INVALID};
enum class BatchType{PLAIN_INORDER, EDGE_WISE, PLAIN_LEVELORDER, INVALID};

class Options
{
private:
	// Flag for parsing intervals. If true, the timestamps will
	// be exported for each path.
	bool parse_intervals;

	// The time interval for trajectory calculation. Only trajectories that have
	// time stamps in this interval will be counted.
	TimeIntervall time_interval;

	// The time slots for time interval calculation. This bitstring sets the
	// weekdays that should be included.
	TimeSlots time_slots;

	// Flag for packing paths. If true, paths will stay packed, else they will
	// be unpacked completly.
	bool packing;

	// Flag for packing paths to a certain level. If packing is set to true and
	// level >= 0, the paths will be (un-)packed to this level.
	Level ch_level;

	// The map zoom level. Will be used to calculate the path levels
	// corresponding to this zoom value.
	int map_zoom;

	// The export type of returned request types 
	ExportType export_type;

	// Flag for prunning paths. If true, only high level edges (according to cutoff_level) are kept
	// others will be ignored
	bool prunning;

	// The cutoff level For edges to retrieve.
	// if prunning is activated, only edges larger than the cutoff-level are kept.
	int cutoff_level;

public:
	// Creates an Option instance and sets its values to the default values.
	Options()
	{
		time_interval = c::FULL_INTERVALL;
		time_slots = TimeSlots(true);
		parse_intervals = false;
		packing = true;		
		prunning = true;
		ch_level = c::NO_LEVEL;
		cutoff_level = c::NO_LEVEL;
		map_zoom = c::NO_ZOOM;
		export_type = ExportType::TRAJECORIES;
	}

	void setParseIntervals(bool const& flag)
	{
		parse_intervals = flag;
	}

	bool getParseIntervals() const
	{
		return parse_intervals;
	}

	void setInterval(TimeIntervall const& interval)
	{
		time_interval = interval;
	}

	TimeIntervall getInterval() const
	{
		return time_interval;
	}

	void setSlots(TimeSlots const& slots)
	{
		time_slots = slots;
	}

	TimeSlots getSlots() const
	{
		return time_slots;
	}

	void setLevel(Level const& level)
	{
		ch_level = level >= 0 ? level : c::NO_LEVEL;
	}

	Level getLevel() const
	{
		return ch_level;
	}

	void setCutoffLevel(Level const& level)
	{
		cutoff_level = level >= 0 ? level : c::NO_LEVEL;
	}

	Level getCutoffLevel() const
	{
		return cutoff_level;
	}

	void setPrunning(bool const& flag)
	{
		prunning = flag;
	}

	bool getPrunning() const
	{
		return prunning;
	}

	void setPacking(bool const& flag)
	{
		packing = flag;
	}

	bool getPacking() const
	{
		return packing;
	}

	void setZoom(int zoom)
	{
		map_zoom = zoom >= 0 ? zoom : c::NO_ZOOM;
	}

	int getZoom() const
	{
		return map_zoom;
	}

	void setExportType(std::string type)
	{
		if(type == "trajectories"){
			export_type =  ExportType::TRAJECORIES;
		}		
		else if(type == "segment_graph"){
			export_type = ExportType::SEGMENT_GRAPH;
		}
		else if(type == "heatmap"){
			export_type = ExportType::HEATMAP;
		}
		else if(type == "batch_plain_inorder"){
			export_type = ExportType::BATCHED_PLAIN_INORDER;
		}
		else if(type == "batch_plain_levelorder"){
			export_type = ExportType::BATCHED_PLAIN_LEVELORDER;
		}
		else if(type == "batch_edgewise_queue"){
			export_type = ExportType::BATCHED_EDGEWISE;
		}
		else if(type == "prunning"){
			export_type = ExportType::PRUNNING;
		}
		else{
			export_type = ExportType::INVALID;
		}
	}

	ExportType getExportType()
	{
		return export_type;
	}
};

} // namespace pf
