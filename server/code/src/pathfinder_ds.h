#pragma once

#include "bounding_box.h"
#include "ch_traversal.h"
#include "edge_id_range.h"
#include "graph.h"
#include "index/IIndex.h"
#include "index/indices.h"
#include "interval_tree.h"
#include "path_intervall.h"
#include "paths.h"
#include "paths_ds.h"
#include "query.h"
#include "r_tree.h"
#include "timeIntervall.h"
#include "timeSlots.h"

#include <cstdint>
#include <functional>
#include <mutex>
#include <omp.h>
#include <vector>

namespace pf
{

namespace unit_tests
{
void testPathfinderDS();
void testEdgeSorting();
} // namespace unit_tests

class PathfinderDS
{
private:
	using Result = CHTraversal::Result;

public:
	
	PathfinderDS(Graph& graph, uint nr_of_threads, PathsDS&& paths_ds,
	    InvertedIndicesVariant inverted_indices_variant =
#if defined(PATHS_ON_DISK)
	        InvertedIndicesVariant::VectorOnDisk,
#else
	        InvertedIndicesVariant::Vector,
#endif
	    bool sortWRTEdgeStatus = true);

	PathfinderDS(const PathfinderDS&) = default;
	PathfinderDS(PathfinderDS&&) = default;
	PathfinderDS& operator=(const PathfinderDS&) = default;
	PathfinderDS& operator=(PathfinderDS&&) = default;

	virtual ~PathfinderDS()
	{
	}

	Graph const& getGraph() const;
	PathsDS const& getPathsDS() const;
	Path const getPath(PathID path_id) const;

	BoundingBoxes const& getEdgeBoxes() const;
	BoundingBoxes const& getPathBoxes() const;
	BoundingBox const& getEdgeBox(EdgeID edge_id) const;
	BoundingBox const& getRNodeBox(NodeID node_id) const;
	BoundingBox const& getPathBox(PathID path_id) const;
	TimeIntervall const& getEdgeIntervall(EdgeID edge_id) const;
	TimeIntervall const& getREdgeIntervall(EdgeID edge_id) const;
	TimeSlots const& getEdgeSlots(EdgeID edge_id) const;
	TimeSlots const& getREdgeSlots(NodeID node_id) const;
	BoundingBox getBoxOfAdjacentEdges(NodeID node_id) const;
	TimeIntervall getIntervallOfAdjacentEdges(NodeID node_id) const;
	TimeSlots getSlotsOfAdjacentEdges(NodeID node_id) const;

	NodeIDs getTopNodes(BoundingBox const& box) const;

	TimeIntervall getTimeIntervall(EdgeID edge_id);
	bool isContainedInPath(EdgeID edge_id);
	PathIntervalls getPathIntervalls(EdgeID edge_id);

	void getPathIDs(EdgeIDs const& edge_ids, Query const& query);

	PathIDs removeDuplicatePathIDs() const;

	bool isObsolete(EdgeID edge_id) const;

	bool isTreeEdge(EdgeID edge_id) const;

	void resetCollected();

	CHTraversal& traverse();

private:
	Graph& graph;
	PathsDS paths_ds;
	CHTraversal ch_traversal;

	std::unique_ptr<IIndex> index;

	// Bounding boxes
	BoundingBoxes edge_boxes;   // BBS
	BoundingBoxes r_node_boxes; // BBV
	BoundingBoxes path_boxes;   // for prunning the search (check overlap with view box)

	TimeIntervalls edge_intervalls;
	TimeIntervalls r_node_intervalls;
	TimeSlotsVector edge_slots;
	TimeSlotsVector r_node_slots;

	// Nodes from which we can explore the whole graph on a down search

	RTree<NodeID> top_nodes_tree;

	std::vector<bool> is_obsolete;
	std::vector<bool> is_tree_edge;

	std::vector<PathIDs> path_ids_lists;

	std::vector<NodeIDs> search_stacks;

	// flags for collecting the path_ids in getPathIDs
	// possible extension on 3 values: collected, rejected, unseen
	std::vector<char> collected;

	void initBoundingBoxVectors();
	void initTimeIntervallVectors();
	void initEdgeBoxes();
	void initRNodeBoxes();
	void initPathBoxes();

	void initEdgeIntervalls();
	void initRNodeIntervalls();
	void initEdgeSlots();
	void initRNodeSlots();

	void initTopNodes();

	void initEdgeToPathsIndex();

	void initTimeIntervalls();

	void markObsoleteEdges();

	void markTreeEdges();

	friend void unit_tests::testPathfinderDS();

	void getPathIDsSequential(EdgeIDs const& edge_ids, Query const& query);
	void getPathIDsSequentialOnlySpatial(EdgeIDs const& edge_ids);
	void getPathIDsSequentialWithTime(
	    EdgeIDs const& edge_ids, Query const& query);

	void getPathIDsParallel(EdgeIDs const& edge_ids, Query const& query);
	void getPathIDsParallelOnlySpatial(EdgeIDs const& edge_ids);
	void getPathIDsParallelWithTime(
	    EdgeIDs const& edge_ids, Query const& query);

	double calculateAverageNofEdgesInPath(PathsDS const& paths_ds) const;

	void pushIfNotYetCollected(PathIDs& path_ids, PathID path_id);

	uint getNrOfThreads() const;
};

} // namespace pf
