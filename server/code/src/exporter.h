#pragma once

#include "geometry.h"
#include "graph.h"
#include "io/export_types.h"
#include "options.h"
#include "segment_splitter.h"
#include "heatmap_builder.h"
#include "flags.h"
#include "pathfinder_ds.h"
#include "grid_stack.h"
#include "level_extractor.h"

#include <cstdint>
#include <string>
#include <unordered_map>
#include <vector>

namespace pf
{

	namespace unit_tests
	{
		void testExporter();
	} // namespace unit_tests

	class Exporter
	{
	public:
		Exporter(Graph const& graph);

		// original exporting
		std::string exportTrajectories(ExportData const& data, Options const& options) const;
		// added exporting functionality
		std::string exportSegmentGraph(ExportData const& data, Options const& options) const;
		std::string exportHeatmap(ExportData const& data, Options const& options) const;
		std::string export_initial_batch(ExportData const& data, Options const& options, PathIDs const& found_paths, BatchType const& batch_type) const;
		std::string export_prunned_segments(ExportData const& data, Options const& options, PathfinderDS const& p_ds, GridStack const& grid_stack, LevelExtractor const& level_extractor) const;


	private:
		Graph const& graph;
		Geometry geometry;
		struct CoordinateHash {
			std::size_t operator()(Coordinate const& coordinate) const;
		};

		using ExportNodeID = NodeID;
		using NodeMap = std::unordered_map<Coordinate, ExportNodeID, CoordinateHash>;

		NodeMap createNodeMap(ExportData const& data) const;
	
		// "to string" helpers
		std::string node_to_latlon(const NodeID) const;
		std::string segment_to_nodelist(const TGEdge&, const TGSegments&, const TGNodes&) const;
		std::string counter_to_string_representation(const size_t node_index, const float normalization, const CounterVector& absolute_counter_vec) const;
		std::string trajlist_to_str(TrajList& trajecory_list) const;
		std::string edgelist_to_str(TGEdgeIDs& edges) const;
		void append_segment_str(std::string*, const TGEdge&, const TGSegments&, const TGNodes&, TrajectoryDFA&) const;
		void append_graph_str(std::string* json_string, const TrajectoryGraph&) const;
		void append_node_str(std::string* json_string, TGNode& node) const;
		void append_heatmap_str(std::string* json_string, const CounterVector& absolute_counter_vec) const;
		// batches 
		std::string get_initial_plain_inorder_batch(ExportData const& data, Options const& options, PathIDs const& found_paths) const;
		std::string get_initial_plain_levelorder_batch(ExportData const& data, Options const& options, PathIDs const& found_paths) const;
		std::string get_initial_edgewise_level_batch(ExportData const& data, Options const& options, PathIDs const& found_paths) const;
		std::string get_initial_uv_batch(std::string name,ExportData const& data, Options const& options, PathIDs const& found_paths) const;
		

	};

} // namespace pf
