#include "node_range.h"

#include "defs.h"
#include "paths.h"

namespace pf
{

//
// NodeRange
//

// Note that we need to increase the end by one. The reason for this is
// explained in the comment of the iterator class in the header file.
NodeRange::NodeRange(Graph const& graph, Path const& path)
    : path(path)
    , begin_it(graph, path, 0)
    , end_it(graph, path, path.getNoOfRootEdges() + 1)
{
	// If the path is empty then make the iterator range empty too.
	if (path.empty()) {
		++begin_it;
	}
}

// Note that we need to increase the end by one. The reason for this is
// explained in the comment of the iterator class in the header file.
NodeRange::NodeRange(
    Graph const& graph, Path const& path, Index begin, Index end)
    : path(path), begin_it(graph, path, begin), end_it(graph, path, end + 1)
{
}

auto NodeRange::begin() const -> iterator
{
	return begin_it;
}

auto NodeRange::end() const -> iterator
{
	return end_it;
}

// As the end index was increased in the constructor it has to be decreased for
// the constructor of the ReverseNodeRange.
ReverseNodeRange NodeRange::reverse(Graph const& graph) const
{
	return ReverseNodeRange(
	    graph, path, begin_it.getIndex(), end_it.getIndex() - 1);
}

//
// NodeRange::iterator
//

NodeRange::iterator::iterator(Graph const& graph, Path const& path, Index index)
    : path(path), graph(graph), current_index(index)
{
}

auto NodeRange::iterator::operator++() -> iterator&
{
	++current_index;
	return *this;
}

bool NodeRange::iterator::operator==(iterator other) const
{
	return &path == &other.path && current_index == other.current_index;
}

bool NodeRange::iterator::operator!=(iterator other) const
{
	return !(*this == other);
}

auto NodeRange::iterator::operator*() const -> reference
{
	return path.getNode(graph, current_index);
}

auto NodeRange::iterator::getIndex() const -> Index
{
	return current_index;
}

//
// ReverseNodeRange
//

// For the explanation of the index passed to begin_it see the comment for the
// reverse_iterator class.
ReverseNodeRange::ReverseNodeRange(Graph const& graph, Path const& path)
    : path(path), begin_it(graph, path, path.getNoOfRootEdges()), end_it(graph, path, -1)
{
	// If the path is empty then make the iterator range empty too.
	if (path.empty()) {
		++begin_it;
	}
}

// For the explanation of the index passed to begin_it see the comment for the
// reverse_iterator class.
ReverseNodeRange::ReverseNodeRange(
    Graph const& graph, Path const& path, Index begin, Index end)
    : path(path), begin_it(graph, path, end), end_it(graph, path, begin - 1)
{
}

auto ReverseNodeRange::begin() const -> reverse_iterator
{
	return begin_it;
}

auto ReverseNodeRange::end() const -> reverse_iterator
{
	return end_it;
}

// As the index of end_it was decremented in the constructor it is now
// incremented for the constructor of NodeRange.
NodeRange ReverseNodeRange::reverse(Graph const& graph) const
{
	return NodeRange(graph, path, end_it.getIndex() + 1, begin_it.getIndex());
}

//
// ReverseNodeRange::reverse_iterator
//

ReverseNodeRange::reverse_iterator::reverse_iterator(
    Graph const& graph, Path const& path, Index index)
    : path(path), graph(graph), current_index(index)
{
}

auto ReverseNodeRange::reverse_iterator::operator++() -> reverse_iterator&
{
	--current_index;
	return *this;
}

bool ReverseNodeRange::reverse_iterator::operator==(
    reverse_iterator other) const
{
	return &path == &other.path && current_index == other.current_index;
}

bool ReverseNodeRange::reverse_iterator::operator!=(
    reverse_iterator other) const
{
	return !(*this == other);
}

auto ReverseNodeRange::reverse_iterator::operator*() const -> reference
{
	return path.getNode(graph, current_index);
}

auto ReverseNodeRange::reverse_iterator::getIndex() const -> Index
{
	return current_index;
}

} // namespace pf
