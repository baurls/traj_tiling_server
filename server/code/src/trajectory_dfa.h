#pragma once

#include <cstdint>
#include <string>
#include <unordered_map>
#include <vector>
#include <fstream>

/*
	# # # # # # # # # # # # # # # # # # # # # # # # # #
	#                                                 #
	#   Created by Lukas Baur, Master Thesis 2021.    #
	#                                                 #
	#   Supervisor: T. Rupp, Prof. S. Funke           #
	#   Institute:  FMI (University of Stuttgart)     #
	#                                                 #
	# # # # # # # # # # # # # # # # # # # # # # # # # #
*/


namespace pf
{

	static const size_t primes[] = {2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71};
	static const size_t no_of_primes = (sizeof primes / sizeof primes[0]);

	using TrajectoryID = int64_t;
	using TrajList = std::vector<TrajectoryID>;

	class TrajListHasher
	{
	public:
		size_t operator()(const TrajList &l) const
		{
			size_t hash = 0;
			for (size_t i = 0; i < l.size(); i++)
			{
				hash += primes[i % no_of_primes] * l[i];
			}
			return hash;
		}
	};

	class TrajListComparator
	{
	public:
		bool operator()(const TrajList &l1, const TrajList &l2) const
		{
			if(l1.size() != l2.size()){
				return false;
			}
			for (size_t i = 0; i < l1.size(); i++)
			{
				if (l1[i] != l2[i]){
					return false;
				}
			}
			return true;
		}
	};

	using TrajSuperimpositionID = int64_t;
	using Depth = int64_t;
	using SuccessorMap = std::unordered_map<TrajectoryID, TrajSuperimpositionID>;

	struct State
		{
			TrajSuperimpositionID id = -1;
			SuccessorMap successors;
			Depth depth = 0;
			TrajSuperimpositionID previous_state_id = -1;
			TrajectoryID previous_traj_id = -1;
		};
	

		
	const TrajSuperimpositionID INVALID_SUCCESSOR = -1;

	using States = std::vector<State>;
	using TrajListStateMap = std::unordered_map<TrajList, TrajSuperimpositionID, TrajListHasher, TrajListComparator>;
	
	

	class TrajectoryDFA
	{
	public:
		TrajectoryDFA (int64_t number_of_trajectories) : eps_outgoing_map(std::vector<TrajSuperimpositionID>(number_of_trajectories, INVALID_SUCCESSOR)){
			create_empty_state();
		}

		static const TrajSuperimpositionID EMPTY_STATE_ID = 0;
		TrajSuperimpositionID make_epsilon_transition(TrajectoryID);
		TrajSuperimpositionID make_transition(TrajSuperimpositionID, TrajectoryID);
		TrajList get_trajectories_for_state_id(TrajSuperimpositionID, TrajectoryID);
		TrajList get_trajectories_for_state_id(TrajSuperimpositionID);
		Depth get_usage_for_superimposition_id(TrajSuperimpositionID);
		States states;

	private:
		std::vector<TrajSuperimpositionID> eps_outgoing_map;
		Depth max_depth = 0;
		TrajListStateMap trajlist_state_mapping;
		void create_empty_state();
		TrajSuperimpositionID create_state(TrajectoryID, TrajSuperimpositionID, Depth);
		static const TrajSuperimpositionID NO_SUCCESSOR_STATE_ID = 0;
	
	};

	
	std::ostream& operator<<(std::ostream&, const TrajectoryDFA&);

} // namespace pf
