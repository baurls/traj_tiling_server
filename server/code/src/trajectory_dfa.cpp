#include "trajectory_dfa.h"
#include "defs.h"
#include "time_tracker.h"
#include "paths_ds.h"
#include "assert.h"
#include <fstream>
#include <bits/stdc++.h>

/*
	# # # # # # # # # # # # # # # # # # # # # # # # # #
	#                                                 #
	#   Created by Lukas Baur, Master Thesis 2021.    #
	#                                                 #
	#   Supervisor: T. Rupp, Prof. S. Funke           #
	#   Institute:  FMI (University of Stuttgart)     #
	#                                                 #
	# # # # # # # # # # # # # # # # # # # # # # # # # #
*/


namespace pf
{
	std::ostream &operator<<(std::ostream &o, const SuccessorMap &succ_map)
	{
		if (succ_map.size() > 0){
			auto iter0 = succ_map.begin(); 
			TrajSuperimpositionID from = iter0->first;
			TrajSuperimpositionID to = iter0->second;
			o << from << "->" << to; 

			++iter0;
			for (auto iter = iter0; iter != succ_map.end(); ++iter){
				TrajSuperimpositionID from = iter->first;
				TrajSuperimpositionID to = iter->second;
				o << ", " << from << "->" << to; 
			}
		}
		return o;
	}

	std::ostream &operator<<(std::ostream &o, const State &s)
	{
		o << "State_#" <<  s.id << ":[depth=" << s.depth << " succ={" << s.successors << "}]";
		return o;
	}

	std::ostream &operator<<(std::ostream &o, const States &s_vec)
	{
		if (s_vec.size() > 0){
			o << s_vec[0];
		}
		for (size_t i = 1; i < s_vec.size(); i++){
			o << ", " << s_vec[i];
		}

		return o;
	}

	std::ostream& operator<<(std::ostream& o, const TrajectoryDFA& dfa){
		return o << "States: {" << dfa.states <<  "}" << std::endl;
	}


	void TrajectoryDFA::create_empty_state()
	{
		create_state(INVALID_SUCCESSOR, NO_SUCCESSOR_STATE_ID, 0);
	}

	TrajSuperimpositionID TrajectoryDFA::create_state(TrajectoryID trajectory_id, TrajSuperimpositionID current_state_id, Depth new_depth)
	{
		State new_state;
		TrajSuperimpositionID new_state_id = states.size();
		new_state.id = new_state_id;
		new_state.previous_traj_id = trajectory_id;
		new_state.previous_state_id = current_state_id;
		new_state.depth = new_depth;

		max_depth = std::max(max_depth, new_depth);
		states.push_back(new_state);
		return new_state_id;
	}
	
	TrajList TrajectoryDFA::get_trajectories_for_state_id(TrajSuperimpositionID state_id)
	{
		TrajList trajectories;
		while (state_id != EMPTY_STATE_ID)
		{
			State state = states[state_id];
			trajectories.push_back(state.previous_traj_id);
			state_id = state.previous_state_id;
		}
		std::sort(trajectories.begin(), trajectories.end());
		return trajectories;
	}

	TrajList TrajectoryDFA::get_trajectories_for_state_id(TrajSuperimpositionID state_id, TrajectoryID current_traj_id)
	{
		TrajList trajectories;
		while (state_id != EMPTY_STATE_ID)
		{
			State state = states[state_id];
			trajectories.push_back(state.previous_traj_id);
			state_id = state.previous_state_id;
		}
		trajectories.push_back(current_traj_id);
		std::sort(trajectories.begin(), trajectories.end());
		return trajectories;
	}

	Depth TrajectoryDFA::get_usage_for_superimposition_id(TrajSuperimpositionID state_id)
	{
		return states[state_id].depth;
	}

	TrajSuperimpositionID TrajectoryDFA::make_epsilon_transition(TrajectoryID trajectory_id)
	{
		bool state_has_traj_outedge = eps_outgoing_map[trajectory_id] != INVALID_SUCCESSOR; 
		if (state_has_traj_outedge)
		{
			// transition exists, load and return.
			auto next_state = eps_outgoing_map[trajectory_id]; 
			return next_state;
		}

		// there's no state for this trajectory set created yet. Do so.
		// set link and return
		auto next_state = create_state(trajectory_id, EMPTY_STATE_ID, 1);
		eps_outgoing_map[trajectory_id] = next_state;
		return next_state;
	}
	

	TrajSuperimpositionID TrajectoryDFA::make_transition(TrajSuperimpositionID current_state_id, TrajectoryID trajectory_id)
	{
		assert( (current_state_id > 0) and (current_state_id < states.size()));

		State state = states[current_state_id];

		bool state_has_traj_outedge = state.successors.find(trajectory_id) != state.successors.end();	 
		if (state_has_traj_outedge)
		{
			// transition exists, load and return.
			auto next_state = state.successors[trajectory_id];
			return next_state;
		}

		// there's no state for this trajectory set created yet. Do so.
		// set link and return
		auto next_state = create_state(trajectory_id, current_state_id, state.depth + 1);
		states[current_state_id].successors[trajectory_id] = next_state;
		return next_state;
	}
	


} // namespace pf
