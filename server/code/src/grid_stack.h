#pragma once

#include "basic_types.h"
#include "defs.h"
#include "grid.h"

/*
	# # # # # # # # # # # # # # # # # # # # # # # # # #
	#                                                 #
	#   Created by Lukas Baur, Master Thesis 2021.    #
	#                                                 #
	#   Supervisor: T. Rupp, Prof. S. Funke           #
	#   Institute:  FMI (University of Stuttgart)     #
	#                                                 #
	# # # # # # # # # # # # # # # # # # # # # # # # # #
*/



namespace pf
{

//
// Member functions of GridStack
//
// level 0:
//                   j
//    		   ______________n = 2^q
//            |              |
//            |              |
//        i   |              |
//            |              |
//      2^p=m |______________|
//
//
// level 1:
//                   j
//	     	   _________n = 2^(q-1)
//             |        |
//         i   |        |
//   2^(p-1)=m |________|
// 
//				


// types
using Grids = std::vector<Grid>;


class GridStack{
	public:
		// constructor
		GridStack(const Grid& g, const GridIndex level);
		GridStack();
		Grid get_grid(GridIndex level) const;

	private:

		// attributes
		Grids grids;
		GridIndex level;
};

} // namespace pf
