#pragma once

#include "paths.h"
#include "paths_ds.h"

#include <cstdint>
#include <sstream>
#include <string>
#include <vector>

namespace pf
{

class PathImporter
{
public:
	PathImporter(Graph const& graph, PathsDS& paths_ds);

	// Returns true iff parsing was successful.
	bool importPathsForPerformanceTest(
	    std::string graph_filename, const uint number_of_paths, uint distance);

	bool importPaths(std::string file_name);

	bool importPathsBin(std::string file_name);

private:
	bool already_parsed = false;

	Graph const& graph;
	PathsDS& paths_ds;

	std::size_t readHeader(std::ifstream& f);
	void readPaths(std::ifstream& f, std::size_t number_of_paths);

	std::size_t readHeaderBin(std::ifstream& f);
	void readPathsBin(std::ifstream& f, long number_of_paths);
};

} // namespace pf
