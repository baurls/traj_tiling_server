#pragma once

#include "geometry.h"
#include "graph.h"
#include "io/export_types.h"
#include "options.h"

#include <cstdint>
#include <string>
#include <unordered_map>
#include <vector>

/*
	# # # # # # # # # # # # # # # # # # # # # # # # # #
	#                                                 #
	#   Created by Lukas Baur, Master Thesis 2021.    #
	#                                                 #
	#   Supervisor: T. Rupp, Prof. S. Funke           #
	#   Institute:  FMI (University of Stuttgart)     #
	#                                                 #
	# # # # # # # # # # # # # # # # # # # # # # # # # #
*/


namespace pf
{

class AbsolutePathColorPalette{
	public:
		static std::string get_edge_color(size_t count);

	private:
		static const std::string COLOR_BLUE;
		static const std::string COLOR_DARK_RED;
		static const std::string COLOR_LIGHT_RED;
		static const std::string COLOR_ORANGE;
		static const std::string COLOR_YELLOW;
		static const std::string COLOR_GREEN;
};


} // namespace pf
