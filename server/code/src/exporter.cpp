#include "exporter.h"

#include "defs.h"
#include "paths_ds.h"
#include "segment_splitter.h"
#include "heatmap_builder.h"
#include "edge_colormaps.h"
#include "time_tracker.h"
#include "grid_heatmap.h"

namespace pf
{

Exporter::Exporter(Graph const& graph) : graph(graph), geometry(graph)
{
}

void append_edgelist_as_json(std::string* result, const EdgeIDs& edges, const Graph& graph){
	unsigned int j = 1;
	for (EdgeID edge_id : edges) {
		auto const& edge = graph.getEdge(edge_id);

		auto coordinate_lat = graph.getNode(edge.source).coordinate.lat;
		auto coordinate_lng = graph.getNode(edge.source).coordinate.lon;
		*result += "[" + std::to_string(coordinate_lat) + "," + std::to_string(coordinate_lng) + "], ";

		// if last element
		if (edges.size() == j) {
			auto target_coordinate_lat = graph.getNode(edge.target).coordinate.lat;
			auto target_coordinate_lng = graph.getNode(edge.target).coordinate.lon;
			*result += "[" + std::to_string(target_coordinate_lat) + "," + std::to_string(target_coordinate_lng) + "]";
		}
		j++;
	}
}

std::string Exporter::exportTrajectories(
    ExportData const& data, Options const& options) const
{
	// The result string
	std::string result = "{\"trajectories\":[";

	auto found = data.intersection_paths;
	auto total = data.total_paths;
	auto totalLength = 0;

	// Write edges of paths
	for (PathID path_id = 0; path_id < data.paths_ds.getNrOfPaths();
	     ++path_id) {

		Path const& path = data.paths_ds.getPath(path_id);

		auto length = path.getLength(graph);
		totalLength += length;

		result += "{\"length\": " + std::to_string(path.getLength(graph));

		if (options.getParseIntervals()) {
			result += ", \"timestamp\":{\"start\":" +
			          std::to_string(path.getStartTime()) +
			          ",\"end\":" + std::to_string(path.getEndTime()) + "}";
		}

		result += ", \"coords\":[";
		// Convert the trajectory objects to coordinate Arrays
		append_edgelist_as_json(&result, path.getEdges(), graph);
		result += "]}";

		// Append comma if the path isn't the last one
		if (path_id < data.paths_ds.getNrOfPaths() - 1) {
			result += ",";
		}
	}

	result += "], \"found\":" + std::to_string(found) +
	          ", \"total\":" + std::to_string(total) +
	          ", \"length\":" + std::to_string(totalLength) + "}";

	return result;
}


inline void add_comma_if_not_first(std::string* json_string, int* counter) {
	if(*counter > 0){
		*json_string += ",";
	}
	*counter = *counter +1;
}



std::string Exporter::edgelist_to_str(TGEdgeIDs& edges) const {
	std::string json_string = "[";
	int output_counter = 0;
	for (auto edge : edges){
		add_comma_if_not_first(&json_string, &output_counter);
		json_string += std::to_string(edge);
	}
	json_string += "]";
	return json_string;
}


void Exporter::append_node_str(std::string* json_string, TGNode& node) const {
	*json_string += "\""  + std::to_string(node.id) +  "\":" + edgelist_to_str(node.adjacent_edges);
}

std::string Exporter::trajlist_to_str(TrajList& trajecory_list) const {
	auto last_pointer = std::unique(trajecory_list.begin(), trajecory_list.end());
    trajecory_list.resize(std::distance(trajecory_list.begin(), last_pointer));

	std::string list = "[";
	int output_counter = 0;
	for (auto trajectory : trajecory_list){
		add_comma_if_not_first(&list, &output_counter);
		list += std::to_string(trajectory);
	}
	list += "]";
	return list;
}


std::string Exporter::node_to_latlon(const NodeID node) const{
	auto lat = graph.getNode(node).coordinate.lat;
	auto lon = graph.getNode(node).coordinate.lon;
	return std::to_string(lat) + "," + std::to_string(lon);
}


std::string Exporter::segment_to_nodelist(const TGEdge& segment_edge, const TGSegments& segments, const TGNodes& nodes) const {
	std::string nodelist = "[";
	int output_counter = 1;
	nodelist += node_to_latlon(nodes[segment_edge.u].pathnode_id);
	if(segment_edge.segment_link != TrajectoryGraph::EMPTY_SEGMENT_ID){
		for(auto inner_node : segments[segment_edge.segment_link]){
			add_comma_if_not_first(&nodelist, &output_counter);
			auto osm_node = nodes[inner_node].pathnode_id;
			nodelist += node_to_latlon(osm_node);
		}
	}
	add_comma_if_not_first(&nodelist, &output_counter);
	nodelist += node_to_latlon(nodes[segment_edge.v].pathnode_id);
	nodelist += "]";
	return nodelist;

}

void Exporter::append_segment_str(std::string* json_string, const TGEdge& segment_edge, const TGSegments& segments, const TGNodes& nodes, TrajectoryDFA& dfa) const {
	auto usage_count = dfa.get_usage_for_superimposition_id(segment_edge.trajectory_imposition);
	auto used_trajectories = dfa.get_trajectories_for_state_id(segment_edge.trajectory_imposition);
	auto color_string = AbsolutePathColorPalette::get_edge_color(usage_count);
	auto nodelist_string = segment_to_nodelist(segment_edge, segments, nodes);

	*json_string += "\""  + std::to_string(segment_edge.id) +  "\":{\"superimposition_id\":" + std::to_string(segment_edge.trajectory_imposition) + ", " +
					"\"color\":\"" + color_string + "\"," + 
					"\"nodes\":" + nodelist_string + "," + 
					"\"u\":" + std::to_string(segment_edge.u) + "," + 
					"\"v\":" + std::to_string(segment_edge.v) + "," + 
					"\"trajectories\":" + trajlist_to_str(used_trajectories) + 
					"}";
	
	
}

void Exporter::append_graph_str(std::string* json_string, const TrajectoryGraph& traj_graph) const {
	// load graph components	
	auto segments_connections = traj_graph.get_segment_connections();
	auto segments = traj_graph.get_segments();
	auto dfa = traj_graph.trajectory_dfa;
	auto nodes = traj_graph.get_nodes();
	
	// convert to string
	*json_string += "{";
	// add all 'edges' (=segments)
	*json_string += "\"segments\":";
	*json_string += "{";
	int output_counter = 0;
	for (auto segment_connection : segments_connections){
		add_comma_if_not_first(json_string, &output_counter);
		append_segment_str(json_string, segment_connection, segments, nodes, dfa);
	}
	*json_string += "},";
	// add segment connections
	*json_string += "\"connections\":";
	*json_string += "{";
	output_counter = 0;
	for (auto v : traj_graph.get_active_nodes()){
		add_comma_if_not_first(json_string, &output_counter);
		append_node_str(json_string, v);
	}
	*json_string += "}";

	*json_string += "}";
}

std::string Exporter::exportSegmentGraph(
    ExportData const& data, Options const& options) const
{

	// The result string

	auto found = data.intersection_paths;
	auto total = data.total_paths;
	auto totalLength = 0;

	TimeTracker time_tracker;
	time_tracker.start();
	// get segment graph
	std::cout << "  Creating Graph.." << std::endl;
	const TrajectoryGraph traj_graph(graph, data.paths_ds);
	time_tracker.stop();
	std::cout << "  ╙ Graph Creation finished. Took " << time_tracker.getLastMeasurment() << " seconds"<< std::endl;
	
	// write all segements
	time_tracker.start();
	std::cout << "  Exporting to String.."<< std::endl;
	std::string result = "{\"trajectorySegmentGraph\": ";
	append_graph_str(&result, traj_graph);
	result += ", \"found\":" + std::to_string(found) +
	          ", \"total\": " + std::to_string(total) +
	          ", \"length\":" + std::to_string(totalLength) 
			  + "}";
	time_tracker.stop();
	std::cout << "  String exporting finished. Took " << time_tracker.getLastMeasurment() << " seconds"<< std::endl;

	return result;
}

std::size_t Exporter::CoordinateHash::operator()(
    Coordinate const& coordinate) const
{
	auto lat_hash = std::hash<Coordinate::FloatType>{}(coordinate.lat);
	auto lon_hash = std::hash<Coordinate::FloatType>{}(coordinate.lon);
	return lat_hash + lon_hash;
}


std::string Exporter::exportHeatmap(
    ExportData const& data, Options const& options) const
{

	// The result string

	auto found = data.intersection_paths;
	auto total = data.total_paths;
	auto totalLength = 0;

	TimeTracker time_tracker;
	time_tracker.start();
	// get segment graph
	std::cout << "  Collecting Nodes.." << std::endl;
	const HeatmapBuilder heatmap_builder(graph, data.paths_ds);
	auto absolute_counter_vec = heatmap_builder.get_absolute_nodes_counter();
	time_tracker.stop();
	std::cout << "  ╙ finished. Took " << time_tracker.getLastMeasurment() << " seconds"<< std::endl;
	
	// write all segements
	time_tracker.start();
	std::cout << "  Exporting to String..";
	std::string result = "{\"trajectoryHeatmap\": ";
	append_heatmap_str(&result, absolute_counter_vec);
	result += ", \"found\":" + std::to_string(found) +
	          ", \"total\": " + std::to_string(total) +
	          ", \"length\":" + std::to_string(totalLength) 
			  + "}";
	time_tracker.stop();
	std::cout << "  String exporting finished. Took" << time_tracker.getLastMeasurment() << " seconds"<< std::endl;

	return result;
}


void Exporter::append_heatmap_str(std::string* json_string, const CounterVector& absolute_counter_vec) const {
	// load graph components	
	auto max_counter = *std::max_element(absolute_counter_vec.begin(), absolute_counter_vec.end());
	float normalization = 1.0f / static_cast<float>(max_counter);
	
	// contvert to string
	*json_string += "[";
	
	// find first non-zero entry
	auto first_non_zero = absolute_counter_vec.size();
	int vertex_counter = 0;
	for (size_t i=0; i < absolute_counter_vec.size(); i++){
		if(absolute_counter_vec[i] > 0){
			first_non_zero = i;
			vertex_counter++;
			*json_string += counter_to_string_representation(i, normalization, absolute_counter_vec);
			break;
		} 
	}
	for (size_t i=first_non_zero + 1; i < absolute_counter_vec.size(); i++){
		if(absolute_counter_vec[i] > 0){
			vertex_counter++;
			*json_string += "," + counter_to_string_representation(i, normalization, absolute_counter_vec);
		} 
	}
	*json_string += "]";
	std::cout << " " <<vertex_counter << " vertices written." << std::endl;
}

inline std::string Exporter::counter_to_string_representation(const size_t node_index, const float normalization, const CounterVector& absolute_counter_vec) const{
	auto const& node = graph.getNode(node_index);
	// [lat, lon, intensity]
	return "[" + std::to_string(node.coordinate.lat) + "," + std::to_string(node.coordinate.lon) + "," + std::to_string(absolute_counter_vec[node_index] * normalization) + "]";
}


std::string Exporter::export_initial_batch(ExportData const& export_data, Options const& options, PathIDs const& found_paths, BatchType const& batch_type) const{
	if (batch_type == pf::BatchType::PLAIN_INORDER){
		return get_initial_plain_inorder_batch(export_data, options, found_paths);
	}
	else if (batch_type == pf::BatchType::PLAIN_LEVELORDER){
		return get_initial_plain_levelorder_batch(export_data, options, found_paths);
	}
	else if (batch_type == pf::BatchType::EDGE_WISE){
		return get_initial_edgewise_level_batch(export_data, options, found_paths);
	}
	std::cout << "ERROR: Unknown Export Type.";
}

std::string Exporter::get_initial_plain_inorder_batch(ExportData const& export_data, Options const& options, PathIDs const& found_paths) const{
	std::string name = "InitPlainInorderBatch";
	return get_initial_uv_batch(name, export_data, options, found_paths);
}

std::string Exporter::get_initial_plain_levelorder_batch(ExportData const& export_data, Options const& options, PathIDs const& found_paths) const{
	std::string name = "InitPlainLevelorderBatch";
	return get_initial_uv_batch(name, export_data, options, found_paths);
}

std::string Exporter::get_initial_edgewise_level_batch(ExportData const& export_data, Options const& options, PathIDs const& found_paths) const{
	std::string name = "InitEdgeWiseBatch";
	return get_initial_uv_batch(name, export_data, options, found_paths);
}

std::string Exporter::get_initial_uv_batch(std::string batch_name, ExportData const& export_data, Options const& options, PathIDs const& found_paths) const{

	// The result string
	std::string result = "{\"" + batch_name + "\": {";

	auto found = export_data.intersection_paths;
	auto total = export_data.total_paths;
	auto totalLength = 0;

	auto is_first_element = true;
	// Write first and last node of all edges
	for (PathID path_id : found_paths) {
		if(!is_first_element){
			result += ",";
		}
		is_first_element = false;

		Path const& path = export_data.paths_ds.getPath(path_id);
		// increment lengths
		auto length = path.getLength(graph);
		totalLength += length;

		// extract endpoints
		auto u_id = path.getSource(graph);
		auto v_id = path.getTarget(graph);
		
		result += "\"" + std::to_string(path_id) + "\":{";
		result += "\"length\":" + std::to_string(path.getLength(graph));
		result += ",\"n\":" + std::to_string(path.getNoOfRootEdges() + 1);
		result += ",\"uv\":[[" + node_to_latlon(u_id) + "],[" + node_to_latlon(v_id) + "]]";
		result += "}";	
	}
	result += "}";
	result += ", \"found\":" + std::to_string(found) +
	          ", \"total\": " + std::to_string(total) +
	          ", \"length\":" + std::to_string(totalLength); 
	result += "}";
	return result;

}

// add all root edges which are visible and have a certain level
void filter_root_edges_wrt_view_and_level(PathIDs const& found_paths, Graph const& graph, Level const cutoff_level, BoundingBox const& view_box, PathfinderDS const& p_ds, EdgeIDs& matching_edges, EdgeIDs& removed_edges){
	TimeTracker time_tracker;
	time_tracker.start();
	std::cout << "| | Filter wrt view and level" <<  std::endl;

	// path (-bounding box) is at least partially visible, go over all root-edges and check visibility
	PathsDS paths_ds = p_ds.getPathsDS();
	
	// iterate over all root paths and copy if visible and above cutoff level
	for(PathID path_id : found_paths){

		// check path for high-level properties:
		if(not view_box.overlaps_with(p_ds.getPathBox(path_id))){
			// the hole path is not visible. 
			// -> Skip it.
			continue;
		}

		const Path& path = paths_ds.getPath(path_id);
		for(const EdgeID edge_id: path.getEdges()){
			const Edge edge = graph.getEdge(edge_id);
			auto const source = graph.getNode(edge.source);
			auto const target = graph.getNode(edge.target);
			
			bool is_visible = view_box.overlaps_with(p_ds.getEdgeBox(edge_id));
			
			if(not is_visible){
				// skip nodes which are not visible
				continue;
			}

			bool is_not_below_level = source.level >= cutoff_level or target.level >= cutoff_level;
			if(is_not_below_level){
				matching_edges.push_back(edge_id);
			}
			else{
				removed_edges.push_back(edge_id);
			}

		}
	}
	time_tracker.stop();
	std::cout << "| | ↳ Took "<< time_tracker.getLastMeasurment() << " seconds" << std::endl;


	time_tracker.start();
	std::cout << "| | Removing duplicates" << std::endl;
	// remove duplicates
	matching_edges.erase( std::unique( matching_edges.begin(), matching_edges.end() ), matching_edges.end() );
	time_tracker.stop();
	std::cout << "| | ↳ Took "<< time_tracker.getLastMeasurment() << " seconds" << std::endl;
	
}


std::string Exporter::export_prunned_segments(ExportData const& data, Options const& options, PathfinderDS const& p_ds, GridStack const& grid_stack, LevelExtractor const& level_extractor) const {
	auto found = data.found_paths.size();
	auto total = data.total_paths;

	TimeTracker time_tracker;
	
	Level const root_edge_unpack_level = level_extractor.extract_level(options, graph);
	Level const cutoff_level = options.getCutoffLevel();
	BoundingBox const view_box = data.boxes[1];


	std::cout << "| Filter edges" << std::endl;
	time_tracker.start();

	EdgeIDs filtered_root_edges; 
	EdgeIDs kept_root_edges; 
	filter_root_edges_wrt_view_and_level(data.found_paths, graph, cutoff_level, view_box, p_ds, kept_root_edges, filtered_root_edges);
	time_tracker.stop();
	std::cout << "| ↳ Took " << time_tracker.getLastMeasurment() << " seconds" << std::endl;

	std::cout << "| Convert resultlist to json" << std::endl;
	std::cout << "| | writing high level edges: " << kept_root_edges.size() << std::endl;
	time_tracker.start();
	// ----------- export to json-string ---------------------
	// (1) write high-level edges
	std::string result = "{\"edges\":[";
	if(kept_root_edges.size() > 0){
		// handle first edge
		auto const& root_edge_id = kept_root_edges[0];
		
		EdgeIDs unpacked_root_edge = graph.unpack(root_edge_id, root_edge_unpack_level);
		result += "[";
		append_edgelist_as_json(&result, unpacked_root_edge, graph);
		result += "]";
	}
	// handle all n-1 other edges
	for (uint i=1; i < kept_root_edges.size(); i++){
		auto const& root_edge_id = kept_root_edges[i];
		
		EdgeIDs unpacked_root_edge = graph.unpack(root_edge_id, root_edge_unpack_level);
		result += ",[";
		append_edgelist_as_json(&result, unpacked_root_edge, graph);
		result += "]";
	}
	time_tracker.stop();
	std::cout << "| | ↳ Took " << time_tracker.getLastMeasurment() << " seconds" << std::endl;


	const GridIndex level = 0;
	Grid grid = grid_stack.get_grid(level);
	CountGrid count_grid = CountGrid(grid);

	
	std::cout << "| | writing low level edges (heatmap): " << filtered_root_edges.size() << std::endl;
	time_tracker.start();
	// (2) write low-level heatmap
	// work on first edge
	if(filtered_root_edges.size() > 0){
		auto const& edge_id = filtered_root_edges[0];
		Edge const& edge = graph.getEdge(edge_id);
		count_grid.process_covering(edge.cell_coverings[level]);
	}
	// handle all n-1 other edges
	for (uint i=1; i < filtered_root_edges.size(); i++){
		auto const& edge_id = filtered_root_edges[i];
		auto const& edge = graph.getEdge(edge_id);
		count_grid.process_covering(edge.cell_coverings[level]);
	}
	// write heatmap_data:
	const float cutoff_value =  20.0f;
	result += "],";
	GridHeatmapExporter::append_as_json(&result, count_grid, cutoff_value);
	// (3) wirte statistics
	result += ", \"found\":" + std::to_string(found) +
	          ", \"total\":" + std::to_string(total) +
	          ", \"length\": -1}";

	time_tracker.stop();
	std::cout << "| | ↳ Took " << time_tracker.getLastMeasurment() << " seconds" << std::endl;

	return result;
	
}

} // namespace pf