#pragma once

#include "edge_colormaps.h"
#include <string>

/*
	# # # # # # # # # # # # # # # # # # # # # # # # # #
	#                                                 #
	#   Created by Lukas Baur, Master Thesis 2021.    #
	#                                                 #
	#   Supervisor: T. Rupp, Prof. S. Funke           #
	#   Institute:  FMI (University of Stuttgart)     #
	#                                                 #
	# # # # # # # # # # # # # # # # # # # # # # # # # #
*/


namespace pf
{

const std::string AbsolutePathColorPalette::COLOR_BLUE = "1b0083";
const std::string AbsolutePathColorPalette::COLOR_DARK_RED = "#7c0000";
const std::string AbsolutePathColorPalette::COLOR_LIGHT_RED = "#cf0000";
const std::string AbsolutePathColorPalette::COLOR_ORANGE = "#e88e00";
const std::string AbsolutePathColorPalette::COLOR_YELLOW = "#FFFB09";
const std::string AbsolutePathColorPalette::COLOR_GREEN = "#18b900";


std::string AbsolutePathColorPalette::get_edge_color(size_t count)
{
	
	if (count == 1){
		return COLOR_GREEN;
	}
	if (count <=  2){
		return COLOR_YELLOW;
	}
	if (count <=  3){
		return COLOR_ORANGE;
	}
	if (count <=  4){
		return COLOR_LIGHT_RED;
	}
	if (count <=  5){
		return COLOR_DARK_RED;
	}
	return COLOR_BLUE;
}


} // namespace pf
