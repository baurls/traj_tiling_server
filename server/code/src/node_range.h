#pragma once

#include "graph.h"
#include "paths.h"

#include <cstdint>
#include <iterator>

namespace pf
{

namespace unit_tests
{
void testNodeRange();
} // namespace unit_tests

// Forward declare for reverse() member function of NodeRange
class ReverseNodeRange;

class NodeRange
{
private:
	using iterator_base = std::iterator<std::forward_iterator_tag, NodeID,
	    NodeID, NodeID const*, NodeID>;

	using Index = Path::NodeIndex;

public:
	// An iterator on the edge of index i of a path points to the source of this
	// edge. This is also why the index of the end is path.length() + 1. The
	// node of index path.length() is path.getTarget().
	class iterator : iterator_base
	{
	private:
		Path const& path;
		Graph const& graph;
		Index current_index;

	public:
		explicit iterator(Graph const& graph, Path const& path, Index index);

		iterator& operator++();
		bool operator==(iterator other) const;
		bool operator!=(iterator other) const;
		reference operator*() const;

		Index getIndex() const;
	};

	NodeRange(Graph const& graph, Path const& path);
	NodeRange(Graph const& graph, Path const& path, Index begin, Index end);

	iterator begin() const;
	iterator end() const;

	ReverseNodeRange reverse(Graph const& graph) const;

private:
	Path const& path;

	iterator begin_it;
	iterator end_it;
};

class ReverseNodeRange
{
private:
	using iterator_base = std::iterator<std::forward_iterator_tag, NodeID,
	    NodeID, NodeID const*, NodeID>;

	using Index = Path::NodeIndex;

public:
	// An iterator on the edge of index i of a path points to the source of this
	// edge. This is also why the index of begin is path.length(). The node of
	// index path.length() is path.getTarget().
	class reverse_iterator : iterator_base
	{
	private:
		Path const& path;
		Graph const& graph;
		Index current_index;

	public:
		explicit reverse_iterator(
		    Graph const& graph, Path const& path, Index index);

		reverse_iterator& operator++();
		bool operator==(reverse_iterator other) const;
		bool operator!=(reverse_iterator other) const;
		reference operator*() const;

		Index getIndex() const;
	};

	ReverseNodeRange(Graph const& graph, Path const& path);
	ReverseNodeRange(
	    Graph const& graph, Path const& path, Index begin, Index end);

	reverse_iterator begin() const;
	reverse_iterator end() const;

	NodeRange reverse(Graph const& graph) const;

private:
	Path const& path;

	reverse_iterator begin_it;
	reverse_iterator end_it;
};

} // namespace pf
