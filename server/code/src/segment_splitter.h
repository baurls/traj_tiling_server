#pragma once

#include "geometry.h"
#include "graph.h"
#include "io/export_types.h"
#include "trajectory_dfa.h"

#include <cstdint>
#include <string>
#include <unordered_map>
#include <vector>

/*
	# # # # # # # # # # # # # # # # # # # # # # # # # #
	#                                                 #
	#   Created by Lukas Baur, Master Thesis 2021.    #
	#                                                 #
	#   Supervisor: T. Rupp, Prof. S. Funke           #
	#   Institute:  FMI (University of Stuttgart)     #
	#                                                 #
	# # # # # # # # # # # # # # # # # # # # # # # # # #
*/


namespace pf
{

	using NodePair =  std::pair<int, int>;
	
	using TGNodeID = int64_t;
	using TGEdgeID = int64_t;
	using TGEdgeIDs = std::vector<TGEdgeID>;

	using TGSegmentID = int64_t;
	using TGSegment = std::vector<TGNodeID>;
	using TGSegments = std::vector<TGSegment>;

	class PairHasher
	{
	public:
		size_t operator()(const NodePair &p) const
		{
			if (p.first > p.second)
			{
				return p.first * 7919 + p.second*107;
			}
			else
			{
				return p.second * 7919 + p.first*107;
			}
		}
	};

	class PairComparator
	{
	public:
		bool operator()(const NodePair &p1, const NodePair &p2) const
		{
			return (std::min(p1.first, p1.second) == std::min(p2.first, p2.second)) and
				   std::max(p1.first, p1.second) == std::max(p2.first, p2.second);
		}
	};

	using PathnodeNodeMap = std::unordered_map<NodeID, TGNodeID>;
	using NodepairEdgeMap = std::unordered_map<NodePair, TGEdgeID, PairHasher, PairComparator>;
		

	struct TGEdge
	{
			TGEdgeID id;
			TrajSuperimpositionID trajectory_imposition = TrajectoryDFA::EMPTY_STATE_ID;
			bool is_virtual = false;
			TGNodeID u,v;
			TGSegmentID segment_link = -1;
	};

	struct TGNode
	{
			NodeID pathnode_id;
			TGNodeID id;
			TGEdgeIDs adjacent_edges;
			bool is_virtual = false;
	};

	using TGNodes = std::vector<TGNode>;
	using TGEdges = std::vector<TGEdge>;

	class TrajectoryGraph
	{
	public:
		TrajectoryGraph(Graph const &graph, PathsDS const &paths_ds);
		void print() const;
		TGEdges get_segment_connections() const;
		TGSegments get_segments() const;
		TGNodes get_nodes() const;
		TGNodes get_active_nodes() const;
		TrajectoryDFA trajectory_dfa;

		static const TGSegmentID EMPTY_SEGMENT_ID = -1;

	private:
		Graph const &graph;
		Geometry geometry;
		PathsDS const &paths_ds;
		// Graph
		TGNodes V;
		TGEdges E;
		TGSegments S;


		void create_graph();

		// Nodes
		PathnodeNodeMap create_nodes();
		void create_nodes_from_bool_array();
		void create_nodes_from_sorting();
		void create_nodes_from_hashmap();
		TGNodeID create_node(const NodeID);
		bool can_merge(const TGNode&);
		
		//Edges
		void create_edges(PathnodeNodeMap&);
		TGEdgeID create_edge(const TGNodeID, const TGNodeID, const TrajSuperimpositionID);
		TGEdgeID create_edge(const TGNodeID, const TGNodeID, const TrajSuperimpositionID, const TGSegmentID);
		bool have_same_color(const TGEdgeID&, const TGEdgeID&);
		bool has_a_virtual_edge(const TGEdgeID&, const TGEdgeID&);

		// Navigation
		TGNodeID get_vertex_on_other_end(const TGNodeID&, const TGEdgeID&);
		TGNodeID get_next_node_along_path(const TGNodeID&, const TGNodeID&);
		TGEdgeID get_edge_between(const TGNodeID&, const TGNodeID&);

		// Segments
		TGSegmentID create_segment(TGSegment&);
		

		// merge code
		void merge_graph_segments();
		void merge_in_both_directions(TGNodeID v);
		void mark_invalid_and_return_path_end(TGNodeID, TGNodeID, TGNodeID *, TGEdgeID *, TGSegment *);
		void update_adjacent_edges(TGEdgeIDs&, TGEdgeID,TGEdgeID);
		TGSegmentID create_segment_from_parts(const TGSegment&, TGNodeID, const TGSegment&);
	};

} // namespace pf
