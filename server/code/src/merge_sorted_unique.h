#pragma once

/*
inspired by:
https://stackoverflow.com/questions/9013485/c-how-to-merge-sorted-vectors-into-a-sorted-vector-pop-the-least-element-fro
*/

#include <algorithm>
#include <queue>
#include <vector>

namespace pf
{

namespace unit_tests
{
void testMergeSortedUnique();
} // namespace unit_tests

namespace merge_sorted_unique
{
using std::make_pair;
using std::pair;
using std::vector;

template <typename T>
void removeDuplicatesOfSortedVector(vector<T>& sorted_vector)
{
	auto new_end = std::unique(sorted_vector.begin(), sorted_vector.end());
	sorted_vector.erase(new_end, sorted_vector.end());
}

template <typename T>
uint getConcatenatedSize(const vector<vector<T>>& input_sorted_vectors)
{
	uint concat_size = 0;
	for (auto vector : input_sorted_vectors) {
		concat_size += vector.size();
	}
	return concat_size;
}

template <typename T>
vector<T> naive(const vector<vector<T>>& input_sorted_vectors)
{
	vector<T> output_vector;
	output_vector.reserve(getConcatenatedSize(input_sorted_vectors));
	for (auto& sorted_vector : input_sorted_vectors) {
		output_vector.insert(
		    output_vector.end(), sorted_vector.begin(), sorted_vector.end());
	}

	std::sort(output_vector.begin(), output_vector.end());
	removeDuplicatesOfSortedVector(output_vector);
	return output_vector;
}

template <typename T>
vector<T> inplace(const vector<vector<T>>& input_sorted_vectors)
{

	vector<T> output_vector;
	output_vector.reserve(getConcatenatedSize(input_sorted_vectors));
	vector<size_t> offsets;
	offsets.push_back(0);

	for (auto& sorted_vector : input_sorted_vectors) {
		output_vector.insert(
		    output_vector.end(), sorted_vector.begin(), sorted_vector.end());
		offsets.push_back(output_vector.size());
	}

	while (offsets.size() > 2) {
		assert(offsets.back() == output_vector.size());
		assert(offsets.front() == 0);
		vector<size_t> new_offsets;
		size_t x = 0;
		while (x + 2 < offsets.size()) {
			// mergesort (offsets[x],offsets[x+1]) and
			// (offsets[x+1],offsets[x+2])
			std::inplace_merge(output_vector.begin() + offsets.at(x),
			    output_vector.begin() + offsets.at(x + 1),
			    output_vector.begin() + offsets.at(x + 2));

			// now they are sorted, we just put offsets[x] and offsets[x+2] into
			// the new offsets.
			// offsets[x+1] is not relevant any more
			new_offsets.push_back(offsets.at(x));
			new_offsets.push_back(offsets.at(x + 2));
			x += 2;
		}
		// if the number of offsets was odd, there might be a dangling offset
		// which we must remember to include in the new_offsets
		if (x + 2 == offsets.size()) {
			new_offsets.push_back(offsets.at(x + 1));
		}
		// assert(new_offsets.front() == 0);
		assert(new_offsets.back() == output_vector.size());
		offsets.swap(new_offsets);
	}

	removeDuplicatesOfSortedVector(output_vector);
	return output_vector;
}

} // namespace merge_sorted_unique

} // namespace pf