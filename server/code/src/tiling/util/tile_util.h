#pragma once
#pragma once

#include <math.h>

/*
	# # # # # # # # # # # # # # # # # # # # # # # # # #
	#                                                 #
	#   Created by Lukas Baur, Master Thesis 2021.    #
	#                                                 #
	#   Supervisor: T. Rupp, Prof. S. Funke           #
	#   Institute:  FMI (University of Stuttgart)     #
	#                                                 #
	# # # # # # # # # # # # # # # # # # # # # # # # # #
*/


namespace pf
{

	class TileCalculationUtil
	{
	

	public:
		static int long2tilex(double lon, int z);
		static int lat2tiley(double lat, int z);
		static double tilex2long(int x, int z) ;
		static double tiley2lat(int y, int z);
	};


} // namespace pf
