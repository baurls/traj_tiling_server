#include "folder_util.h"

#include "../defs.h"

#include <string>
#include <sys/stat.h>
#include <fstream>


/*
	# # # # # # # # # # # # # # # # # # # # # # # # # #
	#                                                 #
	#   Created by Lukas Baur, Master Thesis 2021.    #
	#                                                 #
	#   Supervisor: T. Rupp, Prof. S. Funke           #
	#   Institute:  FMI (University of Stuttgart)     #
	#                                                 #
	# # # # # # # # # # # # # # # # # # # # # # # # # #
*/



namespace pf
{
 
	/* 
	   +++++++++++++++++++++++++++++++++++++++
	   +++++++    FolderUtil CODE    +++++++++
	   +++++++++++++++++++++++++++++++++++++++
	*/

	bool FolderUtil::dirExists(const std::string &s)
	// taken from 
	{
		struct stat buffer;
		return (stat (s.c_str(), &buffer) == 0);
	}

	void FolderUtil::create_folder(const std::string& path){
		if (dirExists(path)){
			return;
		}
		auto status = mkdir(path.c_str(), 0777);
		if(status == -1){
			throw std::invalid_argument( "ERROR: Couln't create directory: " + path);
		}
	}

} // namespace pf


