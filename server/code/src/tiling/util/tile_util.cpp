#include "tile_util.h"


/*
	# # # # # # # # # # # # # # # # # # # # # # # # # #
	#                                                 #
	#   Created by Lukas Baur, Master Thesis 2021.    #
	#                                                 #
	#   Supervisor: T. Rupp, Prof. S. Funke           #
	#   Institute:  FMI (University of Stuttgart)     #
	#                                                 #
	# # # # # # # # # # # # # # # # # # # # # # # # # #
*/



namespace pf
{

	/*
		This code is taken from the official OSM-wiki:
		https://wiki.openstreetmap.org/wiki/Slippy_map_tilenames 
	*/ 
 
	int TileCalculationUtil::long2tilex(double lon, int z) 
	{ 
		return (int)(floor((lon + 180.0) / 360.0 * (1 << z))); 
	}

	int TileCalculationUtil::lat2tiley(double lat, int z)
	{ 
		double latrad = lat * M_PI/180.0;
		return (int)(floor((1.0 - asinh(tan(latrad)) / M_PI) / 2.0 * (1 << z))); 
	}

	double TileCalculationUtil::tilex2long(int x, int z) 
	{
		return x / (double)(1 << z) * 360.0 - 180;
	}

	double TileCalculationUtil::tiley2lat(int y, int z) 
	{
		double n = M_PI - 2.0 * M_PI * y / (double)(1 << z);
		return 180.0 / M_PI * atan(0.5 * (exp(n) - exp(-n)));
	}


} // namespace pf


