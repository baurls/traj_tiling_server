#pragma once
#pragma once

#include <string>

/*
	# # # # # # # # # # # # # # # # # # # # # # # # # #
	#                                                 #
	#   Created by Lukas Baur, Master Thesis 2021.    #
	#                                                 #
	#   Supervisor: T. Rupp, Prof. S. Funke           #
	#   Institute:  FMI (University of Stuttgart)     #
	#                                                 #
	# # # # # # # # # # # # # # # # # # # # # # # # # #
*/


namespace pf
{

	class FolderUtil
	{
	public:
		static bool dirExists(const std::string &s); 
		static void create_folder(const std::string& path);
		
	};



} // namespace pf
