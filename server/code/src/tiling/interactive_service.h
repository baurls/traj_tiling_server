#pragma once
#pragma once


#include "query_cache.h"
#include "tile_creator.h"
#include "../query.h"
#include "../pathfinder.h"

#include <string>
#include <pistache/description.h>
#include <pistache/endpoint.h>
#include <pistache/http.h>
#include <pistache/http_header.h>
#include <pistache/serializer/rapidjson.h>

/*
	# # # # # # # # # # # # # # # # # # # # # # # # # #
	#                                                 #
	#   Created by Lukas Baur, Master Thesis 2021.    #
	#                                                 #
	#   Supervisor: T. Rupp, Prof. S. Funke           #
	#   Institute:  FMI (University of Stuttgart)     #
	#                                                 #
	# # # # # # # # # # # # # # # # # # # # # # # # # #
*/


namespace pf
{

	class InteractiveTileService
	{
	private:
		Pathfinder& pathfinder; 
		QueryCache cache;
		const Graph& graph;		
		InteractiveTileService();
		const PathboxTileCreator tile_creator;

	public:
		InteractiveTileService(const std::string tile_dir, const Graph& graph, Pathfinder& pathfinder, const CacheSize& cache_size, const PathboxTileCreator& tile_creator);
		SessionID create_session_from_query(const Query& query);
		void handle_tile_request(const Pistache::Rest::Request& request, Pistache::Http::ResponseWriter& response) const;

	};

	

} // namespace pf
