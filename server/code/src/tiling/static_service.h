#pragma once
#pragma once


#include <../defs.h>

#include <fstream>
#include <sstream>
#include <string>

#include <pistache/description.h>
#include <pistache/endpoint.h>
#include <pistache/http.h>
#include <pistache/http_header.h>

#include <pistache/serializer/rapidjson.h>

#include <iostream>

/*
	# # # # # # # # # # # # # # # # # # # # # # # # # #
	#                                                 #
	#   Created by Lukas Baur, Master Thesis 2021.    #
	#                                                 #
	#   Supervisor: T. Rupp, Prof. S. Funke           #
	#   Institute:  FMI (University of Stuttgart)     #
	#                                                 #
	# # # # # # # # # # # # # # # # # # # # # # # # # #
*/



namespace pf
{

	class StaticTileService
	{
	private:
		StaticTileService();
		const std::string& tile_dir;

	public:
		StaticTileService(const std::string& tile_dir);
		void handle_tile_request(const Pistache::Rest::Request& request, Pistache::Http::ResponseWriter& response) const;
	};




} // namespace pf
