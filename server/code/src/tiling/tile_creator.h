#pragma once
#pragma once

#include <cstdint>
#include <string>
#include <unordered_map>
#include <vector>
#include <math.h>
#include <opencv2/opencv.hpp>

#include "../bounding_box.h"
#include "../graph.h"
#include "../paths_ds.h"
#include "../level_extractor.h"
#include "../pathfinder.h"

/*
	# # # # # # # # # # # # # # # # # # # # # # # # # #
	#                                                 #
	#   Created by Lukas Baur, Master Thesis 2021.    #
	#                                                 #
	#   Supervisor: T. Rupp, Prof. S. Funke           #
	#   Institute:  FMI (University of Stuttgart)     #
	#                                                 #
	# # # # # # # # # # # # # # # # # # # # # # # # # #
*/


namespace pf
{

	struct TileCreationSettings{
		int transparency = 0.25 * 255;
		int base_thickness = 3;
		int base_alpha = 5;
		cv::LineTypes line_type = cv::LINE_8;
		int img_type = CV_8UC1;
		int tile_size_x = 512;
		int tile_size_y = 512;
	};

	class TileCreator
	{
	private:
		TileCreator();
		virtual void _generate_tile(cv::Mat& merged, const BoundingBox& tile_bb,const double& lon_normalization, const double& lat_normalization, const int& thickness, const int& alpha, const Level& unpacking_lvl) const;
		
	protected:
		const Graph& graph;
		const std::string tile_dir;
		const PathsDS& paths_ds;
		const BoundingBox graph_bb;
		const size_t nr_of_threads;
		const bool log_tiling;
		std::string tilelog_filename = "generated_tiles.log";

		void create_folder(const int z, const int x) const;
		void create_folder(const int z) const;
		void generate_tiles_for_level_range_naively(const int& z_lower, const int& z_upper) const;
		void generate_tiles_for_level_range_based_on_last_round(const int& z_lower, const int& z_upper) const;
	

		bool generate_tile_and_store(
			const BoundingBox& tile_bb, 
			const double& lon_normalization, 
			const double& lat_normalization,
			const std::string& path,
			const int& thickness,
			const int& alpha,
			const Level& unpacking_lvl
		) const;

		void generate_tiles_for_level(
			const int z, 
			const std::vector<int>& indices_x, 
			const std::vector<int>& indices_y,
			std::vector<int>& out_children_x,
			std::vector<int>& out_children_y
		) const;
		void convert_to_rgba(const cv::Mat& merged, cv::Mat& final_png) const;
		void merge_lists(std::vector<int>& output, const std::vector<std::vector<int>>& lists) const;
		
		const struct TileCreationSettings settings;

	public:
		TileCreator(const Graph& graph, const PathsDS& paths_ds, const std::string tile_dir, const LevelExtractor& level_extractor, size_t nr_of_threads =1, const bool log_tiling=false);
		void generate_tiles_for_level(const int z) const;
		void generate_tiles_for_level_range(const int& z_lower, const int& z_upper, bool reuse_last_round_indices = true) const;
		bool generate_tile(
			cv::Mat& result_image,
			const BoundingBox& tile_bb, 
			const double& lon_normalization, 
			const double& lat_normalization,
			const int& thickness,
			const int& alpha,
			const Level& unpacking_lvl
		) const;
		
		const LevelExtractor level_extractor;
		int thickness_for_level(const int z) const;
		int alpha_for_level(const int z) const;
	};



	class PathboxTileCreator : public TileCreator
	{
	private:
		PathboxTileCreator();
		const BoundingBoxes& path_boxes;
		void _generate_tile(cv::Mat& merged, const BoundingBox& tile_bb,const double& lon_normalization, const double& lat_normalization, const int& thickness, const int& alpha, const Level& unpacking_lvl) const override;
		void linear_sweep_path_matrix_addition(cv::Mat& merged, const BoundingBox& tile_bb,const double& lon_normalization, const double& lat_normalization, const int& thickness, const int& alpha, const Level& unpacking_lvl) const;
		
	public: 
		PathboxTileCreator(
			const Graph& graph, 
			const PathsDS& paths_ds, 
			const std::string tile_dir, 
			const BoundingBoxes& path_boxes, 
			const LevelExtractor& level_extractor, 
			size_t nr_of_threads =1,
			const bool log_tiling = 0);
		
	};




	class PathfinderTileCreator : public TileCreator
	{
	private:
		PathfinderTileCreator();
		Pathfinder& pathfinder;
		void _generate_tile(cv::Mat& merged, const BoundingBox& tile_bb,const double& lon_normalization, const double& lat_normalization, const int& thickness, const int& alpha, const Level& unpacking_lvl) const override;
		void pathfinder_request_matrix_addition(cv::Mat& merged, const BoundingBox& tile_bb, const double& lon_normalization, const double& lat_normalization, const int& thickness, const int& alpha, const Level& unpacking_lvl) const;
	
	public: 
		PathfinderTileCreator(
			const Graph& graph, 
			const PathsDS& paths_ds,
			Pathfinder& pathfinder, 
			const std::string tile_dir, 
			const LevelExtractor& level_extractor,
			const bool log_tiling =0);
		
	};
	

} // namespace pf
