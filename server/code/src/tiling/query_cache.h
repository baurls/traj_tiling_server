#pragma once
#pragma once

#include <string>
#include <vector>

#include "../query.h"

/*
	# # # # # # # # # # # # # # # # # # # # # # # # # #
	#                                                 #
	#   Created by Lukas Baur, Master Thesis 2021.    #
	#                                                 #
	#   Supervisor: T. Rupp, Prof. S. Funke           #
	#   Institute:  FMI (University of Stuttgart)     #
	#                                                 #
	# # # # # # # # # # # # # # # # # # # # # # # # # #
*/


namespace pf
{
	using CacheSize = size_t;
	using SessionID = uint;

	class QueryCache
	{
	private:
		const CacheSize cache_size;
		CacheSize next_free_index = 0;

		std::vector<SessionID> session_ids;
		std::vector<Query> stored_queries;
		std::vector<PathIDs> stored_results;

		QueryCache();
		
	public:
		QueryCache(const CacheSize& cache_size);
		
		SessionID create_new_session(const Query& query);
		bool session_open(const SessionID& session_id) const;

		PathIDs pull_result(const SessionID session_id) const;
		bool put_result(const PathIDs found_paths, const SessionID session_id);
	};

	

} // namespace pf
