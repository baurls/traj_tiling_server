#include "query_cache.h"
#include "interactive_service.h"
#include "util/folder_util.h"
#include "util/tile_util.h"

#include <iostream>
#include <string>


/*
	# # # # # # # # # # # # # # # # # # # # # # # # # #
	#                                                 #
	#   Created by Lukas Baur, Master Thesis 2021.    #
	#                                                 #
	#   Supervisor: T. Rupp, Prof. S. Funke           #
	#   Institute:  FMI (University of Stuttgart)     #
	#                                                 #
	# # # # # # # # # # # # # # # # # # # # # # # # # #
*/



namespace pf
{
 
	/* 
	   +++++++++++++++++++++++++++++++++++++++
	   +++  InteractiveTileService CODE   ++++
	   +++++++++++++++++++++++++++++++++++++++
	*/

	InteractiveTileService::InteractiveTileService(const std::string tile_dir, const Graph& graph, Pathfinder& pathfinder, const CacheSize& cache_size, const PathboxTileCreator& tile_creator) :  cache(cache_size), pathfinder(pathfinder), graph(graph), tile_creator(tile_creator){

	}

	SessionID InteractiveTileService::create_session_from_query(const Query& query){
		PathIDs found_paths = pathfinder.run(query); 		
		SessionID session_id = cache.create_new_session(query);
		cache.put_result(found_paths, session_id);
		return session_id;
	}

	void InteractiveTileService::handle_tile_request(const Pistache::Rest::Request& request, Pistache::Http::ResponseWriter& response) const {
		// Allow all methods for access control
		response.headers().add<Pistache::Http::Header::ContentType>(MIME(Image, Png));
		response.headers().add<Pistache::Http::Header::AccessControlAllowHeaders>("Origin, X-Requested-With, Content-Type, Accept, Authorization");
		response.headers().add<Pistache::Http::Header::AccessControlAllowMethods>("GET, PUT, POST, DELETE, PATCH, OPTIONS");

		int x = request.param(":x").as<int>();
		int y = request.param(":y").as<int>();
		int z = request.param(":z").as<int>();
		SessionID session_id = (SessionID) request.param(":session_id").as<SessionID>();
		
		Print("Tile Request retrieved: (id=" << session_id << "z=" << z << " x=" << x << " y=" << y << ")") ;
		
		if(!cache.session_open(session_id)){
			Print("Session is no longer open. Resend your request.") ;
			response.send(Pistache::Http::Code::Request_Timeout, "Session is no longer open. Resend your request.");
			return;
		}



		// Calculate the tiles 
		// (implemented very basic right now)

		const double offset_lat = TileCalculationUtil::tiley2lat(y, z);
		const double next_lat = TileCalculationUtil::tiley2lat(y+1, z);
		const double offset_lon = TileCalculationUtil::tilex2long(x, z);
		const double next_lon = TileCalculationUtil::tilex2long(x+1, z);

		const double lon_normalization = 512 / (next_lon - offset_lon) ;
		auto lat_normalization = 512 / (next_lat - offset_lat);

		Coordinate min_coordinate = Coordinate(next_lat, offset_lon);
		Coordinate max_coordinate = Coordinate(offset_lat, next_lon);
		
		const BoundingBox tile_bb = BoundingBox(min_coordinate, max_coordinate);
		auto thickness = tile_creator.thickness_for_level(z);
		auto alpha = tile_creator.alpha_for_level(z);
		const Level unpacking_lvl = z > 13? 0 : tile_creator.level_extractor.extract_level(z, graph);

		cv::Mat result_png;
		
		// create image
		bool success = tile_creator.generate_tile(result_png, tile_bb, lon_normalization, lat_normalization, thickness, alpha, unpacking_lvl);
		if(!success){
			// send empty image
			Print("↳ sending empty image!");
			response.send(Pistache::Http::Code::Ok, "");
		}
	

		std::vector<unsigned char> buffer;
		if(result_png.empty()){
			std::cout << "| Image matrix was empty!" << std::endl;
		}
		else{
			cv::imencode(".png", result_png, buffer);
		}

		std::string return_png = std::string(buffer.begin(), buffer.end());
		response.send(Pistache::Http::Code::Ok, return_png);	

		Print("↳ sending calculated image!");
		
		

	}


	

} // namespace pf


