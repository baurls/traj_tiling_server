#include "static_service.h"
#include "util/folder_util.h"


#include <opencv2/opencv.hpp>

#include <string>
#include <sys/stat.h>
#include <fstream>


/*
	# # # # # # # # # # # # # # # # # # # # # # # # # #
	#                                                 #
	#   Created by Lukas Baur, Master Thesis 2021.    #
	#                                                 #
	#   Supervisor: T. Rupp, Prof. S. Funke           #
	#   Institute:  FMI (University of Stuttgart)     #
	#                                                 #
	# # # # # # # # # # # # # # # # # # # # # # # # # #
*/



namespace pf
{
 
	/* 
	   +++++++++++++++++++++++++++++++++++++++
	   ++++    StaticTileService CODE    +++++
	   +++++++++++++++++++++++++++++++++++++++
	*/

	StaticTileService::StaticTileService(const std::string& tile_dir) : tile_dir(tile_dir){
		if(!FolderUtil::dirExists(tile_dir)){
			throw std::invalid_argument( "ERROR: Directoy does not exit: " + tile_dir);
		}
	}

	void StaticTileService::handle_tile_request(const Pistache::Rest::Request& request, Pistache::Http::ResponseWriter& response) const {
		// Allow all methods for access control
		response.headers().add<Pistache::Http::Header::ContentType>(MIME(Image, Png));
		response.headers().add<Pistache::Http::Header::AccessControlAllowHeaders>("Origin, X-Requested-With, Content-Type, Accept, Authorization");
		response.headers().add<Pistache::Http::Header::AccessControlAllowMethods>("GET, PUT, POST, DELETE, PATCH, OPTIONS");
	
		int x = request.param(":x").as<int>();
		int y = request.param(":y").as<int>();
		int z = request.param(":z").as<int>();

		Print("Tile Request retrieved: (z=" << z << " x=" << x << " y=" << y << ")") ;

		std::ostringstream oss;
		oss << tile_dir << "/" << z <<  "/" <<  x << "/" << y << ".png";
		std::string image_path = oss.str();
	
		cv::Mat img = cv::imread(image_path, cv::IMREAD_UNCHANGED);
		std::vector<unsigned char> buffer;
		if(img.empty()){
			std::cout << "| Could not read the image: " << image_path << std::endl;
		}
		else{
			cv::imencode(".png", img, buffer);
		}

		std::string return_png = std::string(buffer.begin(), buffer.end());
		response.send(Pistache::Http::Code::Ok, return_png);	

		Print("↳ Done!");
	}

	

} // namespace pf


