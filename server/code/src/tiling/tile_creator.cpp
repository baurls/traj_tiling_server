#include "tile_creator.h"
#include "util/tile_util.h"
#include "util/folder_util.h"

#include "../defs.h"
#include "../paths_ds.h"
#include "../time_tracker.h"
#include "../assert.h"
#include "../query.h"

#include <string>
#include <sys/stat.h>
#include <fstream>
#include <iomanip>
#include <ctime>


/*
	# # # # # # # # # # # # # # # # # # # # # # # # # #
	#                                                 #
	#   Created by Lukas Baur, Master Thesis 2021.    #
	#                                                 #
	#   Supervisor: T. Rupp, Prof. S. Funke           #
	#   Institute:  FMI (University of Stuttgart)     #
	#                                                 #
	# # # # # # # # # # # # # # # # # # # # # # # # # #
*/



namespace pf
{
 
	/* 
	   +++++++++++++++++++++++++++++++++++++++
	   +++++++    TileCreator CODE    ++++++++
	   +++++++++++++++++++++++++++++++++++++++
	*/

	void log(std::ofstream& log_file, const std::string& log_message){
		auto t = std::time(nullptr);
		auto tm = *std::localtime(&t);
		log_file << "[" <<  std::put_time(&tm, "%d-%m-%Y %H-%M-%S") << "] " << log_message << std::endl;
	}
	
	void output_time(){
		auto t = std::time(nullptr);
		auto tm = *std::localtime(&t);
		std::cout << "[ Current time: " <<  std::put_time(&tm, "%d-%m-%Y %H-%M-%S") << " ]" << std::endl;
	}
	void _create_file(std::string& filename){
		std::ofstream file;
		file.open(filename);  
		file << "";
		file.close();
		output_time();
		std::cout << "logfile created: " << filename;
	}

	TileCreator::TileCreator(Graph const& graph, const PathsDS& paths_ds, const std::string tile_dir, const LevelExtractor& level_extractor, size_t nr_of_threads, bool log_tiling) : graph(graph), graph_bb(BoundingBox(graph)), paths_ds(paths_ds), tile_dir(tile_dir), level_extractor(level_extractor), nr_of_threads(nr_of_threads), log_tiling(log_tiling) {
		std::cout << "Creation finished. " << std::endl;
		if(!FolderUtil::dirExists(tile_dir)){
			throw std::invalid_argument( "ERROR: Directoy does not exit: " + tile_dir);
		}
		if(log_tiling){
			_create_file(tilelog_filename);
		}
	}

	int TileCreator::thickness_for_level(const int z) const{
		const int thickness = settings.base_thickness + (z/10);
		return thickness;
	}

	int TileCreator::alpha_for_level(const int z) const{
		const int thickness = settings.base_alpha + 0.8*z;
		return thickness;
	}

	void TileCreator::generate_tiles_for_level_range(const int& z_lower, const int& z_upper, bool reuse_last_round_indices) const{
		if (reuse_last_round_indices){
			generate_tiles_for_level_range_based_on_last_round(z_lower, z_upper);
		}
		else{
			generate_tiles_for_level_range_naively(z_lower, z_upper);
		}
		output_time();
	}
	
	void TileCreator::generate_tiles_for_level_range_naively(const int& z_lower, const int& z_upper) const{
		for (int z=z_lower;  z <= z_upper; z++){
			generate_tiles_for_level(z);
		}
	}

	void TileCreator::generate_tiles_for_level_range_based_on_last_round(const int& z_lower, const int& z_upper) const{
		auto startpoint = graph_bb.min_coordinate;
		auto endpoint = graph_bb.max_coordinate;
		
		int start_x = TileCalculationUtil::long2tilex(startpoint.lon, z_lower);
		int end_x = TileCalculationUtil::long2tilex(endpoint.lon, z_lower);

		int end_y = TileCalculationUtil::lat2tiley(startpoint.lat, z_lower);
		int start_y = TileCalculationUtil::lat2tiley(endpoint.lat, z_lower);
		
		std::vector<int> indices_x;
		std::vector<int> indices_y;

		for(int x=start_x; x <= end_x; x++){
			for(int y=start_y; y <= end_y; y++){
				indices_x.push_back(x);
				indices_y.push_back(y);
			}
		}

		for (int z=z_lower;  z <= z_upper; z++){
			std::vector<int> out_children_x;
			std::vector<int> out_children_y;
			generate_tiles_for_level(z, indices_x, indices_y, out_children_x, out_children_y);
			
			indices_x = out_children_x;
			indices_y = out_children_y;
		}

		

	}

	/*
		Generates all tiles for level z.
		- Method: Goes through all cells in the z-layer bounding box
	*/
	void TileCreator::generate_tiles_for_level(const int z) const{
		auto startpoint = graph_bb.min_coordinate;
		auto endpoint = graph_bb.max_coordinate;
		
		int start_x = TileCalculationUtil::long2tilex(startpoint.lon, z);
		int end_x = TileCalculationUtil::long2tilex(endpoint.lon, z);

		int end_y = TileCalculationUtil::lat2tiley(startpoint.lat, z);
		int start_y = TileCalculationUtil::lat2tiley(endpoint.lat, z);
		
		std::cout << "Generating tiles on level " << z << "..." << std::endl;
		std::cout << "| # tiles in x-dir: "<< end_x - start_x  + 1<< std::endl;
		std::cout << "| # tiles in y-dir: "<< end_y - start_y + 1 << std::endl;
		std::cout << "| # tiles:          "<< (end_x - start_x + 1) * (end_y - start_y + 1) << std::endl;
		
		const int thickness = thickness_for_level(z);
		const int alpha = alpha_for_level(z);
		const Level unpacking_lvl = z > 16? 0 : level_extractor.extract_level(z, graph);
		
		// return here if dry run

		create_folder(z);
		for(int x = start_x; x <= end_x; x++){
			create_folder(z, x);
			const double offset_lon = TileCalculationUtil::tilex2long(x, z);
			const double next_lon = TileCalculationUtil::tilex2long(x+1, z);
			const double lon_normalization = settings.tile_size_x / (next_lon - offset_lon) ;

#pragma omp parallel for num_threads(nr_of_threads) schedule(guided)
			for(int y = start_y; y <= end_y; y++){
				// create pathname from x,y,z
				std::ostringstream oss;
				oss << tile_dir << "/" << z <<  "/" <<  x << "/" << y << ".png";
				std::string path = oss.str();

				const double offset_lat = TileCalculationUtil::tiley2lat(y, z);
				const double next_lat = TileCalculationUtil::tiley2lat(y+1, z);

				// create tile-bb
				Coordinate min_coordinate = Coordinate(next_lat, offset_lon);
				Coordinate max_coordinate = Coordinate(offset_lat, next_lon);
				
				auto lat_normalization = settings.tile_size_y / (next_lat - offset_lat);


				const BoundingBox tile_bb = BoundingBox(min_coordinate, max_coordinate);
				

				// create tile x, y, z
				generate_tile_and_store(tile_bb, lon_normalization, lat_normalization, path, thickness, alpha, unpacking_lvl);
				
				// for debugging:
				// std::cout << "x=" << x << " y=" << y << std::endl;
			}	
		}
	}

	/*
		Generates all tiles for level z.
		- Method: Use hasChildres Indices from layer z-1 
	*/
	void TileCreator::generate_tiles_for_level(
		const int z, 
		const std::vector<int>& indices_x, 
		const std::vector<int>& indices_y,
		std::vector<int>& out_children_x,
		std::vector<int>& out_children_y
		) const{
		assert(indices_x.size() == indices_y.size());

		output_time();
		std::cout << "Generating tiles on level " << z << " based on previous level.." << std::endl;
		std::cout << "| # input-tiles:          " << indices_x.size() << std::endl;
		
		const int thickness = thickness_for_level(z);
		const int alpha = alpha_for_level(z);
		// const Level unpacking_lvl = z > 13? 0 : level_extractor.extract_level(z, graph);
		const Level unpacking_lvl = level_extractor.extract_level(z, graph);

		create_folder(z);
		// Declaring new vector
    	std::vector<int> disjoint_x(indices_x);
		std::sort(disjoint_x.begin(), disjoint_x.end());
		auto it = std::unique(disjoint_x.begin(), disjoint_x.end());
		disjoint_x.resize(std::distance(disjoint_x.begin(),it) );
		
		std::ofstream log_file;
		if(log_tiling){
			log_file.open(tilelog_filename, std::ios::out | std::ios::app);  // append to file
		}
		
		for (const int& x :  disjoint_x){
			create_folder(z, x);
		}



		std::vector<std::vector<int>> out_children_collected_x(nr_of_threads);
		std::vector<std::vector<int>> out_children_collected_y(nr_of_threads);


#pragma omp parallel num_threads(nr_of_threads)
		{
		std::vector<int>& my_x_childs = out_children_collected_x[omp_get_thread_num()];
		std::vector<int>& my_y_childs = out_children_collected_y[omp_get_thread_num()];
				
#pragma omp for schedule(guided)
			for(size_t i = 0; i < indices_x.size(); i++){
				const int& x = indices_x[i];
				const int& y = indices_y[i];

				const double& offset_lon = TileCalculationUtil::tilex2long(x, z);
				const double& next_lon = TileCalculationUtil::tilex2long(x+1, z);
				const double& lon_normalization = settings.tile_size_x / (next_lon - offset_lon);
				
				auto offset_lat = TileCalculationUtil::tiley2lat(y, z);
				auto next_lat = TileCalculationUtil::tiley2lat(y+1, z);
				auto lat_normalization = settings.tile_size_y / (next_lat - offset_lat);

				// create pathname from x,y,z
				std::ostringstream oss;
				oss << tile_dir << "/" << z <<  "/" <<  x << "/" << y << ".png";
				std::string path = oss.str();

				// create tile-bb
				const Coordinate& min_coordinate = Coordinate(next_lat, offset_lon);
				const Coordinate& max_coordinate = Coordinate(offset_lat, next_lon);
				const BoundingBox tile_bb = BoundingBox(min_coordinate, max_coordinate);
				
				// create tile x, y, z
				bool has_childred = generate_tile_and_store(tile_bb, lon_normalization, lat_normalization, path, thickness, alpha, unpacking_lvl);
				// for debugging:
				// std::cout << "x=" << x << " y=" << y << " children= " << has_childred << std::endl;
				if (log_tiling)
				{
					log(log_file, std::to_string(z) + "-" + std::to_string(x) + "-" + std::to_string(y));
				}

				if(has_childred){
					my_x_childs.push_back(2*x);
					my_y_childs.push_back(2*y);

					my_x_childs.push_back(2*x);
					my_y_childs.push_back(2*y+1);

					my_x_childs.push_back(2*x+1);
					my_y_childs.push_back(2*y);

					my_x_childs.push_back(2*x+1);
					my_y_childs.push_back(2*y+1);
				}
			}
		}
		// merge results
		merge_lists(out_children_x, out_children_collected_x);
		merge_lists(out_children_y, out_children_collected_y);


		if (log_tiling)
		{
			log_file.close();
		}

	}

	void TileCreator::merge_lists(std::vector<int>& output, const std::vector<std::vector<int>>& lists) const{
		// determine size
		size_t total_size = 0;
		for(size_t i = 0; i < lists.size(); i++){
			auto list = lists[i];
			total_size += list.size();
		}
		output.reserve(total_size);
		
		// copy elements
		for(size_t i = 0; i < lists.size(); i++){
			auto list = lists[i];
			output.insert(output.end(), list.begin(), list.end());  // add vector i;
		}

	}

	void TileCreator::create_folder(const int z, const int x) const{
		std::ostringstream oss;
		oss << tile_dir << "/" << z <<  "/" <<  x;
		auto path = oss.str();
		FolderUtil::create_folder(path);
	}

	void TileCreator::create_folder(const int z) const{
		std::ostringstream oss;
		oss << tile_dir << "/" << z;	
		auto path = oss.str();
		FolderUtil::create_folder(path);
	}

	/* 
	generates a tile-image based on the view-box by scanning 
	all paths linearly (with early-skipping bounding box check)
	*/ 
	bool TileCreator::generate_tile_and_store(
		const BoundingBox& tile_bb,
		const double& lon_normalization, 
		const double& lat_normalization,
		const std::string& path,
		const int& thickness,
		const int& alpha, 
		const Level& unpacking_lvl
	) const{
		
		cv::Mat result_png;
		
		// create image
		bool success = generate_tile(result_png, tile_bb, lon_normalization, lat_normalization, thickness, alpha, unpacking_lvl);
		if(!success){
			return false;
		}

		// write image to disk
		cv::imwrite(path, result_png);  
		return true;
	}

	/* 
	generates a tile-image based on the view-box by scanning 
	all paths linearly (with early-skipping bounding box check)
	*/ 
	bool TileCreator::generate_tile(
		cv::Mat& final_png,
		const BoundingBox& tile_bb,
		const double& lon_normalization, 
		const double& lat_normalization,
		const int& thickness,
		const int& alpha, 
		const Level& unpacking_lvl
	) const{
		

		// create initial image
		cv::Mat single_channel_img = cv::Mat::zeros(settings.tile_size_x, settings.tile_size_y, CV_8UC1);

		// plot paths
		_generate_tile(single_channel_img, tile_bb, lon_normalization, lat_normalization, thickness, alpha, unpacking_lvl);
		
		bool is_empty = cv::sum(single_channel_img)[0] == 0;
		if(is_empty){
			// don't save the image, since it's empty anyway.
			return false;
		}

		// create colored image version (1 channel -> 4 channels)
		convert_to_rgba(single_channel_img, final_png);

		return true;
	}

	void TileCreator::_generate_tile(cv::Mat& merged, const BoundingBox& tile_bb,const double& lon_normalization, const double& lat_normalization, const int& thickness, const int& alpha, const Level& unpacking_lvl) const{
		// OVERRIDE
	}
	
	
	/*
		Input:  Matrix with density-values from ℕ
		Output: Image-Matrix with 4 channels (rgba)
	*/
	void TileCreator::convert_to_rgba(const cv::Mat& merged, cv::Mat& output_png) const{
		const cv::Mat empty = cv::Mat::zeros(settings.tile_size_x, settings.tile_size_y, CV_8UC1);
		const cv::Mat ones = cv::Mat::ones(settings.tile_size_x, settings.tile_size_y, CV_8UC1) * 255;
		
		std::vector<cv::Mat> channels;
		channels.push_back(ones);
		channels.push_back(empty);
		channels.push_back(empty);
		channels.push_back(merged);

    	cv::merge(channels, output_png);
	}



	PathboxTileCreator::PathboxTileCreator
	(
			const Graph& graph, 
			const PathsDS& paths_ds,
			const std::string tile_dir, 
			const BoundingBoxes& path_boxes,
			const LevelExtractor& level_extractor,
			const size_t nr_of_threads,
			const bool log_tiling		
	) :  TileCreator(graph, 
			paths_ds,
			tile_dir, 
			level_extractor, 
			nr_of_threads,
			log_tiling), 
			path_boxes(path_boxes){

	}

	void PathboxTileCreator::_generate_tile(cv::Mat& merged, const BoundingBox& tile_bb,const double& lon_normalization, const double& lat_normalization, const int& thickness, const int& alpha, const Level& unpacking_lvl) const{
		linear_sweep_path_matrix_addition(merged, tile_bb, lon_normalization, lat_normalization, thickness, alpha, unpacking_lvl);
	}
	

	/*
	Go through all paths. Plot each path which intersects the visible bounding box.
	*/
	void PathboxTileCreator::linear_sweep_path_matrix_addition(cv::Mat& merged, const BoundingBox& tile_bb,const double& lon_normalization, const double& lat_normalization, const int& thickness, const int& alpha, const Level& unpacking_lvl) const{
		
		for (PathID path_id = 0; path_id < (PathID)paths_ds.getNrOfPaths(); ++path_id) {

			// check for skip if path-bb does not intersect tile
			const BoundingBox path_bb = path_boxes[path_id];
			if(! path_bb.overlaps_with(tile_bb)){
				// skip this path
				continue;
			}
			
			// create new layer for path
			cv::Mat image = cv::Mat::zeros(settings.tile_size_x, settings.tile_size_y, CV_8UC1);

			// extract path's points
			// (1) unpack path		
			auto const& unpacked_path = paths_ds.getPath(path_id).unpack(graph, unpacking_lvl);
			// (2) copy points
			std::vector<cv::Point> pts;
			pts.reserve(unpacked_path.getNoOfRootEdges());
			for (auto node_id : unpacked_path.getNodeRange(graph)){
				const auto node = graph.getNode(node_id);
				const auto coordinate = node.coordinate;
				const auto abs_x =  (coordinate.lon - tile_bb.min_coordinate.lon) * lon_normalization;
				const auto abs_y =  (coordinate.lat - tile_bb.max_coordinate.lat) * lat_normalization;
				pts.push_back(cv::Point(abs_x, abs_y));
			}

			// write to image
			cv::polylines(image, pts, false, alpha, thickness, settings.line_type);
			merged += image;
		}

	}




	PathfinderTileCreator::PathfinderTileCreator
	(
			const Graph& graph, 
			const PathsDS& paths_ds,
			Pathfinder& pathfinder, 
			const std::string tile_dir, 
			const LevelExtractor& level_extractor,
			const bool log_tiling
	) :  TileCreator(graph, 
			paths_ds,
			tile_dir, 
			level_extractor, 
			1, // only one thread possible for
			log_tiling), 
			pathfinder(pathfinder){

	}

	void PathfinderTileCreator::_generate_tile(cv::Mat& merged, const BoundingBox& tile_bb,const double& lon_normalization, const double& lat_normalization, const int& thickness, const int& alpha, const Level& unpacking_lvl) const{
		pathfinder_request_matrix_addition(merged, tile_bb, lon_normalization, lat_normalization, thickness, alpha, unpacking_lvl);
	}

	/*
	Use pathfinder to obtain paths. Plot each path which intersects the visible bounding box.
	*/
	void PathfinderTileCreator::pathfinder_request_matrix_addition(cv::Mat& merged, const BoundingBox& tile_bb, const double& lon_normalization, const double& lat_normalization, const int& thickness, const int& alpha, const Level& unpacking_lvl) const{
		const Query q(tile_bb);
		PathIDs path_ids = pathfinder.run(q);


		// std::cout << "#paths="<< path_ids.size()  << std::endl;
		
		for (PathID path_id : path_ids) {
			// create new layer for path
			cv::Mat image = cv::Mat::zeros(settings.tile_size_x, settings.tile_size_y, CV_8UC1);

			// extract path's points
			// (1) unpack path		
			auto const& unpacked_path = paths_ds.getPath(path_id).unpack(graph, unpacking_lvl);
			// (2) copy points
			std::vector<cv::Point> pts;
			pts.reserve(unpacked_path.getNoOfRootEdges());
			for (auto node_id : unpacked_path.getNodeRange(graph)){
				const auto& node = graph.getNode(node_id);
				const auto& coordinate = node.coordinate;
				const auto abs_x =  (coordinate.lon - tile_bb.min_coordinate.lon) * lon_normalization;
				const auto abs_y =  (coordinate.lat - tile_bb.max_coordinate.lat) * lat_normalization;
				pts.push_back(cv::Point(abs_x, abs_y));
			}

			// write to image
			cv::polylines(image, pts, false, alpha, thickness, settings.line_type);
			merged += image;
		}

	}
	

} // namespace pf


