#include "query_cache.h"

#include <iostream>
#include <string>
#include <cstdlib> 


/*
	# # # # # # # # # # # # # # # # # # # # # # # # # #
	#                                                 #
	#   Created by Lukas Baur, Master Thesis 2021.    #
	#                                                 #
	#   Supervisor: T. Rupp, Prof. S. Funke           #
	#   Institute:  FMI (University of Stuttgart)     #
	#                                                 #
	# # # # # # # # # # # # # # # # # # # # # # # # # #
*/



namespace pf
{
 	
	 
	SessionID get_random_number() {
		return 10000000  + std::rand() % 10000000; 
	}


	/* 
	   +++++++++++++++++++++++++++++++++++++++
	   +++++++    QueryCache CODE     ++++++++
	   +++++++++++++++++++++++++++++++++++++++
	*/


	QueryCache::QueryCache(const CacheSize& cache_size) :  
	cache_size(cache_size)
	, session_ids(cache_size)
	, stored_queries(cache_size)
	, stored_results(cache_size)
	{
		std::cout << "Creation finished. " << std::endl;
	}

	
	SessionID QueryCache::create_new_session(const Query& query){
		SessionID session_id = get_random_number() * cache_size + next_free_index;
		Query query_copy(query.bounding_box, query.time_intervall, query.time_slots);
		stored_queries[next_free_index] = query_copy;
		session_ids[next_free_index] = session_id;
		next_free_index = (next_free_index+1) % cache_size;
		return session_id;
	}

	bool QueryCache::put_result(const PathIDs found_paths, const SessionID session_id){
		stored_results[session_id % cache_size] = found_paths;
	}

	PathIDs QueryCache::pull_result(const SessionID session_id) const{
		return stored_results[session_id % cache_size];
	}

	bool QueryCache::session_open(const SessionID& session_id) const{
		SessionID reference_session_id = session_ids[session_id % cache_size];
		return reference_session_id == session_id;
	}
	

} // namespace pf


