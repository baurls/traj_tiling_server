#include "heatmap_builder.h"

#include "defs.h"
#include "paths_ds.h"
#include "time_tracker.h"
#include "assert.h"
#include <fstream>


/*
	# # # # # # # # # # # # # # # # # # # # # # # # # #
	#                                                 #
	#   Created by Lukas Baur, Master Thesis 2021.    #
	#                                                 #
	#   Supervisor: T. Rupp, Prof. S. Funke           #
	#   Institute:  FMI (University of Stuttgart)     #
	#                                                 #
	# # # # # # # # # # # # # # # # # # # # # # # # # #
*/



namespace pf
{
	void HeatmapBuilder::print() const{
		std::cout << "HeatmapBuilder:<not implemented yet>"  << std::endl;
	}

	HeatmapBuilder::HeatmapBuilder(Graph const &graph, PathsDS const &paths_ds) : graph(graph), paths_ds(paths_ds)
	{
		
	}


	RelCounterVector HeatmapBuilder::get_truncated_rel_nodes_counter(size_t max_val) const{
		auto abs_vec = get_absolute_nodes_counter();

		auto float_max_val = static_cast<float>(max_val);

		auto n = graph.getAllNodes().size();
		RelCounterVector counter_vec(n, 1.0f);
		for(size_t i = 0; i < n; i++){
			if (abs_vec[i] <= max_val){
				counter_vec[i] = static_cast<float>(abs_vec[i]) / float_max_val;  
			}
		}
		return counter_vec;
	}


	CounterVector HeatmapBuilder::get_absolute_nodes_counter() const{
		TimeTracker time_tracker;
		time_tracker.start();
		
		auto n = graph.getAllNodes().size();
		CounterVector counter_vec(n, 0);

		for (PathID path_id = 0; path_id < paths_ds.getNrOfPaths(); path_id++){
			Path path = paths_ds.getPath(path_id);
			for (NodeID node_id : path.getNodeRange(graph)){
				counter_vec[node_id] += 1;
			}
		}
		time_tracker.stop();
		return counter_vec;
	}

} // namespace pf


