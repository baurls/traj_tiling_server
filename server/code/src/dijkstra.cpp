#include "dijkstra.h"

#include "defs.h"

#include <algorithm>

namespace pf
{

//
// Member functions of Dijkstra
//

Dijkstra::Dijkstra(Graph const& graph)
    : graph(graph)
    , found_by(graph.getNumberOfNodes())
    , distances(graph.getNumberOfNodes(), c::NO_LENGTH)
{
}

void Dijkstra::run(NodeID source, NodeID target)
{
	reset();

	PQ pq;
	pq.emplace(source, c::NO_EID, 0);
	distances[source] = 0;
	reset_distances.push_back(source);

	// Dijkstra loop
	while (!pq.empty() && pq.top().node != target) {
		PQElement top(pq.top());
		pq.pop();

		if (distances[top.node] == top.distance) {
			found_by[top.node] = top.found_by;
			relaxAllEdges(pq, top);
		}
	}

	// Only fill the DijkstraResult if we actually found a path
	if (!pq.empty()) {
		result.path_found = true;
		result.path_length = pq.top().distance;

		buildPath(source, target, pq);
	}
}

void Dijkstra::relaxAllEdges(PQ& pq, PQElement const& top)
{
	for (auto const& edge : graph.getOutEdgesOf(top.node)) {
		if (edge.isShortcut()) {
			continue;
		}

		NodeID target(edge.target);
		Length new_distance(top.distance + edge.length);

		if (new_distance < distances[target]) {
			if (distances[target] == c::NO_LENGTH) {
				reset_distances.push_back(target);
			}
			distances[target] = new_distance;

			pq.emplace(target, edge.id, new_distance);
		}
	}
}

void Dijkstra::buildPath(NodeID source, NodeID target, PQ const& pq)
{
	EdgeIDs edges;
	NodeID bt_node(target);
	found_by[target] = pq.top().found_by;

	while (bt_node != source) {
		auto edge_id = found_by[bt_node];
		bt_node = graph.getEdge(edge_id).source;
		edges.push_back(edge_id);
	}

	std::reverse(edges.begin(), edges.end());

	result.path.init(graph, edges);
}

DijkstraResult const& Dijkstra::getResult() const
{
	return result;
}

DijkstraResult Dijkstra::stealResult()
{
	return std::move(result);
}

void Dijkstra::reset()
{
	for (auto node : reset_distances) {
		distances[node] = c::NO_LENGTH;
	}
	reset_distances.clear();

	result.clear();
}

//
// Member functions of CHDijkstra
//

CHDijkstra::CHDijkstra(Graph const& graph) : graph(graph)
{
	for (auto& dir_info : dir) {
		dir_info.distances.resize(graph.getNumberOfNodes(), c::NO_LENGTH);
		dir_info.found_by.resize(graph.getNumberOfNodes());
	}
}

void CHDijkstra::run(NodeID source, NodeID target)
{
	reset();

	PQ pq;
	pq.emplace(source, c::NO_EID, Direction::Forward, 0);
	pq.emplace(target, c::NO_EID, Direction::Backward, 0);

	dir[Direction::Forward].distances[source] = 0;
	dir[Direction::Forward].reset_distances.push_back(source);
	dir[Direction::Backward].distances[target] = 0;
	dir[Direction::Backward].reset_distances.push_back(target);

	// Dijkstra loop
	Length shortest_distance(c::NO_LENGTH);
	NodeID center_node = c::NO_NID;
	while (!pq.empty() && pq.top().distance <= shortest_distance) {
		PQElement top(pq.top());
		pq.pop();

		if (dir[top.direction].distances[top.node] == top.distance) {
			// relax edges
			dir[top.direction].found_by[top.node] = top.found_by;
			relaxAllEdges(pq, top);

			// update shortest distance
			Length remaining_distance = dir[!top.direction].distances[top.node];
			if (remaining_distance != c::NO_LENGTH &&
			    top.distance + remaining_distance < shortest_distance) {
				shortest_distance = top.distance + remaining_distance;
				center_node = top.node;
			}
		}
	}

	if (center_node != c::NO_NID) {
		result.path_found = true;
		result.path_length = shortest_distance;

		buildPath(source, center_node, target);
	}
}

DijkstraResult const& CHDijkstra::getResult() const
{
	return result;
}

DijkstraResult CHDijkstra::stealResult()
{
	return std::move(result);
}

void CHDijkstra::buildPath(NodeID source, NodeID center_node, NodeID target)
{
	EdgeIDs path;
	for (Direction direction : {Direction::Backward, Direction::Forward}) {
		NodeID bt_node = center_node;
		NodeID end_node;

		end_node = (direction == Direction::Forward ? source : target);

		while (bt_node != end_node) {
			EdgeID edge_id = dir[direction].found_by[bt_node];
			bt_node = graph.getEdge(edge_id).getTail(direction);
			path.push_back(edge_id);
		}

		// Yes, you have to think about that line a bit. We first backtrack the
		// last part of the path which is in the right order. So, by reversing
		// it we have it in reverse order. By now backtracking the first half of
		// the path (which is backtracked in reverse order) we get the whole
		// path in reverse order. Thus a second reverse brings the path into the
		// right order. Nice, huh?
		std::reverse(path.begin(), path.end());
	}

	result.path.init(graph, path);
}

void CHDijkstra::relaxAllEdges(PQ& pq, PQElement const& top)
{
	Direction direction(top.direction);

	for (auto const& edge : graph.getUpEdgesOf(top.node, direction)) {

		NodeID head_id(edge.getHead(direction));
		Length new_distance(top.distance + edge.length);

		if (new_distance < dir[direction].distances[head_id]) {
			if (dir[direction].distances[head_id] == c::NO_LENGTH) {
				dir[direction].reset_distances.push_back(head_id);
			}
			dir[direction].distances[head_id] = new_distance;

			pq.emplace(head_id, edge.id, direction, new_distance);
		}
	}
}

void CHDijkstra::reset()
{
	for (auto& dir_info : dir) {
		for (auto const node : dir_info.reset_distances) {
			dir_info.distances[node] = c::NO_LENGTH;
		}
		dir_info.reset_distances.clear();
	}

	result.clear();
}

} // namespace pf
