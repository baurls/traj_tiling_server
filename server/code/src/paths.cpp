#include "paths.h"

#include "defs.h"
#include "paths_ds.h"
#include <math.h>
#include <stack>

namespace pf
{

Path::Path()
{
	// one more than the number of edges, although no node is specified yet
	this->timeAtNodes.push_back(c::INVALID_TIME);
}

Path::Path(Graph const& graph, EdgeIDs const& edges)
{
	this->edges.clear();
	this->timeAtNodes.clear();
	// one more than the number of edges, although no node is specified yet
	this->timeAtNodes.push_back(c::INVALID_TIME);

	for (EdgeID edge_id : edges) {
		push(graph, edge_id);
	}

	if (!is_consistent(graph)) {
		throw Exception("The passed edges are not consistent.");
	}
}

Path::Path(Graph const& graph, GeoMeasurements& geomeasurements,
    Factors& factors,
    std::unordered_map<OsmNodeId, NodeID> const& osmIdsToGraphIds)
{
	// one more than the number of edges, although no node is specified yet
	this->timeAtNodes.push_back(c::INVALID_TIME);

	for (Factor factor : factors) {
		TimeType start = geomeasurements.at(factor.smplStart).timestamp;
		TimeType end = geomeasurements.at(factor.smplStop).timestamp;
		TimeType diff = end - start;

		// could be done in a different way (as the previous matching, so it's
		// ok)
		if (factor.resPath.size() > 0) {
			double diffPerEdge = ((double)diff / (double)factor.resPath.size());

			int i = 0;
			for (OsmEdge const& osm_edge : factor.resPath) {
				TimeType intervall_start = floor(start + i * diffPerEdge);
				TimeType intervall_end =
				    floor(end - (factor.resPath.size() - i - 1) * diffPerEdge);

				EdgeID edge_id =
				    osmEdgeToEdge(graph, osm_edge, osmIdsToGraphIds);
				assert(!graph.getEdge(edge_id).isShortcut());
				// special handling since the matched paths are saved like that
				if (edges.size() != 0 && i == 0 && edge_id == edges.back()) {
					// it is already the edge from the last factor so we do not
					// need to push it another time, we simply expand the last
					// time interval
				} else {
					push(graph, edge_id);
				}

				if (edges.size() == 1) {
					// we need to set the beginning timestamp for the very first
					// edge
					timeAtNodes.front() = intervall_start;
				}
				timeAtNodes.back() = intervall_end;

				i++;
			}
		}
	}
}

EdgeID Path::osmEdgeToEdge(Graph const& graph, OsmEdge const& osmEdge,
    std::unordered_map<OsmNodeId, NodeID> const& osmIdsToGraphIds) const
{
	NodeID src = osmIdsToGraphIds.at(osmEdge.src);
	auto edges = graph.getOutEdgesOf(src);

	Edge const* foundEdge = NULL;
	for (Edge const& edge : edges) {
		if (edge.target == osmIdsToGraphIds.at(osmEdge.tgt)) {
			foundEdge = &edge;
			break;
		}
	}

	if (foundEdge == NULL) {
		throw Exception("Edge not found.");
	}
	return foundEdge->id;
}

Path& Path::operator=(Path const& path)
{
	this->edges = path.edges;
	this->timeAtNodes = path.timeAtNodes;

	return *this;
}

void Path::init(Graph const& graph, EdgeIDs& edges)
{
	this->edges.clear();
	this->timeAtNodes.clear();
	this->timeAtNodes.push_back(c::INVALID_TIME);

	for (EdgeID edge_id : edges) {
		push(graph, edge_id);
	}
}

void Path::push(EdgeID edge_id)
{
	edges.push_back(edge_id);
	timeAtNodes.push_back(c::INVALID_TIME);
}

void Path::push(Graph const& graph, EdgeID edge_id)
{
	checkPushConsistency(graph, edge_id);

	// this edge is consistent, so push
	edges.push_back(edge_id);
	timeAtNodes.push_back(c::INVALID_TIME);
}

void Path::push(
    Graph const& graph, EdgeID edge_id, TimeIntervall const& time_intervall)
{
	push(graph, edge_id);
	setTimeIntervallForEdge(edges.size() - 1, time_intervall);
}

void Path::checkPushConsistency(Graph const& graph, EdgeID edge_id) const
{
	// checks if the pushed edge would be consistent with the path
	if (!edges.empty()) {
		auto const& last_edge = graph.getEdge(edges.back());
		auto const& new_edge = graph.getEdge(edge_id);

		if (last_edge.target != new_edge.source) {
			throw Exception(
			    "The pushed edge does not match the last edge of the path.");
		}
	}
}

void Path::append(Graph const& graph, Path const& path)
{
	if (path.getNoOfRootEdges() == 0) {
		return;
	} else if (!edges.empty() && getTarget(graph) != path.getSource(graph)) {
		throw Exception("The path that should be appended doesn't match.");
	}

	for (EdgeIndex edge_index = 0; edge_index < path.getEdges().size();
	     edge_index++) {
		push(graph, path.getEdge(edge_index),
		    path.getTimeIntervallForEdge(edge_index));
	}
}

void Path::pop()
{
	if (edges.empty()) {
		throw Exception("Cannot pop from the path as it is empty.");
	}

	edges.pop_back();
}

EdgeID Path::back() const
{
	if (edges.empty()) {
		return c::NO_EID;
	}

	return edges.back();
}

void Path::clear()
{
	edges.clear();
	timeAtNodes.clear();
	timeAtNodes.push_back(c::INVALID_TIME);
}

std::size_t Path::getNoOfRootEdges() const
{
	return edges.size();
}

bool Path::empty() const
{
	return edges.empty();
}

Path Path::unpack(Graph const& graph) const
{
	return _unpack(graph, edges);
}

Path Path::unpack(Graph const& graph, Level const& level) const
{
	if (level <= 0) {
		return _unpack(graph, edges);
	}
	return _unpack(graph, edges, level);
}

Path Path::unpack(Graph const& graph, Level const& level, std::ofstream& outfile) const
{
	if (level <= 0) {
		return _unpack(graph, edges, outfile);
	}
	return _unpack(graph, edges, level);
}


Path Path::_unpack(
    Graph const& graph, PathEdgeIDs const& edge_ids) const
{
	Path path;

	EdgeIDs todo;
	for (EdgeID edge_id : edge_ids) {
		todo.push_back(edge_id);
		while (!todo.empty()) {
			auto const& current_edge = graph.getEdge(todo.back());
			todo.pop_back();

			// Time information is lost here
			if (current_edge.isShortcut()) {
				todo.push_back(current_edge.child_edge2);
				todo.push_back(current_edge.child_edge1);
			} else {
				path.push(graph, current_edge.id);
			}
		}
	}

	return path;
}

Path Path::_unpack(
    Graph const& graph, PathEdgeIDs const& edge_ids, std::ofstream& outfile) const
{
	Path path;

	EdgeIDs todo;
	for (EdgeID edge_id : edge_ids) {
		
		outfile << "§unpack " << edge_id << std::endl;
		
		todo.push_back(edge_id);
		while (!todo.empty()) {
			auto const& current_edge = graph.getEdge(todo.back());
			todo.pop_back();
			outfile << "edge: " << current_edge.id;	
			outfile << " u " << graph.getNode(current_edge.source).id;
			outfile << " " << graph.getNode(current_edge.source).coordinate;
			outfile << " v " << graph.getNode(current_edge.target).id;
			outfile << " " << graph.getNode(current_edge.target).coordinate;	
			outfile << " l: " << current_edge.child_edge1 << " r: " << current_edge.child_edge2 << std::endl;
				
			// Time information is lost here
			if (current_edge.isShortcut()) {
				todo.push_back(current_edge.child_edge2);
				todo.push_back(current_edge.child_edge1);
			} else {
				path.push(graph, current_edge.id);
			}
		}

	}

	return path;
}

Path Path::_unpack(Graph const& graph, PathEdgeIDs const& edge_ids, Level const& level) const
{
	Path path;
	EdgeIDs todo;
	for (EdgeID edge_id : edge_ids) {
		todo.push_back(edge_id);
		while (!todo.empty()) {
			auto const& current_edge = graph.getEdge(todo.back());
			todo.pop_back();

			// Check target level
			if (current_edge.isShortcut()) {
				// Retrieve source & target
				Node source =
				    graph.getNode(current_edge.getHead(Direction::Forward));
				Node target =
				    graph.getNode(current_edge.getTail(Direction::Backward));

				// Verify source and target levels
				if (target.level > level || source.level > level) {
					todo.push_back(current_edge.child_edge2);
					todo.push_back(current_edge.child_edge1);
				} else {
					// finished: level 
					path.push(graph, current_edge.id);
				}
			} else {
				// finished: edge is no shortcut
				path.push(graph, current_edge.id);
			}

		}
	}

	return path;

}

PathEdgeIDs::const_iterator Path::begin() const
{
	return edges.cbegin();
}

PathEdgeIDs::const_iterator Path::end() const
{
	return edges.cend();
}

EdgeID Path::operator[](EdgeIndex index) const
{
	return edges[index];
}

NodeRange Path::getNodeRange(Graph const& graph) const
{
	return NodeRange(graph, *this);
}

NodeRange Path::getNodeRange(
    Graph const& graph, NodeIndex begin, NodeIndex end) const
{
	return NodeRange(graph, *this, begin, end);
}

NodeID Path::getSource(Graph const& graph) const
{
	if (edges.empty()) {
		return c::NO_NID;
	}

	auto const& first_edge = graph.getEdge(edges.front());
	return first_edge.source;
}

NodeID Path::getTarget(Graph const& graph) const
{
	if (edges.empty()) {
		return c::NO_NID;
	}

	auto const& last_edge = graph.getEdge(edges.back());
	return last_edge.target;
}

PathEdgeIDs const& Path::getEdges() const
{
	return edges;
}

PathTimes const& Path::getTimeAtNodes() const
{
	return timeAtNodes;
}

bool Path::is_consistent(Graph const& graph) const
{
	if (edges.empty()) {
		return true;
	}

	NodeID last_target = graph.getEdge(edges.front()).source;
	for (EdgeID edge_id : edges) {
		auto const& edge = graph.getEdge(edge_id);
		if (last_target != edge.source) {
			return false;
		}

		last_target = edge.target;
	}

	return true;
}

NodeID Path::getNode(Graph const& graph, NodeIndex node_index) const
{
	if (node_index < 0 || node_index > (NodeIndex)edges.size()) {
		throw Exception("Out of bounds index passed to Path::getNode.");
	}

	if (node_index == (NodeIndex)edges.size()) {
		return getTarget(graph);
	}

	return graph.getEdge(edges[node_index]).source;
}

EdgeID Path::getEdge(EdgeIndex edge_index) const
{
	return edges[edge_index];
}

EdgeIDRange Path::getPathEdgeRangeForFactor(
    Graph const& graph, EdgeIDs& edge_ids, Factor factor) const
{
	// we assume that no factor exists twice for one trajectory
	for (uint i = 0; i < edge_ids.size(); i++) {
		bool equals = true;
		for (uint j = 0; j < factor.resPath.size(); j++) {
			OsmEdge osmEdge = factor.resPath[j];
			Edge edge = graph.getEdge(edge_ids.at(i + j));

			Node src = graph.getNode(edge.source);
			Node tgt = graph.getNode(edge.target);
			if (!(src.osm_id == osmEdge.src && tgt.osm_id == osmEdge.tgt)) {
				equals = false;
				break;
			}
		}

		if (equals) {
			auto begin = edge_ids.begin() + i;
			auto end = edge_ids.begin() + i + factor.resPath.size();

			return EdgeIDRange(begin, end);
		}
	}

	throw Exception("Error: There should be edges for the factor.");
}

void Path::setTimeIntervallsForCompressed(
    Graph const& graph, Path const& uncompressed_path)
{
	assert(uncompressed_path.isUncompressed(graph));

	// assumption: this is a compressed path
	Path unpacked = this->unpack(graph);
	if (!(unpacked == uncompressed_path)) {
		throw Exception("Error: The argument has to be the corresponding "
		                "uncompressed path.");
	}

	uint index_uncompressed = 0;
	for (EdgeIndex edge_index = 0; edge_index < edges.size(); edge_index++) {
		Path unpackedEdge = unpackEdgeToPath(graph, edges.at(edge_index));

		TimeIntervall union_time_intervall;
		for (uint i = 0; i < unpackedEdge.getEdges().size(); i++) {
			union_time_intervall.expand(
			    uncompressed_path.getTimeIntervallForEdge(index_uncompressed));
			index_uncompressed++;
		}
		this->setTimeIntervallForEdge(edge_index, union_time_intervall);
	}
	assert(index_uncompressed == uncompressed_path.getEdges().size());
}

bool Path::isUncompressed(Graph const& graph) const
{
	for (EdgeID edge_id : edges) {
		if (graph.getEdge(edge_id).isShortcut()) {
			throw Exception("The argument is not a compressed path.");
		}
	}
	return true;
}

void Path::setTimeIntervallsForMoreCompressed(
    Graph const& graph, Path const& lessCompressed)
{
	assert(this->isCompressedOf(graph, lessCompressed));

	// we can not simply unpack because lessCompressed could contain shortcuts

	EdgeIndex less_compressed_index = 0;
	for (EdgeIndex edge_index = 0; edge_index < edges.size(); edge_index++) {
		EdgeID edge_id = edges.at(edge_index);
		std::stack<EdgeID> stack_for_compressed_edge;
		stack_for_compressed_edge.push(edge_id);

		EdgeIndex start = less_compressed_index;
		while (!stack_for_compressed_edge.empty()) {
			if (stack_for_compressed_edge.top() ==
			    lessCompressed.getEdges().at(less_compressed_index)) {
				less_compressed_index++;
				stack_for_compressed_edge.pop();
			} else {
				assert(graph.getEdge(stack_for_compressed_edge.top())
				           .isShortcut());
				EdgeID toUnpack = stack_for_compressed_edge.top();
				EdgeID child1 = graph.getEdge(toUnpack).child_edge1;
				EdgeID child2 = graph.getEdge(toUnpack).child_edge2;

				stack_for_compressed_edge.pop();
				stack_for_compressed_edge.push(child2);
				stack_for_compressed_edge.push(child1);
			}
		}
		EdgeIndex end = less_compressed_index;

		TimeIntervall union_time_intervall;
		for (uint less_edge_index = start; less_edge_index < end;
		     less_edge_index++) {
			union_time_intervall.expand(
			    lessCompressed.getTimeIntervallForEdge(less_edge_index));
		}
		this->setTimeIntervallForEdge(edge_index, union_time_intervall);
	}
	assert(less_compressed_index == lessCompressed.getEdges().size());
}

bool Path::isCompressedOf(Graph const& graph, Path const& lessCompressed) const
{
	if (!(this->unpack(graph) == lessCompressed.unpack(graph))) {
		return false;
	}

	// we can not simply unpack because lessCompressed could contain shortcuts
	std::stack<EdgeID> lessCompressedStack = lessCompressed.getEdgeStack();
	std::stack<EdgeID> moreCompressedStack = this->getEdgeStack();

	assert(lessCompressedStack.size() >= moreCompressedStack.size());
	while (!moreCompressedStack.empty()) {
		if (lessCompressedStack.top() == moreCompressedStack.top()) {
			lessCompressedStack.pop();
			moreCompressedStack.pop();
		} else {
			if (graph.getEdge(moreCompressedStack.top()).isShortcut()) {
				EdgeID toUnpack = moreCompressedStack.top();
				EdgeID child1 = graph.getEdge(toUnpack).child_edge1;
				EdgeID child2 = graph.getEdge(toUnpack).child_edge2;

				moreCompressedStack.pop();
				moreCompressedStack.push(child2);
				moreCompressedStack.push(child1);

			} else {
				return false;
			}
		}
	}

	if (lessCompressedStack.empty()) {
		return true;
	} else {
		return false;
	}
}

std::stack<EdgeID> Path::getEdgeStack() const
{
	std::stack<EdgeID> edge_stack;
	for (auto it = edges.rbegin(); it != edges.rend(); ++it) {
		edge_stack.push(*it);
	}
	return edge_stack;
}

Length Path::getLength(Graph const& graph) const
{
	double length = 0;
	for (EdgeID edge_id : edges) {
		length += graph.getEdge(edge_id).length;
	}
	return length;
}

Path Path::unpackEdgeToPath(Graph const& graph, EdgeID edge_id) const
{
	PathEdgeIDs edgeIds;
	edgeIds.push_back(edge_id);
	return _unpack(graph, edgeIds);
}

bool Path::operator==(const Path& rhs) const
{
	if (this->getEdges().size() != rhs.getEdges().size()) {
		return false;
	}

	for (uint i = 0; i < this->getEdges().size(); i++) {
		if (this->getEdges()[i] != rhs.getEdges()[i]) {
			return false;
		}
	}

	return true;
}

std::ostream& streamOp(
    std::ostream& stream, Graph const& graph, Path const& path)
{
	stream << "Source: " << path.getSource(graph) << "\n";
	stream << "Target: " << path.getTarget(graph) << "\n";
	stream << "Length: " << path.getNoOfRootEdges() << "\n";

	stream << "Edges:\n";
	for (EdgeIndex edge_index = 0; edge_index < path.getEdges().size();
	     edge_index++) {
		stream << graph.getEdge(path.getEdge(edge_index)) << "\n";
		stream << path.getTimeIntervallForEdge(edge_index) << "\n";
	}

	return stream;
}

std::ostream& streamOp(
    std::ostream& stream, Graph const& graph, PathsDS const& paths_ds)
{
	for (PathID path_id = 0; path_id < paths_ds.getNrOfPaths(); path_id++) {
		Path path = paths_ds.getPath(path_id);
		stream << "============"
		       << "\n";
		stream << "Path " << path_id << "\n";
		stream << "============"
		       << "\n";
		streamOp(stream, graph, path) << "\n";

		++path_id;
	}

	return stream;
}

const TimeIntervall Path::getTimeIntervallForEdge(EdgeIndex edge_index) const
{
	TimeType begin = timeAtNodes[edge_index];
	TimeType end = timeAtNodes[edge_index + 1];
	return TimeIntervall(begin, end);
}

void Path::setTimeIntervallForEdge(
    EdgeIndex edge_index, TimeIntervall time_intervall)
{
	timeAtNodes[edge_index] = time_intervall.min_time;
	timeAtNodes[edge_index + 1] = time_intervall.max_time;
}

void Path::setTimeAtNode(NodeIndex node_index, TimeType time)
{
	timeAtNodes[node_index] = time;
}

TimeType Path::getStartTime() const
{
	return timeAtNodes[0];
}

TimeType Path::getEndTime() const
{
	return timeAtNodes.back();
}

Path Path::getSubPathCopy(int start, int end) const
{
	Path path;

	PathEdgeIDs::const_iterator begin_edge = edges.begin() + start;
	PathEdgeIDs::const_iterator end_edge = edges.begin() + end;
	path.edges.clear();
	for (PathEdgeIDs::const_iterator it = begin_edge; it < end_edge; it++) {
		path.edges.push_back(*it);
	}

	PathTimes::const_iterator begin_node = timeAtNodes.begin() + start;
	PathTimes::const_iterator end_node = timeAtNodes.begin() + end + 1;
	path.timeAtNodes.clear();
	for (PathTimes::const_iterator it = begin_node; it < end_node; it++) {
		path.timeAtNodes.push_back(*it);
	}

	return path;
}

} // namespace pf
