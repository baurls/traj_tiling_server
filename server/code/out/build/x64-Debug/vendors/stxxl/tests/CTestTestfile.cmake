# CMake generated Testfile for 
# Source directory: C:/Users/Patrick/Documents/workspace/pathfinder_server/code/vendors/stxxl/tests
# Build directory: C:/Users/Patrick/Documents/workspace/pathfinder_server/code/out/build/x64-Debug/vendors/stxxl/tests
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs("algo")
subdirs("common")
subdirs("containers")
subdirs("io")
subdirs("mng")
subdirs("parallel")
subdirs("stream")
