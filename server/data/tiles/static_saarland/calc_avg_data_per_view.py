import os

#
# © 2021 Lukas Baur, MA
#

image_sizes = {}
MIN_LEVEL = 1
MAX_LEVEL = 18

SHOW_FILE_NAME = False
SHOW_FINAL_MAP = False

# iterate through all 18 levels
for level in range(MIN_LEVEL,MAX_LEVEL+1):
	image_sizes[level] = []
	# iterate through all stripes
	for root, dirs, files in os.walk("{}/".format(level)):
		for file in files:
			if file.endswith(".png"):
				if SHOW_FILE_NAME:
					print(root,"/",  file)
				img_size = os.path.getsize(os.path.join(root,file)) 
				image_sizes[level].append(img_size)

summary = "########## Summary (human-readable)#########\n"
summary += "lvl: #imgs, avg size\n"

csv = "lvl, avgsize, 9avgsize\n"

for level in image_sizes:
	nr_of_images = len(image_sizes[level])
	avg = float(sum(image_sizes[level])) / nr_of_images if nr_of_images > 0 else "NaN"
	summary += "{}:  {},  {}\n".format(level, nr_of_images, avg)
	csv += "{},{},{}\n".format(level, avg, 9*avg)

summary += "\n\n########## Summary (csv-like)#########\n"
summary += csv

with open("average_tile_size.txt", "w+") as outfile:
	outfile.write(summary)

if SHOW_FINAL_MAP:
	print(image_sizes)
print(summary)
