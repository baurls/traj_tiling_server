**Created Method**: Plain, went through each cell once. Skipped empty tiles as well.

**Commits**:  *see below*

**Range**:
- Full: 6, ..., 12
- Partial: None

**Time needed**: 

- [Building independent from each other]
  - Commit: 
    - Skip empty tiles and generate_tiles_for_level_range
    - b82fc947304a0c278e51f36e461c5ac824cfd6f3)
  - 6, ..., 12: 1:22 - 1:34min (Release Build) 
  - 9, ..., 14: 18:29min (Release Build)
  - #searched cells:
    
    - 6: 1
      - 7: 1
      - 8: 2
      - 9: 4
      - 10: 16
      - 11: 42
    
    - 12: 130
- [Building based on last layer]
  - Commit
    - Update current Tale-Layer based on last layer
    - bb546c54f6b2efa41a8fda55fe2fdc7de64ed858
  - 6, ..., 12: 1:25min (Release Build) 
  - 9, ..., 14: 12:42min (Release Build) 
    - #searched cells
      - 6: 1
      - 7: 4
      - 8: 4
      - 9: 8
      - 10: 16
      - 11: 40
      - 12: 108
      - 13: 335
      - 14: 1168 
- [Building based on last layer + path skipping]  
  - Commit
    - Early Path Skipping: If Bounding Box does not intersect
    - a8f081b601de50cfcc38170fa5432a54759a45f2
  - 6, ..., 12: 0:21min (Release Build) 
  - 9, ..., 14: 1:03min (Release Build) 
  - 14, ..., 16: 8:30min (Release Build) 
  - 17: 22:47min (Release Build) 
  - 18: 1:19:48h (Release Build) 

- [Building based on last layer + path skipping  + threading]  
  - Commit 
    - Multithreading also for 'generate_tiles_for_level_range_based_on_last_round'
    - 728c4841eb9fd4ad32aeebcb205e89235c6dec6d
  - 6, ..., 12: 0:19min (Release Build) 
  - 9, ...,14: 0:34min (Release Build) 
  - 14, ...,16: 5:09min (Release Build) 
  - 17: 13:59min (Release Build) 

- [Building based on last layer + path skipping  + threading + no print out + unpacking wrt. lvl]
  -   Commit
      -   [Refac] Moved code to own folder
      -   efbbdd9ee48e286c37c8404b0fb875d19db53cc0
  - 6, ...,12: 0:14min (Release Build) 
  - 9, ...,14: 0:25min (Release Build) 
  - 14, ...,16: 3:08min (Release Build) 
  - 17: 10:37min (Release Build) 

- [Building based on last layer + PATHFINDER  + no print out + unpacking wrt. lvl] 
  - Commit
    - PathfinderTileCreator implemented
    - 019d126188f7d5bfbed3666b58ee40c9ccaf4a04
  - 6, ... ,12: 0:15min (Release Build) 
  - 9, ..., 14: 0:28min (Release Build) 
  - 14, ...,16: 2:07min (Release Build)
  - 17:  7:14min (Release Build)
    - 14, ...,17: 5:49min (Release Build) (it's worth building a 'todo-list' first!)

