![header](docs/header.png)



# Trajectory Tiling Server



This code project contains a fully functional [Pistache](http://pistache.io/) server for creating path tiles using [Pathfinder](https://gitlab.com/anusser/pathfinder) for retrieval and [OpenCV](https://github.com/opencv/opencv) for plotting. It allows to pre-calculate tiles for answering static tiling requests. Moreover, an online-tile generator engine is included, for answering requests on-the-fly. There is also a prototype included showing the overall idea.



## Backend

The backend code is located at `server/`. There is another `readme.md` file to 

For further implementation and installation details take a look at the `readme.md` located at `server/`.

#### Tile directory

To include tilings, place tiles as follows: `server/data/tiles/static/<z>/<y>/<x>.png`

This allows to swich tile sets on-the-fly, i.e., even if the tile server has started already.



### Settings 

The setting can be changed in the file ``server/code/settings.txt``. The attributes are explained in the following.

- ``import type``:  defines how paths should be loaded. They can either be parsed using raw data, loaded from a file (binary or plain) or generated internally on startup time.
- ``graph_filename``:  the input file containing the graph
- ``real_paths_filename``:  if import_type is 0 or 1, this file is used to parse real data
- `generated_paths_export_prefix` and `generated_paths_imoprt_prefix`:  used for exporting or importing artificially generated path-files.
- `max_path_length`: if paths are generated, this number upper bound the distance two randomly chosen points can have.
- `number_of_random_paths`: if paths are generated, this many paths are sampled
- `smoothness` and `max_zoom`: if tiles are generated, unpacking will use this value as a function parameter. Take a look at the original documentation to find a more in-depth-explanation
- `nr_of_pathfinder_threads` and ` nr_of_service_threads`: defines how many threads should be used.
- `generate_tiling`: boolean value, 1 for yes, 0 for no. Triggers the tile generator in startup if set to true.
- `tiling_level_start` and `tiling_level_stop`: if tiling generation is turned on, tiles between this levels will be calculated. The bounds are inclusive. If only a single level *l* should be calculated, set both values to *l*.
- `log_tiling`: boolean value, 1 for yes, 0 for no. If enabled, the server writes the logging process to a log file (location: build folder)



## Frontend

The main frontend code is in ``client/static/``. It consists of a simple tiling layer on top of the normal map layer. Since embedding of the tile sever backend is very straight forward, a more practically implementation has been implemented in the [PathfinderVis Project](https://bitbucket.org/baurls/ma_baur_pathfinder_visualization/src/main/).

Moreover, a prototype showing the main idea of online-tiling is located at ``client/dynamic``.  At first, a request has to be 







## Author / Credits

Master's Thesis: Lukas Baur
FMI, University of Stuttgart
Supervisor: T. Rupp

#### License

Apache License 2.0
