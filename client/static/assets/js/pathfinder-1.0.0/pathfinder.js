/**
 * PathfinderWeb functionalities
 *
 * 
 * Basic Code (Project Inf)
 * @author Shoma Kaiser | 3133314 | st142241@stud.uni-stuttgart.de
 * @author Patrick Lindemann | 3335811 | st155665@stud.uni-stuttgart.de
 * @author Enis Spahiu | 3129195 | st141173@stud.uni-stuttgart.de
 *
 * Extentions & Modifications (Master's Thesis)
 * @author Lukas Baur | 3131138 | st141998@stud.uni-stuttgart.de
 *
 * @version 2021/08/04
 * 
*/
class Pathfinder {

    constructor(apiKey, options = {}) {

        /* Options container */

        this._o = defaults;
        this._batch_decoder;

        if (isEmpty(apiKey)) {
            alert('No Api key given! A mapbox access token is required to make the map work. Please set it in the scripts.js.');
            alert('For more information, visit https://docs.mapbox.com/help/how-mapbox-works/access-tokens/');
            return;
        } else {
            this._o.apiKey = apiKey;
        }


        // Set the defined options if they exist
        if (!isEmpty(options) && options !== {}) {
            for (let key in this._o) {
                if (!isUndefined(options[key]))
                    this._o[key] = options[key];
            }
        }

        this._calculated = false;
        this.heatmap_layer_visible = this._o.use_heatmap_layer;

        // Save the initial options for a possible reset
        this._initialOptions = $.extend(true, {}, this._o);

        /* Shape container */

        this._c = new ShapeContainer();

        /* Sidebar container and hooks */

        this._s = new SidebarContainer();

        /* Map container and hooks */

        this._m = new MapContainer('map', this._o.apiKey, { home: this._o.home });

        var topographyAndPlaces = L.tileLayer.wms(this._o.baseUrl + "tile_server/{z}/{x}/{y}", 
        {
            format: 'image/png',
            transparent: true,
            crs: L.CRS.Simple
        }).addTo(this._m._map);

        var newPathLayer = new L.FeatureGroup().addTo(this._m._map);
        newPathLayer.addLayer(topographyAndPlaces);

        this._addHooks(this._m.getMap(), [
            // Map events
            'mousemove', 'touchmove', 'moveend', 'zoom', 'zoomend'
        ], this._handle.bind(this));

       
    
    
    } 
    
    _addHooks(object$, events, callback, activation = 'on') {
            events.forEach((event) => {
                this._addHook(object$, event, callback, activation);
            });
        }

    _addHook(object$, event, callback, activation = 'on') {
        switch (activation) {
            case 'on': object$.on(event, e => { callback(e) }); break;
            case 'off': object$.off(event, e => { callback(e) }); break;
        }
    }
    
    _handle(e, type = '') {
        type = isEmpty(type) ? e.type : type;
        var layer = e.layer,
            layers = e.layers;

        switch (type) {
            case 'mousemove':

            case 'zoom':
                this._m.zoomField(this._m.getMap().getZoom());
                break;
            
        }
    }

}

var defaults = {

    /** 
     * Flag for debug options.
     * @type {boolean}
     */
    debug: false,

    /**
     * Flag for request and response simulation, if no server is available for usage or testing.
     * @type {boolean}
     */
    simulate: false,

    /**
     * Flag for displaying the heatmap as an overlay.
     * @type {boolean}
     */
    use_heatmap_layer: true,

    /**
     * The shape settings. If a shape is set to false, its functionalities will not be used.
     */
    shapes: {
        rectangle: true
    },

    /**
     * The home point for the map. Can be set as coordinates or configured as an object to add a custom marker.
     */
    home: {
        coords: [48.74515, 9.10667],
        marker: {
            heading: 'Universiät Stuttgart',
            text: 'Institut für formale Methoden der Informatik'
        }
    },

    /**
     * In which format the response should be send
     * @type {string}
     */
    exportType: "trajectories",

    /**
     * In which format the response should be send
     * @type {string}
     */
     baseUrl: "http://localhost:9081/tile_server/",

    /**
     * Flag for trajectory packing on different zoom levels.
     * @type {boolean}
     */
     packing: true,

     /**
      * If set to a number greater or equal to 0, the server will unpack the paths to this specific level.
      * @type {Number}
      */
     level: -1,


    /**
     * Flag for trajectory prunning takes place
     * @type {boolean}
     */
    prunning: false,

    /**
     * If set to a number greater or equal to 1, the server will skip returning edges smaller than this level.
     * @type {Number}
     */
    cutoff_level: -1,

    /**
     * Flag for client for trajectory updates on map interaction.
     * @type {boolean}
     */
    updates: true,

    /**
     * The update rate in miliseconds. If updates are enabled, they will only occur 
     * after the map is unchanged for this time interval.
     * @type {Number}
     */
    updateRate: 750,

    /**
     * Flag for server-side calculation by default.
     * Note: This feature is not implemented yet.
     * @type {boolean}
     */
    // serverSide: false,

    /**
     * Trajectory limit for client-side calculations. If the limit is exeeded, server-side calculation will be enabled.
     * Note: This feature is not implemented yet.
     * @type {Number}
     */
    // limit: 10000,

};

/**
 * The component class.
 */
class Component {

    /**
     * Creates a new Component instance.
     * @constructor 
     * @param {Object} groups A blueprint that maps names to key:value pairs and groups them.
     *                        If empty, the component will have no groups.
     */
    constructor(groups) {
        if (!isEmpty(groups)) {
            for (let group in groups)
                this[group] = groups[group];
        }
    }

    /**
     * Maps an item to a group with a key and value.
     * @param {string} group The group name. If it doesn't exist, the item will not be added.
     * @param {string} key The new item key.
     * @param {Object} value The new item value.
     */
    add(group, key, value) {
        if (this[group] !== undefined && !this[group].has(key))
            this[group].add(key, value);
    }

    /**
     * Removes an item from a group by key.
     * @param {string} group The group name. If it doesn't exist, no item will be removed.
     * @param {Object} key The item key, which can be a string or index.
     */
    remove(group, key) {
        if (this[group] !== undefined)
            this[group].remove(key, item);
    }

    /**
     * Sets the item value of an item 
     * @param {string} group The group name.
     * @param {Object} key The item key, which can be a string or index.
     * @param {Object} value The item value.
     */
    set(group, key, value) {
        if (this[group] !== undefined)
            this[group].set(key, value);
    }

    /**
     * Retrieves the item value by group and key.
     * @param {string} group The group name. If it doesn't exist, undefined will be returned.
     * @param {Object} key The item key, which can be a string or index.
     * @returns {Object} The item value if the group and key exist. If not, undefined will be returned.
     */
    get(group, key) {
        return this[group] !== undefined ? this[group].get(key) : undefined;
    }

    /**
     * Checks if an item with a given key exists within a given group.
     * @param {string} group The group name.
     * @param {Object} key The item key, which can be a string or index.
     * @returns {boolean} True if the group and key exist.
     */
    has(group, key) {
        return this[group] !== undefined && this[group].has(key);
    }

    /**
     * Retrieves the group item index by key.
     * @param {string} group The The group name.
     * @param {Object} key The item key, which can be a string or index.
     * @returns {Object} The item value if the group and key exist. If not, -1 will be returned.
     */
    index(group, key) {
        return this[group].index(key);
    }

}

/**
 * A map / dictionary class for grouping items in key:value pairs. 
 * This class should only be used by components and therefore will not be exported.
 */
class Group {

    /**
     * Creates a new group from an array of item objects with key and value pairs.
     * @constructor
     * @param {Object[]} array The item array. If not an array, _items will be instantiated as [].
     */
    constructor(array) {
        this._items = isArray(array) ? array : [];
    }

    /**
     * Adds a key:value pair to the group.
     * @param {string} key The new item key.
     * @param {Object} value The new item value.
     */
    add(key, value) {
        this._items.push({ key, value });
    }

    /**
     * Removes an item from the group by key if it exists.
     * @param {Object} key The item key, which can be a string or index.
     */
    remove(key) {
        this._items.splice(this.index(key), 1);
    }

    /**
     * Sets the value of an item by key if it exists.
     * @param {Object} key The item key, which can be a string or index.
     * @param {Object} value The item value.
     */
    set(key, value) {
        this._items[this.index(key)].value = value;
    }

    /**
     * Retrieves the value of an item by key.
     * @param {Object} key The item key, which can be a string or index.
     * @returns {Object} The item value if the key exists. If not, undefined will be returned.
     */
    get(key) {
        return this.has(key) ? this._items[this.index(key)].value : undefined;
    }

    /**
     * Checks if an item with a given key exists.
     * @param {Object} key The item key, which can be a string or index.
     * @returns {boolean} True if the key exists.
     */
    has(key) {
        return this.index(key) >= 0;
    }

    /**
     * Retrieves the item index by key.
     * @param {Object} key The item key, which can be a string or index.
     * @returns {Object} The item value if the key exists. If not, -1 will be returned.
     */
    index(key) {
        switch (typeOf(key)) {
            case 'number':
                if (key >= 0 && key < this._items.length)
                    return key;
                break;
            case 'string':
                for (let i = 0; i < this._items.length; i++) {
                    if (this._items[i].key === key)
                        return i;
                }
                break;
            default:
                return -1;
        }
    }

    /**
     * Iterates through the group items and executes a given function on the key:value pairs.
     * @param {function} func The function that is executed in each iteration.
     */
    forEach(func) {
        this._items.forEach((_item) => {
            func(_item.key, _item.value);
        });
    }

    /**
     * Returns the size of the group item array.
     */
    size() {
        return this._items.length;
    }

}

/**
 * A Node that holds the latitude and longitude.
 */
class Node {

    /**
     * Creates a new node instance.
     * @constructor
     * @param {*} arg0 Can be a Node, a L.LatLng object, an Array or else will be interpreted as the Node latitude.
     * @param {*} arg1 The Node longitude if arg0 is not a Node, L.LatLng or Array.
     */
    constructor(arg0, arg1) {
        if (arg0 instanceof Node || arg0 instanceof L.LatLng) {
            // Create a node directly from a node or latlng object
            this._lat = arg0.lat;
            this._lng = arg0.lng;
        } else if (isArray(arg1)) {
            // Create a node from array
            this._lat = arg0[0];
            this._lng = arg0[1];
        } else {
            this._lat = arg0;
            this._lng = arg1;
        }
    }

    /**
     * Sets the node latitude
     * @param {number} lat The new latitude
     */
    setLat(lat) {
        this._lat = lat;
    }

    /**
     * Sets the node longitude
     * @param {number} lng The new longitude
     */
    setLng(lng) {
        this._lng = lng;
    }

    /**
     * Returns the node latitude.
     * @param {number} decimals The number of decimals which will be used for rounding. If empty, the number won't be rounded.
     */
    getLat(decimals = -1) {
        return decimals >= 0 ? this._lat.toFixed(decimals) : this._lat;
    }

    /**
     * Returns the node longitude.
     * @param {number} decimals The number of decimals which will be used for rounding. If empty, the number won't be rounded.
     */
    getLng(decimals = -1) {
        return decimals >= 0 ? this._lng.toFixed(decimals) : this._lng;
    }

    /**
     * Converts the node to a leaflet latlng (L.LatLng) object.
     */
    toLatLng() {
        return new L.LatLng(this._lat, this._lng);
    }

    /**
     * Converts the node to a string.
     * @param {number} decimals The number of decimals which will be used for rounding. If empty, the numbers won't be rounded.
     */
    toString(decimals = -1) {
        return this.getLat(decimals) + " / " + this.getLng(decimals);
    }

    /**
     * Converts the node to an array. The result array will hold the lat at index 0, lng at index 1.
     */
    toArray() {
        return [this._lat, this._lng];
    }

}


/**
 * The ShapeContainer class.
 */
class ShapeContainer extends Component {

    /**
     * ShapeContainer
     */
    constructor() {

        // Create the groups.
        super({
            rectangle: new Component({
                _nodes: new Group([
                    { key: 'min', value: undefined },
                    { key: 'max', value: undefined },
                ])
            })
        });

        // Create the supported shape types.
        this._shapeTypes = [
            'none',
            'rectangle'
        ];

        this._shapeType = 'none';
    }

    reset(types) {
        var words = types.split(' ');
        words.forEach(type => {
            switch (type) {
                case 'rectangle':
                    this._clearNodes('rectangle');
                    break;
                case 'all':
                    this._clearNodes('rectangle');
            }
        });
    }

    /* Type functions */

    type(shapeType = '') {
        if (isEmpty(shapeType))
            return this._shapeType;
        this._shapeType = this.hasType(shapeType) ? shapeType : undefined;
    }

    hasType(shapeType) {
        return this._shapeTypes.includes(shapeType);
    }

    resetType() {
        this._shapeType = 'none';
    }

    /* Node functions */

    addNode(component, key, node) {
        this[component].add('_nodes', key, node);
    }

    removeNode(component, key) {
        this[component].remove('_nodes', key);
    }

    node(component, key, node = undefined) {
        if (isUndefined(key))
            return this[component]._nodes;
        else if (isUndefined(node))
            return this[component].get('_nodes', key);
        this[component].set('_nodes', key, node);
    }

    hasNode(component, key) {
        return this[component].has('_nodes', key);
    }

    _clearNodes(component) {
        this[component]._nodes.forEach((key, _node) => {
            this[component]._nodes[key] = undefined;
        });
    }

    /**
     * Converts a shape to the correct representation of geometric values
     */
    transform(component) {

        switch (component) {
            case 'rectangle':
                // Check if both coordiantes are set
                if (isEmpty(this.node('rectangle', 'min')) || isEmpty(this.node('rectangle', 'max')))
                    return;

                let min = this.node('rectangle', 'min');
                let max = this.node('rectangle', 'max');

                // 4 "normal" possible rectangle cases
                if (min._lat < max._lat && min._lng < max._lng) {
                    // Rectangle correct

                } else if (min._lat < max._lat && min._lng > max._lng) {
                    // Max is right from min, switch lngs
                    let tmpLng = min._lng;
                    min._lng = max._lng;
                    max._lng = tmpLng;

                } else if (min._lat > max._lat && min._lng < max._lng) {
                    // Min is on top of max, switch lats
                    let tmpLat = min._lat;
                    min._lat = max._lat;
                    max._lat = tmpLat;

                } else if (min._lat > max._lat && min._lng > max._lng) {
                    // Rectangle is correct, but min and max are switched
                    let tmp = min;
                    min = max;
                    max = tmp;

                } else {
                    // Rectangle is a point or line
                }

                // Replace the transformed values
                this.node('rectangle', 'min', min);
                this.node('rectangle', 'max', max);

            default:

        }
        return this[component]._nodes;
    }
}

/**
 * The MapContainer class.
 */
class MapContainer extends Component {

    /**
     * MapContainer
     * @constructor
     * @param {*} domId 
     * @param {*} apiKey 
     * @param {*} options 
     */
    constructor(domId, apiKey, options) {

        // Create the groups
        super({
            _layerGroups: new Group(),
            _controls: new Group(),
            _fields: new Group(),
            _buttons: new Group()
        });

        // Create the supported shape types.
        this._mapStates = [
            'none',
            'drawing',
            'drawing-active',
            'created',
            'editing',
        ];

        // Save the options and the apiKey
        this._options = {};
        this._options.apiKey = apiKey;
        if (isArray(options.home)) {
            this._options.home = options.home;
        } else {
            if (isUndefined(options.home.coords) || !isArray(options.home.coords)) {
                console.error('Invalid home point format! The MapContainer can\'t be created.');
                return;
            }
            this._options.home = options.home.coords;
            this._options.marker = options.home.marker;
        }

        // Hard-coded max zoom
        this._options.maxZoom = 18;

        // Create the map
        this._map = L.map(
            domId,
            {
                center: this._options.home,
                zoom: this._options.maxZoom
            }
        );

        // Initialize the state
        this._mapState = 'none';

        // Initialize the groups
        this._init();

    }

    _init() {

        /* Layers */

        this.add(
            '_layerGroups',
            'tiles',
            L.tileLayer("https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=" + this._options.apiKey, {
                attribution: '© <a href="https://www.mapbox.com/about/maps/">Mapbox</a> © <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a> <strong><a href="https://www.mapbox.com/map-feedback/" target="_blank">Improve this map</a></strong>',
                tileSize: 512,
                maxZoom: this._options.maxZoom,
                zoomOffset: -1,
                id: "mapbox/streets-v11"
            }).addTo(this._map)
        );

        this.add('_layerGroups', 'draw', new L.FeatureGroup().addTo(this._map));

        this.add('_layerGroups', 'trajectories', new L.FeatureGroup().addTo(this._map));

        this.add('_layerGroups', 'edges', new L.FeatureGroup().addTo(this._map));

        this.add('_layerGroups', 'passing_trajectories', new L.FeatureGroup().addTo(this._map));

        this.add('_layerGroups', 'heatmap', new L.FeatureGroup().addTo(this._map));

        this.add('_layerGroups', 'markers', new L.FeatureGroup().addTo(this._map));

        // Create a home marker if set in options
        if (this._options.marker !== false) {
            this._createHomeMarker();
        }

        /* Controls */

        let baseColor = '#b82c16';

        this.add(
            '_controls',
            'draw',
            new L.Control.Draw({
                position: 'topleft',
                draw: {
                    polygon: false,
                    polyline: false, // Turn off line drawing
                    rectangle: {
                        shapeOptions: {
                            color: baseColor,
                        },
                        metric: true,
                    },
                    circle: false,
                    marker: false, // Turn off marker placing
                    circlemarker: false, // Turn off circlemarker placing
                },
                edit: {
                    featureGroup: this.layerGroup('draw'),
                    remove: true
                }
            })
        );
        this._map.addControl(this.get('_controls', 'draw'));

        L.Control.Home = L.Control.extend({
            options: {
                position: 'topright'
            },
            onAdd: (map) => {
                var container = L.DomUtil.create('div', 'leaflet-control-btn leaflet-bar leaflet-control');
                container.innerHTML = '<i class="fas fa-home"></i>'
                container.title = 'Go home';
                container.style.paddingLeft = '6px'
                container.onclick = () => {
                    map.flyTo(this._options.home, this._options.maxZoom);
                }
                L.DomEvent.disableClickPropagation(container);
                return container;
            }
        });
        this.add('_controls', 'home', new L.Control.Home());
        this._map.addControl(this.get('_controls', 'home'));

        /* Fields */

        $('.leaflet-bottom.leaflet-left').html(
            '<div id="map-coords" class="leaflet-control-attribution leaflet-control">'
            + '<a href="https://en.wikipedia.org/wiki/Latitude">Latitude</a>: <span id="map-lat">' + this._options.home[0].toFixed(5) + '</span> | '
            + '<a href="https://en.wikipedia.org/wiki/Longitude">Longitude</a>: <span id="map-lng">' + this._options.home[1].toFixed(5) + '</span> | '
            + 'Zoom: <span id="map-zoom">' + this._map.getZoom() + '</span>'
            + '</div>'
        );

        this.add('_fields', 'coords-lat$', $('#map-coords > #map-lat'));

        this.add('_fields', 'coords-lng$', $('#map-coords > #map-lng'));

        this.add('_fields', 'coords-zoom$', $('#map-coords > #map-zoom'));

        /* Buttons */

        this.add(
            '_buttons',
            'draw-rectangle$',
            $('.leaflet-draw-draw-rectangle')
        )

        this.add(
            '_buttons',
            'edit$',
            $('.leaflet-draw-edit-edit')
        )

        this.add(
            '_buttons',
            'remove$',
            $('.leaflet-draw-edit-remove')
        )

    }

    /**
     * Multiple type support, splitted on space
     * @param {string} types 
     */
    reset(types) {
        var words = types.split(' ');
        words.forEach(type => {
            switch (type) {
                case 'tiles':
                    return;
                case 'draw':
                    this._clearLayerGroup('draw');
                    break;
                case 'trajectories':
                    this._clearLayerGroup('trajectories');
                    break;
                case 'edges':
                    this._clearLayerGroup('edges');
                    break;
                case 'passing_trajectories':
                    this._clearLayerGroup('passing_trajectories');
                    break;
                case 'heatmap':
                    this._clearLayerGroup('heatmap');
                    break;
                case 'all':
                default:
                    this._clearLayerGroup('draw');
                    this._clearLayerGroup('trajectories');
                    this._clearLayerGroup('edges');
                    this._clearLayerGroup('passing_trajectories');
                    this._clearLayerGroup('heatmap');
            }
        });
    }

    /* Map functions */

    getMap() {
        return this._map;
    }

    /* State functions */

    state(mapState = '') {
        if (isEmpty(mapState))
            return this._mapState;
        this._mapState = this.hasState(mapState) ? mapState : undefined;
    }

    hasState(mapState) {
        return this._mapStates.includes(mapState);
    }

    resetState() {
        this._mapState = 'none';
    }

    _pointlist_to_polyline(pointlist){
        var line_list = [];
        for(var i = 1; i <= pointlist.length; i+=2) {
            var lat = pointlist[i-1];
            var lon = pointlist[i];
            line_list.push([lat, lon]);
        }
        return line_list;
    }

    /* Draw functions */


    /* Layer group functions */

    layerGroup(key, layerGroup = undefined) {
        if (isUndefined(key))
            return this._layerGroups;
        else if (isUndefined(layerGroup))
            return this.get('_layerGroups', key);
        this.set('_layerGroups', key, layerGroup);
    }

    hasLayerGroup(key) {
        return this.has('_layerGroups', key);
    }

        _clearLayerGroup(key) {
        let layers = this.layerGroup(key)._layers;
        for (let layer in layers)
            this.layerGroup(key).removeLayer(layer);
    }

    /* Layer functions */

    addLayer(group, layer) {
        this.layerGroup(group).addLayer(layer);
    }

    /* Button functions */

    button(key, button = undefined) {
        if (isUndefined(key))
            return this._buttons;
        else if (isUndefined(button))
            return this.get('_buttons', key);
        this.set('_buttons', key, button);
    }

    /* Coordinate functions */

    coordsField(e = undefined) {
        if (isUndefined(e))
            return [this.get('_fields', 'coords-lat$'), this.get('_fields', 'coords-lng$')];
        this.get('_fields', 'coords-lat$').text(e.latlng.lat.toFixed(5));
        this.get('_fields', 'coords-lng$').text(e.latlng.lng.toFixed(5));
    }

    zoomField(zoom = undefined) {
        if (isUndefined(zoom))
            return this.get('_fields', 'coords-zoom$');
        zoom = zoom % 1 == 0 ? zoom : zoom.toFixed(2);
        this.get('_fields', 'coords-zoom$').text(zoom);
    }

    /* Marker functions */

    _createHomeMarker() {
        var homeMarker = L.marker(this._options.home);
        var popupHTML = '';

        // Create a popup for marker information if set in options
        if (hasProperty(this._options.marker, 'heading'))
            popupHTML += `<h6>${this._options.marker.heading}</h6>`
        else
            popupHTML += `<h6>Home</h6>`

        if (hasProperty(this._options.marker, 'text')) {
            popupHTML += `<p>${this._options.marker.text}</p>`;
        }

        homeMarker.bindPopup(popupHTML);
        this.addLayer('markers', homeMarker);
    }

}

/**
 * The SidebarContainer class.
 */
class SidebarContainer extends Component {

    /**
     * Creates a new SidebarContainer instance.
     * @constructor
     */
    constructor() {
        super({
            rectangle: new Component({
                _fields: new Group([
                    { key: 'min$', value: $('#rect-min input') },
                    { key: 'max$', value: $('#rect-max input') },
                ]),
                _buttons: new Group([
                    { key: 'draw$', value: $('#rect-draw') },
                ])
            }),
            exportType: new Component({
                _fields: new Group([
                    { key: 'export-traj$', value: $('#export-type input').eq(0) },
                    { key: 'export-segmentgraph$', value: $('#export-type input').eq(1) },
                    { key: 'export-heatmap$', value: $('#export-type input').eq(2) },
                    { key: 'export-batch-plain-inorder$', value: $('#export-type input').eq(3) },
                    { key: 'export-batch-plain-levelorder$', value: $('#export-type input').eq(4) },
                    { key: 'export-batch-edgewise-queue$', value: $('#export-type input').eq(5) },
                    { key: 'export-prunning$', value: $('#export-type input').eq(6) }
                ])
            }),
            advInterval: new Component({
                _checkbox: new Group([
                    { key: 'active$', value: $('#adv-interval .component-toggle input').first() }
                ]),
                _fields: new Group([
                    { key: 'start$', value: $('#adv-interval #interval-start input') },
                    { key: 'end$', value: $('#adv-interval #interval-end input') }
                ])
            }),
            advDays: new Component({
                _checkbox: new Group([
                    { key: 'active$', value: $('#adv-days .component-toggle input').first() }
                ]),
                _fields: new Group([
                    { key: 'days$', value: $('#adv-days #days-boxes input') }
                ])
            }),
            advHeatmap: new Component({
                _checkbox: new Group([
                    { key: 'active$', value: $('#adv-heatmap .component-toggle input').first() }
                ])
            }),
            advPacking: new Component({
                _checkbox: new Group([
                    { key: 'active$', value: $('#adv-packing .component-toggle input').first() }
                ]),
                _fields: new Group([
                    { key: 'type-zoom$', value: $('#adv-packing #packing-type input').eq(0) },
                    { key: 'type-level$', value: $('#adv-packing #packing-type input').eq(1) },
                    { key: 'level$', value: $('#adv-packing #packing-level input') },
                    { key: 'updates$', value: $('#adv-packing #packing-updates input') }
                ])
            }),
            advPrunning: new Component({
                _checkbox: new Group([
                    { key: 'active$', value: $('#adv-prunning .component-toggle input').first() }
                ]),
                _fields: new Group([
                    { key: 'level$', value: $('#adv-prunning #prunning-level input') }
                ])
            }),
            advButtons: new Component({
                _buttons: new Group([
                    { key: 'calculate$', value: $('#adv-buttons #button-calculate') },
                    { key: 'save$', value: $('#adv-buttons #button-save') },
                    { key: 'reset$', value: $('#adv-buttons #button-reset') }
                ])
            }),
            result: new Component({
                _fields: new Group([
                    { key: 'found$', value: $('#result-found') },
                    { key: 'total$', value: $('#result-total') },
                    { key: 'length$', value: $('#result-length') }
                ]),
                _buttons: new Group([
                    { key: 'calculate$', value: $('#calculate') },
                ])
            }),
        });
    }

    /**
     * Multiple type support, splitted on space
     * @param {*} types 
     */
    reset(types) {
        var words = types.split(' ');
        words.forEach(type => {
            switch (type) {
                case 'shapes':
                    this._clearFields('rectangle');
                    break;
                case 'rectangle':
                    this._clearFields('rectangle');
                    break;
                case 'advanced':
                    this._clearFields('advInterval');
                    this._clearFields('advDays');
                    this._clearFields('advPacking');
                    break;
                case 'results':
                    this._clearFields('result');
                    break;
                case 'all':
                    this._clearFields('rectangle');
                    this._clearFields('advInterval');
                    this._clearFields('result');
            }
        });
    }

    /* Field functions */

    addField(component, key, field) {
        this[component].add('_fields', key, field);
    }

    removeField(component, key) {
        this[component].remove('_fields', key);
    }

    field(component, key, field = undefined) {
        if (isUndefined(key))
            return this[component]._fields;
        else if (isUndefined(field))
            return this[component].get('_fields', key);
        this[component].set('_fields', key, field);
    }

    fieldVal(component, key, value = undefined) {
        if (isUndefined(value))
            return this[component].get('_fields', key).val();
        this[component].get('_fields', key).val(value);
    }

    fieldText(component, key, text = undefined) {
        if (isUndefined(text))
            return this[component].get('_fields', key).text();
        this[component].get('_fields', key).text(text);
    }

    hasField(component, key) {
        return this[component].has('_fields', key);
    }

    _clearFields(component) {
        this[component]._fields.forEach((key, _field) => {
            this[component].get('_fields', key).val('');
            this[component].get('_fields', key).text('');
        });
    }

    /* Checkbox functions */
    checkbox(component, checkbox = undefined) {
        if (isUndefined(checkbox))
            return this[component].get('_checkbox', 'active$');
        this[component].set('_buttons', 'active$', checkbox);
    }

    active(component, bool = undefined) {
        if (isUndefined(bool))
            return this[component].get('_checkbox', 'active$').prop('checked');
        this[component].get('_checkbox', 'active$').prop('checked', bool);
    }

    /* Button functions */

    button(component, key, button = undefined) {
        if (isUndefined(key))
            return this[component]._buttons;
        else if (isUndefined(button))
            return this[component].get('_buttons', key);
        this[component].set('_buttons', key, button);
    }

    hasButton(component, key) {
        return this[component].has('_buttons', key);
    }

    enableButton(component, key) {
        this[component].get('_buttons', key).prop('disabled', false);
    }

    disableButton(component, key) {
        this[component].get('_buttons', key).prop('disabled', true);
    }

    /* Other functions */

    getIntervalDays() {
        // Retrieve the interval weekday checkboxes
        let checkboxes$ = this.field('advDays', 'days$').find('input').end();
        // Save their checked values in an array
        let boolArray = []
        $.each(checkboxes$, function (index, checkbox) {
            boolArray.push(checkbox.checked);
        })
        // Return the checked value array
        return boolArray;
    }

    /**
     * Validate if values are set and put their values in the result fields
     * @param {*} item 
     */
    setResults(item) {
        // Validate and set found
        var found = !isUndefined(item.found) ? item.found : '-';
        this.fieldText('result', 'found$', found);

        // Validate and set total
        var total = !isUndefined(item.total) ? item.total : '-';
        this.fieldText('result', 'total$', total);

        // Validate and set total length 
        var length = !isUndefined(item.length) && item.length > 0 ? distanceToString(item.length) : '-';
        this.fieldText('result', 'length$', length);
    }

}

/**
 * Some util functions
 */
function hasProperty(object, key) {
    return object ? hasOwnProperty.call(object, key) : false;
}

function typeOf(obj) {
    return toString.call(obj).slice(8, -1).toLowerCase();
}

function isUndefined(value) {
    return typeof value === 'undefined'
}

function isNull(value) {
    return typeof value === 'null'
}

function isEmpty(value) {
    return isUndefined(value) || isNull(value) || (isString(value) && value == '')
}

function isString(value) {
    return typeof value === 'string'
}

function isNaN(value) {
    return Number.isNaN(value);
}

function isNumber(value) {
    return typeof value === 'number' && !isNaN(value)
}

function isArray(value) {
    return typeOf(value) === 'array' && Array.isArray(value);
}

function stringToNode(str) {
    str = str.replace(' ', '') // Replace all spaces
    let splits = str.split('/') // Split the values
    return new Node(Number(splits[0]), Number(splits[1]))
}

function groupToCoords(group) {
    let coords = [];
    group.forEach((key, _node) => {
        coords.push([_node.getLat(), _node.getLng()]);
    });
    return coords;
}

function checkInterval(start, end) {
    if (isEmpty(start) && !isEmpty(end)) {
        alert("Invalid time interval input! .")
        return new IllegalArgumentException('No start time was set.')
    } else if (!this.isEmpty(start) && this.isEmpty(end)) {
        alert("Invalid time interval input! No end time was set.")
        return false
    } else if (!this.isEmpty(start) && !this.isEmpty(end)) {
        if (start === NaN) {
            alert("Invalid time interval input! The start date is invalid.")
            return false
        } else if (end === NaN) {
            alert("Invalid time interval input! The end date is invalid.")
            return false
        } else if (start < 0 || end < 0) {
            alert("Invalid time interval input! The start or end date is to small.")
            return false
        } else if (start > 2147483647 || end > 2147483647) {
            alert("Invalid time interval input! The start or end date is too big.")
            return false
        } else if (start > end) {
            alert("Invalid time interval input! The start date is greater than the end date.")
            return false
        }
        return true;
    }
    alert("Invalid time interval input! No start and end times were set.")
    return false;
}

function arrayToBitString(array) {
    if (!Array.isArray(array))
        return ""

    let bitString = ""
    array.forEach((val) => {
        bitString += val ? "1" : "0"
    })
    return bitString
}

function distanceToString(dist, metric = true) {
    if (this.isEmpty(dist) || !isNumber(dist))
        return 0;
    else if (dist < 0)
        return NaN;

    var result = "";

    if (metric) {
        // Trajectory lengths are in dm
        dist = dist / 10;
        result = (dist.toString().indexOf(".") < 0 ? dist : dist.toFixed(2)) + " m";
        if (dist > 1000) {
            dist = dist / 1000;
            result = (dist.toString().indexOf(".") < 0 ? dist : dist.toFixed(2)) + " km";
        }
    } else {
        units = ['in', 'ft', 'yd', 'mi']
    }
    return result;
}

function unixToDate(unix) {
    return moment.unix(unix).format("MM/DD/YYYY");
}
