/* Default Mapbox API-Key, you can change it here if needed */
const API_KEY = 'pk.eyJ1IjoiZm1pLWJhdXJscyIsImEiOiJja21ldTluNm8waHJmMm9wanlleWNoNXBkIn0.A6kjG_G6dj1VbpLLNDvpBg';

// Load the modules first
load(modules, VENDOR_PATH, function() {

    /* Pathfinder initialization */
    if (typeof Pathfinder !== 'undefined') {
        pf = new Pathfinder(API_KEY, {
            debug: true,
            home: { 
                coords: [ 48.917, 8.913],
                marker: {
                    heading : "Riedberg",
                    text : "Starting Point"
                } 
            },
            use_heatmap_layer: false,
            //baseUrl: "http://threadripper.informatik.uni-stuttgart.de:9081/",
            baseUrl : "http://localhost:9081/",
        });
    }

});

