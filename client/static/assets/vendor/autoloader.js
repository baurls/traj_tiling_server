/**
 * Module constants & script loader
 */

/* 
* Path variables 
*/
const VENDOR_PATH = 'assets/vendor/';

/* 
* Define scripts variables 
*/

/* All modules*/
const modules = [
    { src: 'jquery-3.4.1/js/jquery-3.4.1.min.js' },
    { src: 'bootstrap-4.3.1/js/bootstrap.min.js' },
    { src: 'leaflet-1.5.1/js/leaflet.js' },
    { src: 'leaflet.draw/1.0.4/leaflet.draw.js'},
    { src: 'datepicker-1.0.9/dist/datepicker.min.js'},
    { src: 'moment-2.24.0/moment.min.js' },
    { src: 'leaflet.heat-0.2.0/dist/leaflet-heat.js' } 
]

/**
 * Loads an array of scripts in-order
 * @param {*} array The array of scripts
 * @param {*} prefix The path prefix, default is ''
 */
function load(array, prefix, callback) {
    if ( typeof array !== 'undefined' && Array.isArray(array) ) {
        let position = document.getElementsByTagName('footer')[0];
        array.forEach(element => {
            let script = document.createElement('script');
            script.type = "text/javascript";
            script.src = prefix + element.src;
            script.async = false; // Load scripts syncronically
            position.insertBefore(script, position.parent);
        });
        setTimeout(() => callback(), 250);
    }
}